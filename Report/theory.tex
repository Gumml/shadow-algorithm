\section{Theory}

In the \ac{POM} algorithm, the actual texture sampling point is moved along the view vector $v$ towards the observer. The offset vector is the vector between the original sample position and the coordinates, where the view vector intersects with the height profile (Fig. \ref{fig:pom1}).
\begin{figure}[htbp]
	\centering
		\includegraphics[width=0.75\textwidth]{images/pom1.png}
	\caption{Determining the offset vector in \ac{POM} by sampling the height profile (image \cite{pomTatarchuk}).}
	\label{fig:pom1}
\end{figure}
To determine the self-shadowing effect, the height profile is sampled along the light vector, with the previously calculated, shifted texture sampling point as new origin (Fig. \ref{fig:pom2}).
\begin{figure}[htbp]
	\centering
		\includegraphics[width=0.75\textwidth]{images/pom2.png}
	\caption{Algorithm to determine the shading of a fragment (image \cite{pomTatarchuk}).}
	\label{fig:pom2}
\end{figure}

With some adjustments, this concept can be applied to a global scene. In the first step, a dynamic height map needs to be created. To achieve this, an observer position $\vec{p_o}$ has to be placed to the center of the scene in such a manner as no y-axis value of the geometry exceeds the y-axis value of $\vec{p_o}$. By placing $\vec{p_o}$ to the center of the scene, the origin of the geometry $\vec{o_g} = \vec{0}$ in the height map is placed to the center of the texture, too. Further, the orthogonal projection volume should be equal, or at least very similar, to the minimum bounding box of whole scene. In the next step, the offset vector for texture sampling has to be calculated. In the \ac{POM} algorithm the texture coordinates of the geometry itself are shifted. Since the depth values of all objects in a scene are mapped into a single texture, accessing this particular texture by using the geometry's texture coordinates wouldn't lead to a correct result. Instead, the position coordinates of the geometry have to be transformed. In addition, to match the OpenGL texture coordinate system, the z-component of the resulting vector needs to be adjusted. Let $\vec{v}$ be the position coordinates of a vertex, $B$ the transformation matrix (stated in \cite{shadowmapping} and slightly adjusted) and $\vec{t}$ the transformed texture coordinate. Further, let $d$ be the dimension of the scene, then the required transformation is expressed by formula \ref{f:texturetransform}.
\begin{align}\label{f:texturetransform}
	\vec{v^{'}} &= B \cdot \vec{v} \\
	\nonumber
	\begin{pmatrix}
		v^{'}_x \\
		v^{'}_y \\
		v^{'}_z \\
		1
	\end{pmatrix} &= \begin{pmatrix}
		\frac{1}{d_x} & 0 & 0 & \frac{1}{2} \\
		0 & \frac{1}{d_y} & 0 & 0 \\
		0 & 0 & \frac{1}{d_z} & \frac{1}{2} \\
		0 & 0 & 0 & 1
	\end{pmatrix} \cdot \begin{pmatrix}
		v_x \\
		v_y \\
		v_z \\
		1
	\end{pmatrix} \\
	\nonumber
	\begin{pmatrix}
		t_x \\
		t_y \\
		t_z \\
		1
	\end{pmatrix} &= \begin{pmatrix}
		v^{'}_x \\
		v^{'}_y \\
		1 - v^{'}_z \\
		1
	\end{pmatrix}
\end{align}
Both, the offset vector $\vec{t}$ and the generated height map are forwarded to the fragment part of the \ac{POM} algorithm, however, only the occlusion shadow part is used for the shadow calculation in the scene.

However, this two-dimensional approach suffers from an information-lost-problem \citep{mmHS}. The reason for this problem is the projection of 3D data, the geometry, onto a 2D surface, the height map. This problem can be solved by storing the volume information of the whole scene in a 3D texture of suitable resolution. A suitable resolution of the volume map is important, because the Nyquist-Shannon sampling theorem states, that, in order to restore a signal, the sampling frequency must be twice as high as the highest frequency occurring in the sampled signal. Hence, if the resolution of the volume map is too low, information about the geometry in the scene is lost and the result is likely to be incorrect. The volume map has to contain solid volume information. Solid means, that not only the surface of an object, but also the containing space between the surfaces has to be stored in the volume map. To determine the cast shadows in the scene, the volume map is sampled along the light vector $l$ towards a light source, starting from the current fragment's sampling coordinates. The sampling coordinates can be derived from the vertex position (f. \ref{f:texturetransform}). It requires a minor adjustment on $B$. Instead of the dimension of the scene, $d$ now represents the dimension of the volume map. If an intersection of $l$ with a volume occurs, the fragment at the given position is shaded (Fig. \ref{fig:volumesampling}).
\begin{figure}[htbp]
	\centering
		\begin{tikzpicture}[scale=0.8]
			\draw[fill = lightgray!30] (0.25 * 0.707, -.25 * 0.707) -- (9, -.25 * 0.707) -- (9 * 0.707, -5.5 * 0.707) -- (-5. * 0.707,  -5.5 * 0.707) -- cycle; 
			\draw [fill=none] (9 * 0.707, -5.7 * 0.707) node[left] {\tiny surface};

			\begin{scope}[shift={(2.5, 0.75)}]
			\coordinate (a) at (0, 0);
			\coordinate (b) at (1, 0);
			\coordinate (c) at (0, 1);
			\coordinate (d) at (1, 1);

			\coordinate (e) at  (.4, .4);
			\coordinate (f)  at (1.4, .4);
			\coordinate (g) at  (.4, 1.4);
			\coordinate (h) at  (1.4, 1.4);

			\draw[fill = none, color = red!60] (a) -- (b) -- (d) -- (c) -- cycle;
			\draw[dashed, color = red!60] (e) -- (f);
			\draw[color = red!60] (h) -- (g);
			\draw[dashed, color = red!60] (g) -- (e);
			\draw[color = red!60] (f) -- (h);
			\draw[dashed, color = red!60] (a) -- (e);
			\draw[color = red!60] (c) -- (g);
			\draw[color = red!60] (b) -- (f);
			\draw[color = red!60] (d) -- (h);

			\draw [fill=none, color = red!60] (.4, 1.55) node[left] {\tiny volume data};
			\end{scope}

			\draw[densely dotted, red!50] (.25 * .707, -.25 * .707) -- (.25 * .707, 4.5);
			\draw[densely dotted, red!50] (9, -.25 * .707) -- (9, 4.5);
			\draw [fill=none, red!50] (9, 4.75) node[left] {\tiny volume map};
			\draw[densely dotted, red!50] (9 * .707, -5.5 * 0.707) -- (9 * .707, .6115);
			\draw[densely dotted, red!50] (-5. * 0.707,  -5.5 * 0.707) -- (-5 * .707, .6115);
			\draw[densely dotted, red!50] (.25 * .707, 4.5) -- (-5 * .707, .6115);
			\draw[densely dotted, red!50] (9, 4.5) -- (9 * .707, .6115);
			\draw[densely dotted, red!50] (.25 * .707, 4.5) -- (9, 4.5);
			\draw[densely dotted, red!50] (-5 * .707, .6115) -- (9 * .707, .6115);

			\coordinate (light) at (3.5, 3.7);
			\draw[fill=blue!50] (light) circle [radius = 0.05];
			\draw [fill=none] (light) node[right] {\tiny light source};

			\coordinate (lit) at (6.5, -1);
			\draw[fill=magenta!50] (lit) circle [radius = 0.05];
			\draw [fill=none] (lit) node[left] {\tiny lit};
			\draw [fill=none] (lit) node[right] {\tiny $p_2$};

			\coordinate (unlit) at (1.65, -2.6);
			\draw[fill=green!40] (unlit) circle [radius = 0.05];
			\draw [fill=none] (unlit) node[left] {\tiny unlit};
			\draw [fill=none] (unlit) node[right] {\tiny $p_1$};

			\draw[-latex, color = black!80] (lit) -- (light);
			\draw [fill=none] (5.15, 1.115) node[right] {\tiny $l_2$};

			\draw[color = black!80] (unlit) -- (2.76, 1.18);
			\draw[-latex, color = black!80, dashed] (2.76, 1.18) -- (light);
			\draw [fill=none] (2.4825, 0.235) node[left] {\tiny $l_1$};
					
			\draw[semithick, -latex] (0, 0) -- (0,5);
			\draw[semithick, fill=none] (0,5) node[right] {$y$};
			\draw[semithick, dashed] (0, -0.5) -- (0, 0);
			\draw[semithick, -latex] (0, 0) -- (10,0);
			\draw[semithick, fill=none] (10,0) node[right] {$x$};
			\draw[semithick, dashed] (-0.5, 0) -- (0, 0);
			\draw[semithick, -latex] (0, 0) -- (-6 * 0.707,  -6 * 0.707);
			\draw[semithick, fill=none] (-6 * 0.707,  -6 * 0.707) node[left] {$z$};
			\draw[semithick, dashed] (0.5 * 0.707, .5 * 0.707) -- (0, 0);

		\end{tikzpicture}
	\caption{The basic idea of the volume map approach. The light vector $l_1$ intersects with a volume, whereby $p_1$ is unlit.}
	\label{fig:volumesampling}
\end{figure}
