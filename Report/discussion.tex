\section{Discussion}

This section will discuss the achieved results of this project. It will cover the trade-offs made during the implementation as well as the basic strengths and limitations of the idea. The second last paragraph in this section is devoted to possible future work on this topic. Finally it gives a summary of the gained knowledge and the project.

\subsection{Implementation}

\subsubsection{Height tracing approach}

The height map, used in the second height tracing scene, is an externally post-processed texture, based on a dynamically generated height map of the scene. The texture is used in order to be able to easily adjust the contrast of the height values to discover the setting for the best result. It has shown, that the height map approach yields the best shadow result, if the the range of the color values in the texture is optimally used. Therefore, the topmost, respectively the bottommost, extents of the geometry has to be as close as possible to the near clipping plane, respectively the far clipping plane, of the projection volume.

High frequency edges in the height map, as caused by cube-like objects, are affecting the smoothness of the shadow edges. However, no deeper research has been done on this topic. Therefore, the exact parameters are yet unknown. Unlike originally stated in my bachelor's thesis, this behavior renders the whole idea useless for urban scenes.

The previously described information-lost-problem can be confirmed. The cast shadows in the first height tracing scene are getting longer the higher the cubes in the scene are moved up.


\subsubsection{Volume tracing approach}

The light position in the scene appears to be wrong. The specular highlights ought to be displayed between the light source (the white dot) and the center of the scene. The reason for this is probably a falsely specified value in the shader program. The false light position can be indeed the reason for the wrong direction of the cast shadow, too. Since this error affects only the correct position of the cast shadow, it is classified as a minor issue, however it has a huge impact on the correctness of the result. Therefore, this issue has to be solved thoroughly in a later project.

Instead of converting the geometry coordinates into sampling coordinates and using them to access the volume texture, the original texture coordinates of the geometry coordinates are used for this purpose. This simplification doesn't affect the produced result, for the ground surface is flat and centered to the origin $\vec{o} = \vec{0}$ of the scene. It is responsible for the shadow artifacts on the cube in the center of the scene, though.


\subsection{Strengths and Limitations}

Projective aliasing can occur, if the tangent plane of a surface is parallel to the light vector. In this case, the information about this surface can't be restored from the shadow map and shadow artifacts appear on this part of the surface (Fig. \ref{fig:ProjektiveAliasing}). Since this information is preserved, the described approach might not to be prone to projective aliasing.
\begin{figure}[htbp]
	\centering
		\includegraphics[width=1.00\textwidth]{images/ProjektiveAliasing.jpg}
	\caption{Demonstration of projective aliasing (Image \url{https://msdn.microsoft.com/en-us/library/windows/desktop/ee416324(v=vs.85).aspx}).}
	\label{fig:ProjektiveAliasing}
\end{figure}
Further, the accuracy of the resulting shadow can be adjusted by the number of samples used in the algorithm. This enables easy adjustable performance scaling. Under certain circumstances, the volume map needs far less memory than other approaches. In the implemented example, only $20 \cdot 20 \cdot 20 = 8000$ pixels in the volume map are required. As mentioned, the silhouette of the shadow is determined by the number of samples of the light vector. Shadow mapping with a shadow map of similar size, that is a texture size of roughly $90 \times 90$ pixels, would yield a way more blocky result. However, the size of the volume map will rapidly increase, if objects move around. Due to the sampling theorem, the 3D space of the scene has to be quantized with a grid size of at least the size of the minimum difference between two subsequent positions of an object.

The height map approach is limited by the gradient's slope, which must not exceed a certain value, otherwise the shadows appear blocky. Since the original \ac{POM} paper \citep{pomTatarchuk} states, that the algorithm also works for high frequency profiles, it has yet to be clarified, what causes this behavior. Also, the resolutions for the height and volume map may be difficult to determine, since it depends on the minimum difference between two positions, as mentioned earlier. It is also difficult to quickly render the volumes of more sophisticated geometry to the texture. So far, the geometry in the scene is approximated by simple bounding boxes. 


\subsection{Performance}

The test application was executed on a Asus GL551JK Notebook. The hardware of the notebook is:
\begin{itemize}
	\item Intel i7 4700HQ 2.4 GHz
	\item NVidia GeForce GTX 850M
	\item 16 GB RAM
	\item Windows 8.1
\end{itemize}

When comparing the \ac{POM} scene (\emph{TestScene}) with the height tracing approaches (\emph{POMShadowScene} and \emph{CurveySurfaceScene}), the frame rates of both scenes are quite similar. Also, they turned out to be very low. The performance of the algorithm, however, is directly linked to the number of samples used in the algorithm and, for good quality and high accuracy, the number of samples in this implementation is high. By reducing the number of samples, the frame rates could be improved. However, considering sole shadow casting, this approach probably can't compete with classic shadow mapping algorithms. 

The volume tracing approach (\emph{VolumeTracingShadowScene}) shows a quite low frame rate as well. This seems to be related to the alternation of ownership of the volume texture between the CUDA and OpenGL context. In the CUDA interoperability scene (\emph{CudaTextureScene}) an inverted texture is created with CUDA. The following OpenGL rendering pass in this scene only maps a texture onto a surface, so no costly computations are performed, but nevertheless, the frame rate in this scene is low, too. Doubling the size of the volume map in the scene with the volume tracing approach (\emph{VolumeTracingShadowScene}) has only little impact on the frame rate. Neither CUDA nor OpenGL seems to be the bottle neck, so it stands to reason that the alternation of the object ownership between the drivers is responsible for the drop of the frame rate.


\subsection{Future work}

This section contains a list of major and minor tasks, which should be solved in possible future projects.

\begin{itemize}
	\item In the height map approach, find a function to automatically determine the dimension of the projection volume of the orthographic projection. A minimum bounding box for the whole scene should yield the right parameters. A suitable bounding box function should be contained in any scene graph implementation.
	
	\item Develop an algorithm and implement a CUDA kernel for volume maps of more sophisticated scene geometry.
	
	\item Find an algorithm to determine the resolution of the volume map automatically. Perhaps, some scene graph functions may be useful to achieve this.
	
	\item The volume map approach lacks of a soft shadow edge implementation. Implement a shadow algorithm for soft shadow edges.
	
	\item In the volume map approach there is a bug with the light direction. Probably, this bug is caused by the wrong light position.
	
	\item When using a point light instead of a directional light in the volume map approach, a perspective distortion of the shadow edge becomes visible. Find a equalizing function to solve this problem.
	
	\item Proof the assumption, that this approach is not prone to projective aliasing. 
	
	\item Thoroughly compare the performance of the height and volume map approach to existing shadow mapping techniques. Also do some research on how to improve the performance with CUDA, like a smarter load balancer for assigning threads, and OpenGL, like future API changes to improve alternation of ownership. %\todo{change this a bit in a way that it expresses that measurement has to be done in a more thoroughly way than it has been done so far}
	
	%\find an automatic bounding volume function for the orthographic projection in 2D approach.
	%\item implement a CUDA kernel for several, more complex volumes in 3D approach.
	%\item find an automatic determination function for volume map resolution in 3D approach.
	%\item implement soft shadows in 3D approach.
	%\item fix the light direction bug in 3D approach.
	%\item find a function to avoid perspective distortion when using a point light in 3D approach.
\end{itemize}


\subsection{Summary}

The results of this project have confirmed the idea of this shadow algorithm. Both, the height map and the volume map approach can be used to produce shadows in a global 3D scene. The approach might even solve the problem with projective aliasing. However, it could be difficult to find suitable resolution parameters of the textures, because it requires a thorough analysis of the global scene, including its dynamic setup, like movement and animation. Beyond that, the project's code base in a good condition and there are several open tasks left. It should be even compatible with Linux. 

The performance of the described approaches is quite limited, though. It requires further optimizations and, in particular for the volume tracing approach, improvements or enhancements on existing APIs.

In conclusion, it is reasonable to further improve this idea in a future project.