\section{Implementation}

To verify the approaches, an interactive test application with CUDA and OpenGL was implemented. The techniques can be applied even on lower-end machines, since no sophisticated GPU functions are required. The implemented shader programs require at least shader model 3.0. The architecture of the the test application is based on the application of the previous students research project. To meet a new requirement, the compatibility to Linux, the windowing system has been abstracted by GLFW. To combine GLFW with the existing code, only the main application and minor parts of the existing scenes had to be rewritten. A dispatcher object serves as interface between GLFW and the existing scenes. The previously implemented set of scenes contains a \ac{POM} implementation with a cobblestone cube (\emph{TestScene}), a shadow mapping scene (\emph{SMScene}), a scene, which addresses the generation of a height map and the correct conversion of the geometry coordinates into sampling coordinates (\emph{HeightShadowScene}), a interoperability test between CUDA and OpenGL (\emph{CudaTextureScene}), and finally a scene, in which CUDA is used to generate a volume map and OpenGL to verify the result (\emph{VolumeTracingScene}).

In this project, three further scenes were added to the list. The first scene shows two cubes on a surface and implements the height map approach (\emph{POMShadowScene}). The second scene implements the same approach but shows a curvy surface (\emph{CurvySurfaceScene}). The last scene implements the volume map approach and shows a simple cube in the center of a scene (\emph{VolumeTracingShadowScene}).

To get a feeling for, at least, the dimension of the performance, a frame counter measures the frames per second and displays them in the title bar of the window. However, due to the conditions in the scenes further effects, like scheduling, the shown result is more a trend than an exact value. 


\subsection{Height tracing approach}

The test application includes two scenes, which implement the height tracing approach. In the first scene two cubes are hovering over a surface (Fig. \ref{fig:heighttracingscenes}, left). The height of the cubes and the direction of the light can be dynamically adjusted. To generate the height map in this scene, a $1024x1024$ sized greyscale texture is attached to a frame buffer object. This frame buffer object is used as render target during the height map render pass. To dynamically generate the height map of the scene, the observer position is placed to the center of the scene and the view direction is facing in direction of the negative y axis. The depth value $d$ is calculated in a separate shader subroutine based on the fragment coordinate $f$ as shown in formula \ref{f:heightvalues}. After this pass, the greyscale texture contains the height map of the scene and is used as a resource in the following \ac{POM} stage.
\begin{align}\label{f:heightvalues}
	d &= 1 - \frac{f_z}{f_w}
\end{align}
\begin{figure}[htbp]
	\centering
		\includegraphics[width=1.00\textwidth]{images/heighttracingscenes.png}
	\caption{Shadow casting scenes, which are implementing the height tracing approach.}
	\label{fig:heighttracingscenes}
\end{figure}
The second scene shows a hilly surface. In this scene the algorithm yields soft shadow edges, due to the reduced slope in the resulting height map (Fig. \ref{fig:heighttracingscenes}, right). As in the first scene, the light direction can be dynamically adjusted. Instead of a dynamically generated height map, an externally post-processed texture is used in this scene, to discover more about the necessary contrast of the texture. 
Two major assumptions can be derived from this scenes:
\begin{enumerate}
	\item the $\delta$ between two height values has to be greater than a certain $\varepsilon$, since no shadow is cast at all when the cubes are close to the ground surface.
	\item to achieve soft shadows, the slope of the height values must not exceed certain bounds. The slope of the height values created by the cubes on the surface appears to be too steep for soft shadow edges. A test with a precomputed height map, which contains height regions of different slopes, supports this hypothesis. 
\end{enumerate}

In summary, the implementation confirms the information-lost-problem and reveals two further issues, which will be adequately addressed in a later section.


\subsection{Volume tracing approach}

In the third new scene, the volume tracing approach is implemented. A cube is placed right to the center of a ground surface and the position of the dynamic light is shown as a white dot (Fig. \ref{fig:volumetracingscenes}). The rendering itself is split up into two parts, the creation of the volume map with CUDA and the actual rendering process with OpenGL. The length of an edge of the cube in the center of the scene is unit length, the length of an edge of the ground surface is 10 units of length. Since the cube has been placed right to the center of the ground surface, the range of its coordinates is therefore $[-\frac{1}{2}, \frac{1}{2}]$ on the x and z axis. In order to sample the whole scene correctly, the dimension of the volume texture has to be on the x and z axis at least 20 units of length. Vertically, meaning the y dimension, any value greater than 1 would suffice, because for testing reasons the volume of the cube isn't moved in the scene. In this implementation, the dimension of the volume map has been set to $20 \times 20 \times 20$.

The number of threads is set to $10 \times 10 \times 10$, because the total number of threads for most GPUs is 1024 (returned by the CUDA API \emph{cudaGetDeviceProperties} function). Therefore, every thread on the GPU has to process 2 Voxels in the volume map. The CUDA kernel function receives the texture object, the dimension of the texture and a list of float values. In the test application a simple bounding box suffice to accurately describe the bounding volume of the cube in the scene. A bounding box can be defined by two 3D vectors, which contain the extrema of the bounding box, so every six float values in the mentioned list form a bounding box in the scene. Because there is only one cube in the scene, the kernel assumes only six float values in the list. Since the coordinates in the CUDA kernel are specified in a volume space coordinate system, the coordinates of the geometry volumes have to be converted into volume space before they are forwarded to the CUDA kernel. In the kernel itself the block index, the block dimension and the thread index are used to calculate the volume space coordinates of the currently processed voxel of the volume map. A simple point-in-bounding-volume-test determines the color value $c$ of the voxel in the volume map, according to formula \ref{f:volumecolor}. 
\begin{align}\label{f:volumecolor}
	c &= \begin{cases}
	1 & \text{if true}\\
	0 & \text{otherwise}
	\end{cases}
\end{align}

The generated volume map is then passed as a 3D texture to the shader program. Like mentioned in formula \ref{f:texturetransform}, the texture coordinates to sample the volume map can be directly derived from the vertex coordinates. In this example, however, the texture coordinates $t$, already provided by the model, are used instead for simplification reasons. Before a lighting model can be applied, a pixel has to be determined as lit or unlit. Starting from $t$, the volume map has to be sampled along the light vector $l$. In several iterations $t$ is offset by discrete steps of $l$ until the light source is reached or the sampled value corresponds to a volume. The latter case means, the pixel is unlit, since there is an occluder between the pixel and the light source. The illumination of the pixel is expressed as a float value $f$ in the range $[0, 1]$, whereby $1$ means fully lit. In this scene, the Phong lighting model is used. To render shadows onto the surface, the diffuse and specular light color component of the lighting model is multiplied by $f$. 

\begin{figure}[htbp]
	\centering
		\includegraphics[width=1.00\textwidth]{images/volumetracingscenes.png}
	\caption{The scene with the volume tracing approach from different angles.}
	\label{fig:volumetracingscenes}
\end{figure}

Varying the number of samples $n_s$ along the light vector revealed, that the accuracy of the shadow silhouette is directly related to $n_s$. 
