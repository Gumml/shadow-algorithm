#version 410


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 normal;

	vec2 uv;

	vec3 sampleposition;
};


struct Textures
{
	sampler2D heightmap;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;

uniform vec4 surfacecolor;

subroutine uniform FragmentProgram fragmentprogram;

out vec4 outColor;
out float depth;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void renderpass()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	float shadowfactor = 1.f;


	//vec2 dx = dFdx(Input.sampleposition.xy);
	//vec2 dy = dFdy(Input.sampleposition.xy);

	//// Shadow
	//float heightScale = 1.0f;
	//float shadowLength = 10.f;
	////vec2 lightRay = Input.lightdir.xy * heightScale * shadowLength;
	//vec2 lightRay = nLightdir.xz * heightScale * shadowLength;

	//float shadowSoftening = 1.0f;

	//float sh0 = textureGrad(textures.heightmap, Input.sampleposition.xz, dx, dy).r;
	//float shA = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .88f, dx, dy).r - sh0 - .88f) * 1.f * shadowSoftening;
	//float sh9 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .77f, dx, dy).r - sh0 - .77f) * 2.f * shadowSoftening;
	//float sh8 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .66f, dx, dy).r - sh0 - .66f) * 4.f * shadowSoftening;
	//float sh7 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .55f, dx, dy).r - sh0 - .55f) * 6.f * shadowSoftening;
	//float sh6 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .44f, dx, dy).r - sh0 - .44f) * 8.f * shadowSoftening;
	//float sh5 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .33f, dx, dy).r - sh0 - .33f) * 10.f * shadowSoftening;
	//float sh4 = (textureGrad(textures.heightmap, Input.sampleposition.xz + lightRay * .22f, dx, dy).r - sh0 - .22f) * 12.f * shadowSoftening;

	//shadowfactor = 1.f - max(max(max(max(max(max(shA, sh9), sh8), sh7), sh6), sh5), sh4);
	//shadowfactor = clamp(shadowfactor * .6f + .2f, 0.0f, 1.f); // clamp-Intervall an ambient Light anpassen.


	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * shadowfactor * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * shadowfactor * light.specular;
	}

	outColor = incidentalLight * surfacecolor + reflectedLight;
	//outColor = incidentalLight * texture(textures.heightmap, Input.uv) + reflectedLight;

	outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) + reflectedLight;

	//outColor = incidentalLight * texture(textures.heightmap, samplePoint) + reflectedLight;
	//outColor = vec4(Input.modelposition, 1);
}

subroutine (FragmentProgram) void heightmap()
{
	float depth = 1 - (gl_FragCoord.z / gl_FragCoord.w);
	//float depth = gl_FragCoord.z / gl_FragCoord.w;
	outColor = vec4(depth, depth, depth, 1.f);
}
// =============================================================================================================
