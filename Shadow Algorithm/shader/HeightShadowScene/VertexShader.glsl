#version 410

// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;
	mat4 m;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct VertexOutput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 normal;

	vec2 uv;

	vec3 sampleposition;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void VertexProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

uniform Matrices matrices;
uniform Lighting light;

subroutine uniform VertexProgram vertexprogram;

layout(location = 0) out VertexOutput Output;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	vertexprogram();	
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (VertexProgram) void renderpass()
{
	gl_Position = matrices.mvp * vertex;

	Output.normal = normalize(matrices.normal * normal);

	vec4 hvec = matrices.mv * vertex;
	vec3 mvPos = hvec.xyz / hvec.w;

	Output.lightdir = light.position - mvPos;
	Output.viewdir = -mvPos;


	hvec = matrices.m * vertex;
	vec3 modelposition = hvec.xyz / hvec.w;
	modelposition /= 10.f;
	modelposition += vec3(.5f, .0f, .5f);
	Output.sampleposition.x = modelposition.x;
	Output.sampleposition.y = modelposition.y;
	Output.sampleposition.z = 1 - modelposition.z;


	Output.uv = uv;
}


subroutine (VertexProgram) void heightmap()
{
	gl_Position = matrices.mvp * vertex;
	Output.uv = uv;
}
// =============================================================================================================