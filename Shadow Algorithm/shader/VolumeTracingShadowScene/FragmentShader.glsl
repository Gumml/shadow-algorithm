#version 440


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 lightvec;

	vec3 normal;

	vec2 uv;

	vec3 coord;
};


struct Textures
{
	sampler3D volume;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;

uniform vec4 surfacecolor;
uniform float layer;

subroutine uniform FragmentProgram fragmentprogram;

out vec4 outColor;
out float depth;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void renderpass()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * light.specular;
	}

	outColor = incidentalLight * surfacecolor + reflectedLight;
}


subroutine (FragmentProgram) void renderpassWithTexture()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * light.specular;
	}


	vec4 volumecolor = texture(textures.volume, vec3(Input.uv, layer));

	//outColor = incidentalLight * (surfacecolor + volumecolor) + reflectedLight;
	outColor = incidentalLight * (volumecolor) + reflectedLight;
}


subroutine (FragmentProgram) void renderpassWithShadow()
{
	//float samples = 256.f;
	float samples = 1024.f;

	float lit = 1.f;

	float n = 0.f;

	for(n = 0.f; n <= samples; ++n)
	{
		float stepsize = n / samples;
		//vec3 offset = stepsize * normalize(Input.lightvec);
		vec3 offset = stepsize * Input.lightvec;
		//vec3 offset = stepsize * normalize(Input.lightdir);
		//vec3 offset = stepsize * Input.lightdir;

		vec4 volume = texture(textures.volume, vec3(Input.uv.x, 0.f, Input.uv.y) + offset);
		if(volume.r > .8f)
		{
			lit = 0.2f;
			break;
		}
	}


	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * lit * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
    float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
	reflectedLight = s * lit * light.specular;


	outColor = incidentalLight * (surfacecolor) + reflectedLight;



	//outColor = surfacecolor * lit;
	//outColor = texture(textures.volume, vec3(Input.uv, layer) + float(0) / float(samples) * Input.lightdir);
	//outColor = texture(textures.volume, vec3(Input.uv, 0) + float(1) / float(samples) * Input.lightdir);
	//outColor = texture(textures.volume, vec3(Input.uv, layer));
	//outColor = texture(textures.volume, vec3(Input.uv.x, layer, Input.uv.y));
	//outColor = vec4(Input.lightdir, 1);
	//outColor = vec4(Input.lightvec, 1);
	//outColor = vec4(20 / samples * normalize(Input.lightdir), 1);
	//outColor = vec4(normalize(light.position - Input.coord), 1);
	//outColor = vec4(vec3(Input.uv.s, Input.coord.y, Input.uv.t), 1);
	//outColor = vec4(Input.coord, 1);
	//outColor = vec4(Input.uv, 0, 1);
	//outColor = vec4((normalize(Input.lightdir) * .5f) + .5f, 1);
	//outColor = vec4(vec3(g), 1);
	//outColor = vec4(vec3(lit), 1);
	//outColor = litvec;
	//outColor = vec4(vec3(20/samples), 1);

	
}


subroutine (FragmentProgram) void renderDot()
{
	vec2 coord = gl_PointCoord - vec2(0.5f);
	if(length(coord) > .5f)
		discard;
	outColor = surfacecolor;
}
// =============================================================================================================
