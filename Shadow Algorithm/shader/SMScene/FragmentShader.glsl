#version 410


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;
	mat4 shadow;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 normal;
	vec2 uv;

	vec4 fragmentPosition;
};


struct Textures
{
	sampler2DShadow shadowmap;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;

uniform vec4 surfacecolor;

subroutine uniform FragmentProgram fragmentprogram;

out vec4 outColor;
out float depth;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void colored()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);

	//float shadow = 1.f;
	//if(texture(textures.shadowmap, Input.fragmentPosition.xy).r < Input.fragmentPosition.z)
	//	shadow = .2f;
	float shadowvalue = texture(textures.shadowmap, Input.fragmentPosition.xyz);
	float shadow = 1.f - .8f * (1.f - shadowvalue);

	vec4 incidentalLight = vec4(0.0f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * shadow * light.diffuse);

	//outColor = texture(textures.shadowmap, Input.uv); //vec4(Input.uv, 0, 1);//vec4(texture(textures.shadowmap, gl_FragCoord.xy).xxx, 1); //texture(textures.shadowmap, Input.fragmentPosition.xy); //incidentalLight * surfacecolor;
	outColor = incidentalLight * surfacecolor;
}


subroutine (FragmentProgram) void noShadow()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);

	vec4 incidentalLight = vec4(0.0f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * light.diffuse);

	outColor = incidentalLight * surfacecolor;
}


subroutine (FragmentProgram) void shadowmap()
{
	depth = gl_FragCoord.z / gl_FragCoord.w;
}
// =============================================================================================================
