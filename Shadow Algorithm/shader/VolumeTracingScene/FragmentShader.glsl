#version 410


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 normal;

	vec2 uv;
};


struct Textures
{
	sampler3D volume;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;

uniform vec4 surfacecolor;
uniform float layer;

subroutine uniform FragmentProgram fragmentprogram;

out vec4 outColor;
out float depth;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void renderpass()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * light.specular;
	}

	outColor = incidentalLight * surfacecolor + reflectedLight;
}


subroutine (FragmentProgram) void renderpassWithTexture()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * light.specular;
	}


	vec4 volumecolor = texture(textures.volume, vec3(Input.uv, layer));

	//outColor = incidentalLight * (surfacecolor + volumecolor) + reflectedLight;
	outColor = incidentalLight * (volumecolor) + reflectedLight;
}
// =============================================================================================================
