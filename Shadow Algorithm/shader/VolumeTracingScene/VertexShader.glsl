#version 410

// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct VertexOutput
{
	vec3 lightdir;
	vec3 viewdir;

	vec3 normal;

	vec2 uv;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void VertexProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

uniform Matrices matrices;
uniform Lighting light;

subroutine uniform VertexProgram vertexprogram;

layout(location = 0) out VertexOutput Output;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	vertexprogram();	
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (VertexProgram) void renderpass()
{
	gl_Position = matrices.mvp * vertex;

	Output.normal = matrices.normal * normal;

	vec4 hvec = matrices.mv * vertex;
	vec3 mvPos = hvec.xyz / hvec.w;

	Output.lightdir = light.position - mvPos;

	Output.viewdir = -mvPos;
}


subroutine (VertexProgram) void renderpassWithTexture()
{
	gl_Position = matrices.mvp * vertex;

	Output.normal = matrices.normal * normal;

	vec4 hvec = matrices.mv * vertex;
	vec3 mvPos = hvec.xyz / hvec.w;

	Output.lightdir = light.position - mvPos;

	Output.viewdir = -mvPos;

	Output.uv = uv;
}
// =============================================================================================================