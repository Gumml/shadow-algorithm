#version 410


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdirT;
	vec3 viewdirT;

	vec3 lightdir;
	vec3 viewdir;

	vec2 parallaxOffsetT;

	vec3 normal;

	vec2 uv;

	vec3 sampleposition;
};


struct Textures
{
	sampler2D heightmap;
};


struct ParallaxOcclusionParams
{
	float heightScale;
	float heightBias;
	
	float textureScale;
	
	int minSamples;
	int maxSamples;

	vec2 textureDimension;

	float shadowLength;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;

uniform vec4 surfacecolor;

subroutine uniform FragmentProgram fragmentprogram;

ParallaxOcclusionParams poms;

out vec4 outColor;
out float depth;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void renderpass()
{
	poms.heightScale = 0.04f;
	poms.heightBias = 0.02f;
	poms.textureScale = 1.f;
	poms.minSamples = 200;
	poms.maxSamples = 600;
	poms.textureDimension = vec2(1024, 1024);
	poms.shadowLength = 15.f;



	vec3 nLightdirO = normalize(Input.lightdir);
    vec3 nViewdirO = normalize(Input.viewdir);
    vec3 nLightdirT = normalize(Input.lightdirT);
	vec3 nViewdirT = normalize(Input.viewdirT);
	vec3 nNormalO = normalize(Input.normal);



	//vec4 dxVec = dFdx(vec4(Input.uv * poms.textureDimension, Input.uv));
	//vec4 dyVec = dFdy(vec4(Input.uv * poms.textureDimension, Input.uv));
	//vec4 dxVec = dFdx(vec4(Input.sampleposition.xy * poms.textureDimension, Input.sampleposition.xy));
	//vec4 dyVec = dFdy(vec4(Input.sampleposition.xy * poms.textureDimension, Input.sampleposition.xy));

	vec4 dxVec = dFdx(vec4(Input.sampleposition.xz * poms.textureDimension, Input.sampleposition.xz));
    vec4 dyVec = dFdy(vec4(Input.sampleposition.xz * poms.textureDimension, Input.sampleposition.xz));

	//vec2 dx = dxVec.zw, dy = dyVec.zw;
	vec2 dx = dxVec.xy, dy = dyVec.xy;
	
	//vec2 texSample = Input.uv;
	//vec2 texSample = Input.sampleposition.xy;
	vec2 texSample = Input.sampleposition.xz;
	float occlusionShadow = .5f;

	int numSteps = int(mix(poms.maxSamples, poms.minSamples, dot(nViewdirO, nNormalO)));

	float currentHeight = 0.f;
	float stepSize = 1.f / numSteps;
	float previousHeight = 1.f;
	float nextHeight = 0.f;

	int stepIndex = 0;

	vec2 texOffsetPerStep = stepSize * Input.parallaxOffsetT;
	vec2 currentTexOffset = Input.uv;

	float currentBound = 1.f;
	float parallaxAmount = 0.f;

	vec2 pt1 = vec2(0);
	vec2 pt2 = vec2(0);
	vec2 texOffset2 = vec2(0.f);

	while(stepIndex < numSteps)
	{
		currentTexOffset -= texOffsetPerStep;

		currentHeight = textureGrad(textures.heightmap, poms.textureScale * currentTexOffset, dx, dy).r;

		currentBound -= stepSize;

		if(currentHeight > currentBound)
		{
			pt1 = vec2(currentBound, currentHeight);
			pt2 = vec2(currentBound + stepSize, previousHeight);

			texOffset2 = currentTexOffset - texOffsetPerStep;
			stepIndex = numSteps + 1; // break
			previousHeight = currentHeight;
		}
		else
		{
			stepIndex++;
			previousHeight = currentHeight;
		}
	}

	float delta1 = pt1.x - pt1.y;
	float delta2 = pt2.x - pt2.y;

	float denom = delta2 - delta1;

	if(denom == 0.f)
		parallaxAmount = 0.f;

	else
		parallaxAmount = (pt1.x * delta2 - pt2.x * delta1) / denom;

	vec2 parallaxOffset = Input.parallaxOffsetT * (1 - parallaxAmount);

	vec2 texSampleBase = Input.uv - parallaxOffset;

	texSample = texSampleBase;


	// Shadow
	vec2 lightRay = nLightdirT.xy * poms.heightScale * poms.shadowLength;

	float shadowSoftening = .5f;

	float sh0 = textureGrad(textures.heightmap, poms.textureScale * texSample, dx, dy).r;
	float shA = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .88f, dx, dy).r - sh0 - .88f) * 1.f * shadowSoftening;
	float sh9 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .77f, dx, dy).r - sh0 - .77f) * 2.f * shadowSoftening;
	float sh8 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .66f, dx, dy).r - sh0 - .66f) * 4.f * shadowSoftening;
	float sh7 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .55f, dx, dy).r - sh0 - .55f) * 6.f * shadowSoftening;
	float sh6 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .44f, dx, dy).r - sh0 - .44f) * 8.f * shadowSoftening;
	float sh5 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .33f, dx, dy).r - sh0 - .33f) * 10.f * shadowSoftening;
	float sh4 = (textureGrad(textures.heightmap, poms.textureScale * texSample + lightRay * .22f, dx, dy).r - sh0 - .22f) * 12.f * shadowSoftening;

	occlusionShadow = 1.f - max(max(max(max(max(max(max(shA, sh9), sh8), sh7), sh6), sh5), sh4), sh0);
	
	occlusionShadow = clamp(occlusionShadow * .6f + .2f, 0.3f, 1.f); // clamp-Intervall an ambient Light anpassen.
	//occlusionShadow = clamp(occlusionShadow * .3f, 0.0f, 1.f); // clamp-Intervall an ambient Light anpassen.
	//occlusionShadow = 1.f;




	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormalO, nLightdirO));
	incidentalLight = light.ambient + (d * occlusionShadow * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdirO, nNormalO));
		float s = pow(max(0.f, dot(nViewdirO, reflectionVector)), light.shininess);
		reflectedLight = s * occlusionShadow * light.specular;
	}

	outColor = incidentalLight * surfacecolor + reflectedLight;
	//outColor = surfacecolor * occlusionShadow;
	//outColor = vec4(occlusionShadow, occlusionShadow, occlusionShadow, 1);
	//outColor = texture(textures.heightmap, Input.sampleposition.xz);
	//outColor = texture(textures.heightmap, Input.uv);
	//outColor vec4(Input.sampleposition.xy, 0, 1);
	//outColor = vec4(Input.sampleposition, 1);
	//outColor = texture(textures.heightmap, Input.uv);

	//outColor = incidentalLight * texture(textures.heightmap, Input.uv) + reflectedLight;

	//outColor = texture(textures.heightmap, Input.sampleposition.xz);
	//outColor = vec4(dy, 0, 1);
	//outColor = vec4(Input.uv, 0, 1);

	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) + reflectedLight;
	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) * surfacecolor + reflectedLight;
	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) * surfacecolor;

	//outColor = incidentalLight * texture(textures.heightmap, samplePoint) + reflectedLight;
	//outColor = vec4(occlusionShadow, occlusionShadow, occlusionShadow, 1);

	//outColor = vec4(Input.uv, 0, 1);
}


subroutine (FragmentProgram) void accesspass()
{
	vec3 nNormal = normalize(Input.normal);
	vec3 nLightdir = normalize(Input.lightdir);
	vec3 nViewdir = normalize(Input.viewdir);

	float shadowfactor = 1.f;

	vec4 incidentalLight = vec4(0.f);
	float d = max(0.f, dot(nNormal, nLightdir));
	incidentalLight = light.ambient + (d * shadowfactor * light.diffuse);

	vec4 reflectedLight = vec4(0.f);
	if(d > 0.f)
	{
		vec3 reflectionVector = normalize(reflect(-nLightdir, nNormal));
		float s = pow(max(0.f, dot(nViewdir, reflectionVector)), light.shininess);
		reflectedLight = s * shadowfactor * light.specular;
	}

	//outColor = incidentalLight * surfacecolor + reflectedLight;
	//outColor = surfacecolor;
	//outColor = incidentalLight * texture(textures.heightmap, Input.uv) + reflectedLight;

	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) + reflectedLight;

	//outColor = texture(textures.heightmap, Input.sampleposition.xz);
	outColor = texture(textures.heightmap, Input.uv);

	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) * surfacecolor + reflectedLight;
	//outColor = incidentalLight * texture(textures.heightmap, Input.sampleposition.xz) * surfacecolor;
}


subroutine (FragmentProgram) void heightmap()
{
	float depth = 1 - (gl_FragCoord.z / gl_FragCoord.w);
	//float depth = gl_FragCoord.z / gl_FragCoord.w;
	outColor = vec4(depth, depth, depth, 1.f);
}
// =============================================================================================================
