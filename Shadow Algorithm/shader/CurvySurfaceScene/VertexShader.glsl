#version 410

// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;
	mat4 m;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct ParallaxOcclusionParams
{
	float heightScale;
	float heightBias;
	
	float textureScale;
	
	int minSamples;
	int maxSamples;

	vec2 textureDimension;

	float shadowLength;
};


struct VertexOutput
{
	vec3 lightdirT;
	vec3 viewdirT;

	vec3 lightdir;
	vec3 viewdir;

	vec2 parallaxOffsetT;

	vec3 normal;

	vec2 uv;

	vec3 sampleposition;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void VertexProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

uniform Matrices matrices;
uniform Lighting light;

subroutine uniform VertexProgram vertexprogram;

ParallaxOcclusionParams poms;

layout(location = 0) out VertexOutput Output;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	vertexprogram();	
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (VertexProgram) void renderpass()
{
	poms.heightScale = 0.02f;
	poms.heightBias = 0.01f;
	poms.textureScale = 1.f;
	poms.minSamples = 200;
	poms.maxSamples = 600;
	poms.textureDimension = vec2(1024, 1024);
	poms.shadowLength = 25.f;


	gl_Position = matrices.mvp * vertex;

	Output.normal = normalize(matrices.normal * normal);
	vec3 nTangent = normalize(matrices.normal * vec3(1, 0, 0));
	vec3 nBitangent = normalize(matrices.normal * vec3(0, 0, -1));

	vec4 hvec = matrices.mv * vertex;
	vec3 mvPos = hvec.xyz / hvec.w;

	Output.lightdir = light.position - mvPos;
	Output.viewdir = -mvPos;


	hvec = matrices.m * vertex;
	vec3 modelposition = hvec.xyz / hvec.w;
	modelposition /= 10.f;
	modelposition += vec3(.5f, .0f, .5f);
	Output.sampleposition.x = modelposition.x;
	Output.sampleposition.y = modelposition.y;
	Output.sampleposition.z = 1 - modelposition.z;
	//Output.sampleposition = modelposition;
	//Output.sampleposition = vec3(1);


	mat3 toTangentSpace = transpose(mat3(nTangent, nBitangent, Output.normal));

	Output.lightdirT = toTangentSpace * Output.lightdir;
	Output.viewdirT = toTangentSpace * Output.viewdir;

	vec2 parallaxDirection = normalize(Output.viewdirT.xy);
	float l = length(Output.viewdirT);
	float parallaxLength = sqrt(l * l - Output.viewdirT.z * Output.viewdirT.z) / Output.viewdirT.z;

	Output.parallaxOffsetT = parallaxDirection * parallaxLength * poms.heightScale;

	Output.uv = uv;
}


subroutine (VertexProgram) void accesspass()
{
	gl_Position = matrices.mvp * vertex;

	Output.normal = normalize(matrices.normal * normal);

	vec4 hvec = matrices.mv * vertex;
	vec3 mvPos = hvec.xyz / hvec.w;

	Output.lightdir = light.position - mvPos;
	Output.viewdir = -mvPos;


	hvec = matrices.m * vertex;
	vec3 modelposition = hvec.xyz / hvec.w;
	modelposition /= 10.f;
	modelposition += vec3(.5f, .0f, .5f);
	Output.sampleposition.x = modelposition.x;
	Output.sampleposition.y = modelposition.y;
	Output.sampleposition.z = 1 - modelposition.z;


	Output.uv = uv;


	Output.lightdirT = vec3(0);
	Output.viewdirT = vec3(0);
	Output.parallaxOffsetT = vec2(0);
}



subroutine (VertexProgram) void heightmap()
{
	gl_Position = matrices.mvp * vertex;
	Output.uv = uv;
}
// =============================================================================================================