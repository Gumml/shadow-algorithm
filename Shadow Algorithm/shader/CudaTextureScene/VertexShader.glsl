#version 410

// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
};


struct VertexOutput
{
	vec2 uv;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void VertexProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in vec4 vertex;
layout(location = 2) in vec2 uv;

uniform Matrices matrices;

subroutine uniform VertexProgram vertexprogram;

layout(location = 0) out VertexOutput Output;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	vertexprogram();	
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (VertexProgram) void cudatexture()
{
	gl_Position = matrices.mvp * vertex;

	Output.uv = uv;
}
// =============================================================================================================