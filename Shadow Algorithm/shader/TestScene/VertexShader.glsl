#version 410

// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct VertexOutput
{
	vec3 lightdirT;
	vec3 viewdirT;

	vec3 lightdirO;
	vec3 viewdirO;

	vec2 parallaxOffsetT;

	vec3 normal;

	vec2 uv;
};


struct ParallaxOcclusionParams
{
	float heightScale;
	float textureScale;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void VertexProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

uniform Matrices matrices;
uniform Lighting light;
uniform ParallaxOcclusionParams poms;

subroutine uniform VertexProgram vertexprogram;

layout(location = 0) out VertexOutput Output;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	vertexprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (VertexProgram) void parallaxocclusion()
{
	gl_Position = matrices.mvp * vertex;

	Output.normal = normalize(matrices.normal * normal);
	vec3 nTangent = normalize(matrices.normal * tangent);
	vec3 nBitangent = normalize(matrices.normal * bitangent);

	vec4 help = matrices.mv * vertex;
	vec3 mvPosition = help.xyz / help.w;

	Output.lightdirO = light.position - mvPosition;
	Output.viewdirO = -mvPosition;
	
	Output.uv = uv;

	mat3 toTangentspace = transpose(mat3(nTangent, nBitangent, Output.normal));
	
	Output.lightdirT = toTangentspace * Output.lightdirO;
	Output.viewdirT = toTangentspace * Output.viewdirO;

	vec2 parallaxDirection = normalize(Output.viewdirT.xy);

	float len = length(Output.viewdirT);
	float parallaxLen = sqrt(len * len - Output.viewdirT.z * Output.viewdirT.z) / Output.viewdirT.z;

	Output.parallaxOffsetT = parallaxDirection * parallaxLen * poms.heightScale;
}
// =============================================================================================================