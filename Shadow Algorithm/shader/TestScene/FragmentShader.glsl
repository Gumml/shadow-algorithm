#version 410


// Structures
// =============================================================================================================
struct Matrices
{
	mat4 mvp;
	mat4 mv;

	mat3 normal;
};


struct Lighting
{
	vec3 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};


struct FragmentInput
{
	vec3 lightdirT;
	vec3 viewdirT;

	vec3 lightdirO;
	vec3 viewdirO;

	vec2 parallaxOffsetT;

	vec3 normal;

	vec2 uv;
};


struct Textures
{
	sampler2D diffuse;
	sampler2D normal;
	sampler2D height;
};


struct ParallaxOcclusionParams
{
	float heightScale;
	float heightBias;
	
	float textureScale;
	
	int minSamples;
	int maxSamples;

	vec2 textureDimension;

	float shadowLength;
};
// =============================================================================================================




// Subroutines
// =============================================================================================================
subroutine void FragmentProgram();
// =============================================================================================================




// Variables
// =============================================================================================================
layout(location = 0) in FragmentInput Input;

uniform Matrices matrices;
uniform Lighting light;
uniform Textures textures;
uniform ParallaxOcclusionParams poms;

subroutine uniform FragmentProgram fragmentprogram;

out vec4 out_color;
// =============================================================================================================




// Methods
// =============================================================================================================
void main()
{
	fragmentprogram();
}
// =============================================================================================================




// Subroutine Implementation
// =============================================================================================================
subroutine (FragmentProgram) void parallaxocclusion()
{
	vec3 nLightdirO = normalize(Input.lightdirO);
    vec3 nViewdirO = normalize(Input.viewdirO);
    vec3 nLightdirT = normalize(Input.lightdirT);
	vec3 nViewdirT = normalize(Input.viewdirT);
	vec3 nNormalO = normalize(Input.normal);



	vec4 dxVec = dFdx(vec4(Input.uv * poms.textureDimension, Input.uv));
	vec4 dyVec = dFdy(vec4(Input.uv * poms.textureDimension, Input.uv));

	vec2 dx = dxVec.zw, dy = dyVec.zw;
	
	vec2 texSample = Input.uv;
	float occlusionShadow = 1.f;

	int numSteps = int(mix(poms.maxSamples, poms.minSamples, dot(nViewdirO, nNormalO)));

	float currentHeight = 0.f;
	float stepSize = 1.f / numSteps;
	float previousHeight = 1.f;
	float nextHeight = 0.f;

	int stepIndex = 0;

	vec2 texOffsetPerStep = stepSize * Input.parallaxOffsetT;
	vec2 currentTexOffset = Input.uv;

	float currentBound = 1.f;
	float parallaxAmount = 0.f;

	vec2 pt1 = vec2(0);
	vec2 pt2 = vec2(0);
	vec2 texOffset2 = vec2(0.f);

	while(stepIndex < numSteps)
	{
		currentTexOffset -= texOffsetPerStep;

		currentHeight = textureGrad(textures.height, poms.textureScale * currentTexOffset, dx, dy).r;

		currentBound -= stepSize;

		if(currentHeight > currentBound)
		{
			pt1 = vec2(currentBound, currentHeight);
			pt2 = vec2(currentBound + stepSize, previousHeight);

			texOffset2 = currentTexOffset-texOffsetPerStep;
			stepIndex = numSteps + 1; // break
			previousHeight = currentHeight;
		}
		else
		{
			stepIndex++;
			previousHeight = currentHeight;
		}
	}

	float delta1 = pt1.x - pt1.y;
	float delta2 = pt2.x - pt2.y;

	float denom = delta2 - delta1;

	if(denom == 0.f)
		parallaxAmount = 0.f;

	else
		parallaxAmount = (pt1.x * delta2 - pt2.x * delta1) / denom;

	vec2 parallaxOffset = Input.parallaxOffsetT * (1 - parallaxAmount);

	vec2 texSampleBase = Input.uv - parallaxOffset;

	texSample = texSampleBase;


	// Shadow
	vec2 lightRay = nLightdirT.xy * poms.heightScale * poms.shadowLength;

	float shadowSoftening = .5f;

	float sh0 = textureGrad(textures.height, poms.textureScale * texSample, dx, dy).r;
	float shA = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .88f, dx, dy).r - sh0 - .88f) * 1.f * shadowSoftening;
	float sh9 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .77f, dx, dy).r - sh0 - .77f) * 2.f * shadowSoftening;
	float sh8 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .66f, dx, dy).r - sh0 - .66f) * 4.f * shadowSoftening;
	float sh7 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .55f, dx, dy).r - sh0 - .55f) * 6.f * shadowSoftening;
	float sh6 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .44f, dx, dy).r - sh0 - .44f) * 8.f * shadowSoftening;
	float sh5 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .33f, dx, dy).r - sh0 - .33f) * 10.f * shadowSoftening;
	float sh4 = (textureGrad(textures.height, poms.textureScale * texSample + lightRay * .22f, dx, dy).r - sh0 - .22f) * 12.f * shadowSoftening;

	occlusionShadow = 1.f - max(max(max(max(max(max(shA, sh9), sh8), sh7), sh6), sh5), sh4);
	
	occlusionShadow = clamp(occlusionShadow * .6f + .2f, 0.0f, 1.f); // clamp-Intervall an ambient Light anpassen.




	vec3 nNormalT = normalize(texture(textures.normal, poms.textureScale * texSample).xyz * 2 - 1);
	//vec3 nNormalT = nNormalO;
	
	vec4 surfaceColor = texture(textures.diffuse, poms.textureScale * texSample); 
	//vec4 surfaceColor = vec4(0.f, .6f, .8f, 1.f);

	
	float d = max(0.0, dot(nLightdirT, nNormalT));
	vec4 incidentalLight = light.ambient + occlusionShadow * d * light.diffuse;

	vec4 reflectedLight = vec4(0.0f);
	if(d > 0.0f)
	{
		vec3 rVec = normalize(nViewdirT + nLightdirT);
		float s = pow(max(0.0f, dot(nNormalT, rVec)), light.shininess);
		reflectedLight = occlusionShadow * s * light.specular;
	}

	out_color = incidentalLight * surfaceColor + reflectedLight;
}
// =============================================================================================================