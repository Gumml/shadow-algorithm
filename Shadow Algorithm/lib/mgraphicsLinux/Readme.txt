Requires a version of the FBX SDK located relative to the base directory (which is where the mgraphics makefile is located):
	$(BaseDir)/../FBXSDK
	
To use the library you need to link

	- a version of FreeImage
	- a version of the FBXSDK library