#pragma once


// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <DataTypes.h>
//	===========================================================================================================================================================================================		#####



namespace mgraphics
{
	// Camera																																													   Camera
	//	=========================================================================================================================================================================================	#####
	class Camera
	{
		mgraphics::datatypes::Vector3 position;
		mgraphics::datatypes::Vector3 target;
		mgraphics::datatypes::Vector3 up;
		float upOrientation;

		void adjustUp();

	public:
		Camera(void);
		Camera(mgraphics::datatypes::Vector3 position, mgraphics::datatypes::Vector3 target);

		~Camera(void);

		void moveTo(mgraphics::datatypes::Vector3 newPosition);
		void moveTo(float x, float y, float z);

		void setTarget(mgraphics::datatypes::Vector3 newTarget);
		void setTarget(float x, float y, float z);

		void setViewDirection(mgraphics::datatypes::Vector3 viewDirection);
		void setViewDirection(float x, float y, float z);

		void setUpOrientation(float orientation);

		mgraphics::datatypes::Matrix4 getViewMatrix();

		void reset();
	};
	//	=========================================================================================================================================================================================	#####
}

