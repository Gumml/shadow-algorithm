#pragma once


// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <string>
#include <gl_core_4_4.hpp>
//	===========================================================================================================================================================================================		#####


namespace mgraphics
{
	namespace textures
	{
		// Texture																																												  Texture
		//	===================================================================================================================================================================================		#####
		class Texture2D
		{
			int width;
			int height;

			unsigned int textureID;

			const unsigned char *data;

		public:
			Texture2D();
			~Texture2D();

			void Generate(std::string filepath, bool autoMipmap = false);
			void Generate(unsigned char* imgData, int imgWidth, int imgHeight, int imgComponents, GLenum imgFormat, bool autoMipmap = false);

			int getWidth()
			{
				return width;
			}

			int getHeight()
			{
				return height;
			}

			unsigned int getTextureID()
			{
				return textureID;
			}

			friend class Texture2DStorage;
		};
		//	===================================================================================================================================================================================		#####



		// TextureStorage																																								   TextureStorage
		//	===================================================================================================================================================================================		#####
		class Texture2DStorage
		{
			static void Store(std::string path, char *data, int width, int height, int imgDepth);
		};
		//	===================================================================================================================================================================================		#####
	}
}


