// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <Camera.h>
//	===========================================================================================================================================================================================		#####



// Usings																																														   Usings
//	===========================================================================================================================================================================================		#####
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Matrix4;
//	===========================================================================================================================================================================================		#####



// Camera																																														   Camera
//	===========================================================================================================================================================================================		#####
mgraphics::Camera::Camera()
{
	position = Vector3::Zero;
	target = Vector3(0.f, 0.f, -1.f);
	up = Vector3::UnitY;
	upOrientation = 0.f;
}


mgraphics::Camera::Camera(Vector3 position, Vector3 target)
{
	this->position = position;
	this->target = target;
	upOrientation = 0.f;
	adjustUp();
}


mgraphics::Camera::~Camera()
{
	reset();
}


void mgraphics::Camera::adjustUp()
{
	Vector3 left;
	Vector3 yVec = Vector3::UnitY;

	Vector3 delta = target - position;
	delta.normalize();

	// Kreuzprodukt == (0, 0, 0) verhindern
	if(delta == yVec)
		up = Vector3::UnitZ;

	else if(delta == -yVec)
		up = -Vector3::UnitZ;

	else
	{
		left = yVec.Cross(delta);
		left.normalize();

		up = delta.Cross(left);
		up.normalize();
	}

	if(upOrientation != 0.f)
	{
		Vector3 axis = target - position;

		Matrix3 rotation = Matrix4::Rotate(upOrientation, axis);

		up = rotation * up;
	}
}


void mgraphics::Camera::moveTo(Vector3 newPosition)
{
	position = newPosition;
}


void mgraphics::Camera::moveTo(GLfloat x, GLfloat y, GLfloat z)
{
	position.X(x);
	position.Y(y);
	position.Z(z);
}


void mgraphics::Camera::setTarget(Vector3 newTarget)
{
	target = newTarget;
}


void mgraphics::Camera::setTarget(GLfloat x, GLfloat y, GLfloat z)
{
	target.X(x);
	target.Y(y);
	target.Z(z);
}


void mgraphics::Camera::setViewDirection(Vector3 viewDirection)
{
	target = position + viewDirection;
}


void mgraphics::Camera::setViewDirection(GLfloat x, GLfloat y, GLfloat z)
{
	target = position + Vector3(x, y, z);
}


Matrix4 mgraphics::Camera::getViewMatrix()
{
	Vector3 delta = target - position;

	delta.normalize();

	adjustUp();

	Vector3 s = delta.Cross(up);
	s.normalize();

	Vector3 u = s.Cross(delta);

	Matrix4 m;

	m.A11(s.X());			m.A12(s.Y());			m.A13(s.Z());			m.A14(0.f);
	m.A21(u.X());			m.A22(u.Y());			m.A23(u.Z());			m.A24(0.f);
	m.A31(-delta.X());		m.A32(-delta.Y());		m.A33(-delta.Z());		m.A34(0.f);
	m.A41(0.f);				m.A42(0.f);				m.A43(0.f);				m.A44(1.f);

	m *= Matrix4::Translate(-position.X(), -position.Y(), -position.Z());
	
	return m;
}


void mgraphics::Camera::setUpOrientation(float orientation)
{
	upOrientation = orientation;
}


void mgraphics::Camera::reset()
{
	position.X(0.f);
	position.Y(0.f);
	position.Z(0.f);

	target.X(0.f);
	target.Y(0.f);
	target.Z(-1.f);

	up.X(0.f);
	up.Y(1.f);
	up.Z(0.f);
}
//	===========================================================================================================================================================================================		#####
