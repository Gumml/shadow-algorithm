/*
* 
* Bibliothek f�r OpenGL Operationen.
* Au�erdem werden durch die Bibliothek zum Laden, kompilieren und Linken von Shaderprogrammen zur Verf�gung
* gestellt, sowie eine Funktion zum Laden von Bilddaten durch die FreeImage-Bibliothek. F�r OpenGL als Transposed-Flag GL_FALSE
* verwenden.
*
* Autor: Markus Miller
*
* Version: 10.0
*
* Windows Optimized Version
*
*/


#pragma once



// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <gl_core_4_4.hpp>
#include <string>
#include <vector>
//	===========================================================================================================================================================================================		#####




namespace mgraphics
{
	// Shader																																													   Shader
	//	=======================================================================================================================================================================================		#####
	namespace shader
	{
		// Shader Methoden
		void readFile(const std::string& srcFile, std::vector<char>& target);
		unsigned int getFileSize(const std::string& file);
		GLuint buildShader(const std::vector<char>& sourceCode, const GLenum type);
		void buildProgram(GLuint &programHandle, GLuint &vertexShaderHandle, GLuint &fragmentShaderHandle);
	}
	//	=======================================================================================================================================================================================		#####


	// Texturen																																													 Texturen
	//	=======================================================================================================================================================================================		#####
	namespace textures
	{
		const GLubyte* LoadImageFile(const std::string& sourceFile, int &width, int &height, int &components, GLenum &format);
		void WriteImageFile(const std::string& targetFile, unsigned int width, unsigned int height, unsigned int depth, GLubyte *imageData);
	}
	//	=======================================================================================================================================================================================		#####


	// Mathematik																																											   Mathematik
	//	=======================================================================================================================================================================================		#####
	namespace math
	{
		template<typename T>
		T minimum(const T a, const T b)
		{
			return a < b ? a : b;
		}

		template<typename T>
		T maximum(const T a, const T b)
		{
			return a > b ? a : b;
		}

		template<typename T>
		T minimum(const T a, const T b, const T c)
		{
			return minimum(a, minimum(b, c));
		}

		template<typename T>
		T maximum(const T a, const T b, const T c)
		{
			return maximum(a, maximum(b, c));
		}

		float round(const float in);
		double round(const double in);

		float Radians(const float in);
		double Radians(const double in);
		float Degree(const float in);
		double Degree(const double in);
	}
	//	=======================================================================================================================================================================================		#####


	// Logik																																														Logik
	//	=======================================================================================================================================================================================		#####
	namespace logic
	{
		template<typename T>
		void clamp(T &val, const T min, const T max)
		{
			if(val - min < 0)
				val = min;

			else if(val - max > 0)
				val = max;
		}

		template<typename T>
		bool inRange(const T sample, const T lowerbound, const T upperbound)
		{
			return sample >= lowerbound && sample <= upperbound;
		}
	}
	//	=======================================================================================================================================================================================		#####
}
