// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include "DataTypes.h"

#include <memory>
#include <limits>
#include <cmath>
//#include <xmmintrin.h>
//	===========================================================================================================================================================================================		#####



// Statics																																														  Statics
//	===========================================================================================================================================================================================		#####
const static float float_epsilon = std::numeric_limits<float>::epsilon();
//	===========================================================================================================================================================================================		#####



// Vector2 Implementierung																																						  Vector2 Implementierung
//	===========================================================================================================================================================================================		#####
const mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::Zero = mgraphics::datatypes::Vector2::Vector2();
const mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::UnitX = mgraphics::datatypes::Vector2::Vector2(1.0f, 0.0f);
const mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::UnitY = mgraphics::datatypes::Vector2::Vector2(0.0f, 1.0f);


mgraphics::datatypes::Vector2::Vector2(void)
{
	data[0] = 0.0f;		data[1] = 0.0f;
}


mgraphics::datatypes::Vector2::Vector2(GLfloat x, GLfloat y)
{
	data[0] = x;		data[1] = y;
}


mgraphics::datatypes::Vector2::Vector2(const Vector3 &xy)
{
	data[0] = xy.X();		data[1] = xy.Y();
}


mgraphics::datatypes::Vector2::Vector2(const Vector4 &xy)
{
	data[0] = xy.X();		data[1] = xy.Y();
}


mgraphics::datatypes::Vector2::~Vector2()
{
	data[0] = 0.0f;		data[1] = 0.0f;
}


void mgraphics::datatypes::Vector2::normalize()
{
	float len = length();

	data[0] /= len;		data[1] /= len; 
}


GLfloat mgraphics::datatypes::Vector2::length() const
{
	return sqrt(data[0] * data[0] + data[1] * data[1]);
}


GLfloat mgraphics::datatypes::Vector2::Dot(const Vector2 &r) const
{
	return data[0] * r.data[0] + data[1] * r.data[1];
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator*(const GLfloat r) const
{
	Vector2 vec;

	vec.data[0] = data[0] * r;		vec.data[1] = data[1] * r;

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::operator*(const GLfloat l, const Vector2 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator/(const GLfloat r) const
{
	Vector2 vec;

	vec.data[0] = data[0] / r;		vec.data[1] = data[1] / r;

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::operator/(const GLfloat l, const Vector2 &r)
{
	Vector2 v;

	for(int i = 0; i < 2; i++)
		v.data[i] = l / r.data[i];

	return v;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator+(const Vector2 &r) const
{
	Vector2 vec;

	vec.data[0] = data[0] + r.data[0];		vec.data[1] = data[1] + r.data[1];

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator+() const
{
	Vector2 vec;

	vec.data[0] = +(data[0]);		vec.data[1] = +(data[1]);

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator-(const Vector2 &r) const
{
	Vector2 vec;

	vec.data[0] = data[0] - r.data[0];		vec.data[1] = data[1] - r.data[1];

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator-() const
{
	Vector2 vec;

	vec.data[0] = -(data[0]);		vec.data[1] = -(data[1]);

	return vec;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator=(const Vector2 &r)
{
	data[0] = r.data[0];		data[1] = r.data[1];

	return *this;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator*=(const GLfloat r)
{
	data[0] *= r;		data[1] *= r;

	return *this;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator/=(const GLfloat r)
{
	data[0] /= r;		data[1] /= r;

	return *this;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator+=(const Vector2 &r)
{
	data[0] += r.data[0];		data[1] += r.data[1];

	return *this;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Vector2::operator-=(const Vector2 &r)
{
	data[0] -= r.data[0];		data[1] -= r.data[1];

	return *this;
}


bool mgraphics::datatypes::Vector2::operator==(const Vector2 &r) const
{
	return (fabs(data[0] - r.data[0]) <= float_epsilon) && (fabs(data[1] - r.data[1]) <= float_epsilon);
}


bool mgraphics::datatypes::Vector2::operator!=(const Vector2 &r) const
{
	return !(*this == r);
}
//	===========================================================================================================================================================================================		#####



// Vector3 Implementierung																																						  Vector3 Implementierung
//	===========================================================================================================================================================================================		#####
const mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::Zero = mgraphics::datatypes::Vector3::Vector3();
const mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::UnitX = mgraphics::datatypes::Vector3::Vector3(1.0f, 0.0f, 0.0f);
const mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::UnitY = mgraphics::datatypes::Vector3::Vector3(0.0f, 1.0f, 0.0f);
const mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::UnitZ = mgraphics::datatypes::Vector3::Vector3(0.0f, 0.0f, 1.0f);


mgraphics::datatypes::Vector3::Vector3(void)
{
	data[0] = 0.0f;		data[1] = 0.0f;		data[2] = 0.0f;
}


mgraphics::datatypes::Vector3::Vector3(const Vector2 &xy, GLfloat z)
{
	data[0] = xy.X();		data[1] = xy.Y();		data[2] = z;
}


mgraphics::datatypes::Vector3::Vector3(GLfloat x, const Vector2 &yz)
{
	data[0] = x;		data[1] = yz.X();		data[2] = yz.Y();
}


mgraphics::datatypes::Vector3::Vector3(GLfloat x, GLfloat y, GLfloat z)
{
	data[0] = x;		data[1] = y;		data[2] = z;
}


mgraphics::datatypes::Vector3::Vector3(const Vector4 &xyz)
{
	data[0] = xyz.X();		data[1] = xyz.Y();		data[2] = xyz.Z();
}


mgraphics::datatypes::Vector3::~Vector3(void)
{
	data[0] = 0.0f;		data[1] = 0.0f;		data[2] = 0.0f;
}


void mgraphics::datatypes::Vector3::normalize()
{
	GLfloat len = length();

	data[0] /= len;		data[1] /= len;		data[2] /= len;
}


GLfloat mgraphics::datatypes::Vector3::length() const
{
	return sqrt(data[0] * data[0] + data[1] * data[1] + data[2] * data[2]);
}


GLfloat mgraphics::datatypes::Vector3::Dot(const Vector3 &r) const
{
	return data[0] * r.data[0] + data[1] * r.data[1] + data[2] * r.data[2];
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::Cross(const Vector3 &r) const
{
	Vector3 vec;

	vec.data[0] = data[1] * r.data[2] - data[2] * r.data[1];
	vec.data[1] = data[2] * r.data[0] - data[0] * r.data[2];
	vec.data[2] = data[0] * r.data[1] - data[1] * r.data[0];

	return vec;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator*(const GLfloat r) const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = data[i] * r;

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::operator*(const GLfloat l, const Vector3 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator/(const GLfloat r) const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = data[i] / r;

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::operator/(const GLfloat l, const Vector3 &r)
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = l / r.data[i];

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator+(const Vector3 &r) const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = data[i] + r.data[i];

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator+() const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = +(data[i]);

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator-(const Vector3 &r) const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = data[i] - r.data[i];

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator-() const
{
	Vector3 v;

	for(int i = 0; i < 3; i++)
		v.data[i] = -(data[i]);

	return v;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator=(const Vector3 &r)
{
	for(int i = 0; i < 3; i++)
		data[i] = r.data[i];

	return *this;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator*=(const GLfloat r)
{
	for(int i = 0; i < 3; i++)
		data[i] *= r;

	return *this;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator/=(const GLfloat r)
{
	for(int i = 0; i < 3; i++)
		data[i] /= r;

	return *this;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator+=(const Vector3 &r)
{
	for(int i = 0; i < 3; i++)
		data[i] += r.data[i];

	return *this;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Vector3::operator-=(const Vector3 &r)
{
	for(int i = 0; i < 3; i++)
		data[i] -= r.data[i];

	return *this;
}


bool mgraphics::datatypes::Vector3::operator==(const Vector3 &r) const
{
	bool equal = true;

	for(int i = 0; i < 3; i++)
		equal = equal && (fabs(data[i] - r.data[i]) <= float_epsilon);

	return equal;
}


bool mgraphics::datatypes::Vector3::operator!=(const Vector3 &r) const
{
	return !(*this == r);
}
//	===========================================================================================================================================================================================		#####



// Vector4 Implementierung																																						  Vector4 Implementierung
//	===========================================================================================================================================================================================		#####
const mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::Zero = mgraphics::datatypes::Vector4::Vector4();
const mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::UnitX = mgraphics::datatypes::Vector4::Vector4(1.0f, 0.0f, 0.0f, 0.0f);
const mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::UnitY = mgraphics::datatypes::Vector4::Vector4(0.0f, 1.0f, 0.0f, 0.0f);
const mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::UnitZ = mgraphics::datatypes::Vector4::Vector4(0.0f, 0.0f, 1.0f, 0.0f);
const mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::UnitW = mgraphics::datatypes::Vector4::Vector4(0.0f, 0.0f, 0.0f, 1.0f);

mgraphics::datatypes::Vector4::Vector4(void)
{
	data[0] = 0.0f;		data[1] = 0.0f;		data[2] = 0.0f;		data[3] = 0.0f;
}


mgraphics::datatypes::Vector4::Vector4(GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
	data[0] = x;		data[1] = y;		data[2] = z;		data[3] = w;
}


mgraphics::datatypes::Vector4::Vector4(const Vector2 &xy, GLfloat z, GLfloat w)
{
	data[0] = xy.X();		data[1] = xy.Y();		data[2] = z;		data[3] = w;
}


mgraphics::datatypes::Vector4::Vector4(GLfloat x, const Vector2 &yz, GLfloat w)
{
	data[0] = x;		data[1] = yz.X();		data[2] = yz.Y();		data[3] = w;
}


mgraphics::datatypes::Vector4::Vector4(GLfloat x, GLfloat y, const Vector2 &zw)
{
	data[0] = x;		data[1] = y;		data[2] = zw.X();		data[3] = zw.Y();
}


mgraphics::datatypes::Vector4::Vector4(const Vector2 &xy, const Vector2 &zw)
{
	data[0] = xy.X();		data[1] = xy.Y();		data[2] = zw.X();		data[3] = zw.Y();
}


mgraphics::datatypes::Vector4::Vector4(const Vector3 &xyz, GLfloat w)
{
	data[0] = xyz.X();		data[1] = xyz.Y();		data[2] = xyz.Z();		data[3] = w;
}


mgraphics::datatypes::Vector4::Vector4(GLfloat x, const Vector3 &yzw)
{
	data[0] = x;		data[1] = yzw.X();		data[2] = yzw.Y();		data[3] = yzw.Z();
}


mgraphics::datatypes::Vector4::~Vector4(void)
{
	data[0] = 0.0f;		data[1] = 0.0f;		data[2] = 0.0f;		data[3] = 0.0f;
}


void mgraphics::datatypes::Vector4::normalize()
{
	GLfloat len = length();

	for(int i = 0; i < 4; i++)
		data[i] /= len;
}


GLfloat mgraphics::datatypes::Vector4::length() const
{
	float qSum = 0.0f;

	for(int i = 0; i < 4; i++)
		qSum += data[i] * data[i];

	return sqrt(qSum);
}


GLfloat mgraphics::datatypes::Vector4::Dot(const Vector4 &r) const
{
	float dot = 0.0f;

	for(int i = 0; i < 4; i++)
		dot += data[i] * r.data[i];

	return dot;
}

mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator*(const GLfloat r) const
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = data[i] * r;

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::operator*(const GLfloat l, const Vector4 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator/(const GLfloat r) const
{
	Vector4 vec;
	
	for(int i = 0; i < 4; i++)
		vec.data[i] = data[i] / r;

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::operator/(const GLfloat l, const Vector4 &r)
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = l / r.data[i];

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator+(const Vector4 &r) const
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = data[i] + r.data[i];

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator+() const
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = +(data[i]);

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator-(const Vector4 &r) const
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = data[i] - r.data[i];

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator-() const
{
	Vector4 vec;

	for(int i = 0; i < 4; i++)
		vec.data[i] = -(data[i]);

	return vec;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator=(const Vector4 &r)
{
	for(int i = 0; i < 4; i++)
		data[i] = r.data[i];

	return *this;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator*=(const GLfloat r)
{
	for(int i = 0; i < 4; i++)
		data[i] *= r;

	return *this;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator/=(const GLfloat r)
{
	for(int i = 0; i < 4; i++)
		data[i] /= r;

	return *this;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator+=(const Vector4 &r)
{
	for(int i = 0; i < 4; i++)
		data[i] += r.data[i];

	return *this;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Vector4::operator-=(const Vector4 &r)
{
	for(int i = 0; i < 4; i++)
		data[i] -= r.data[i];

	return *this;
}


bool mgraphics::datatypes::Vector4::operator==(const Vector4 &r) const
{
	bool equal = true;

	for(int i = 0; i < 4; i++)
		equal = equal && (fabs(data[i] - r.data[i]) <= float_epsilon);

	return equal;
}


bool mgraphics::datatypes::Vector4::operator!=(const Vector4 &r) const
{
	return !(*this == r);
}
//	===========================================================================================================================================================================================		#####



// Matrix2 Implementierung																																						  Matrix2 Implementierung
//	===========================================================================================================================================================================================		#####
mgraphics::datatypes::Matrix2::Matrix2()
{
	data[0] = 1.0f;		data[2] = 0.0f;
	data[1] = 0.0f;		data[3] = 1.0f;
}


mgraphics::datatypes::Matrix2::Matrix2(const Matrix3 &m)
{
	data[0] = m.A11();			data[2] = m.A12();
	data[1] = m.A21();			data[3] = m.A22();
}


mgraphics::datatypes::Matrix2::Matrix2(const Matrix4 &m)
{
	data[0] = m.A11();			data[2] = m.A12();
	data[1] = m.A21();			data[3] = m.A22();
}


mgraphics::datatypes::Matrix2::Matrix2(const Vector2 &column1, const Vector2 &column2)
{
	data[0] = column1.X();			data[2] = column2.X();
	data[1] = column1.Y();			data[3] = column2.Y();
}


mgraphics::datatypes::Matrix2::Matrix2(const GLfloat a11, const GLfloat a12, const GLfloat a21, const GLfloat a22)
{
	data[0] = a11;		data[2] = a12;
	data[1] = a21;		data[3] = a22;
}


mgraphics::datatypes::Matrix2::~Matrix2()
{
	data[0] = 0.0f;		data[2] = 0.0f;
	data[1] = 0.0f;		data[3] = 0.0f;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator*(const Matrix2 &r) const
{
	Matrix2 mat;

	//	0	2		0	2
	//	1	3		1	3

	mat.data[0] = data[0] * r.data[0] + data[2] * r.data[1];			mat.data[2] = data[0] * r.data[2] + data[2] * r.data[3];
	mat.data[1] = data[1] * r.data[0] + data[3] * r.data[1];			mat.data[3] = data[1] * r.data[2] + data[3] * r.data[3];

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator*(const GLfloat r) const
{
	Matrix2 mat;

	mat.data[0] = data[0] * r;			mat.data[2] = data[2] * r;
	mat.data[1] = data[1] * r;			mat.data[3] = data[3] * r;

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::operator*(const GLfloat l, const Matrix2 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector2 mgraphics::datatypes::Matrix2::operator*(Vector2 &r) const
{
	//	0	2			X
	//	1	3			Y

	Vector2 v;

	v.X(data[0] * r.X() + data[2] * r.Y());
	v.Y(data[1] * r.X() + data[3] * r.Y());

	return v;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator/(const GLfloat r) const
{
	Matrix2 mat;

	mat.data[0] = data[0] / r;			mat.data[2] = data[2] / r;
	mat.data[1] = data[1] / r;			mat.data[3] = data[3] / r;

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::operator/(const GLfloat l, const Matrix2 &r)
{
	Matrix2 m;

	for(int i = 0; i < 4; i++)
		m.data[i] = l / r.data[i];

	return m;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator+(const Matrix2 &r) const
{
	Matrix2 mat;

	mat.data[0] = data[0] + r.data[0];			mat.data[2] = data[2] + r.data[2];
	mat.data[1] = data[1] + r.data[1];			mat.data[3] = data[3] + r.data[3];

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator+() const
{
	Matrix2 mat;

	mat.data[0] = +(data[0]);			mat.data[2] = +(data[2]);
	mat.data[1] = +(data[1]);			mat.data[3] = +(data[3]);

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator-(const Matrix2 &r) const
{
	Matrix2 mat;

	mat.data[0] = data[0] - r.data[0];			mat.data[2] = data[2] - r.data[2];
	mat.data[1] = data[1] - r.data[1];			mat.data[3] = data[3] - r.data[3];

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator-() const
{
	Matrix2 mat;

	mat.data[0] = -(data[0]);			mat.data[2] = -(data[2]);
	mat.data[1] = -(data[1]);			mat.data[3] = -(data[3]);

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator*=(const GLfloat r)
{
	data[0] *= r;			data[2] *= r;
	data[1] *= r;			data[3] *= r;

	return *this;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator*=(const Matrix2 &m)
{
	Matrix2 ref = *this * m;

	for(int i = 0; i < 4; i++)
		data[i] = ref.data[i];

	return *this;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator/=(const GLfloat r)
{
	data[0] /= r;			data[2] /= r;
	data[1] /= r;			data[3] /= r;

	return *this;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator+=(const Matrix2 &r)
{
	data[0] += r.data[0];			data[2] += r.data[2];
	data[1] += r.data[1];			data[3] += r.data[3];

	return *this;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator-=(const Matrix2 &r)
{
	data[0] -= r.data[0];			data[2] -= r.data[2];
	data[1] -= r.data[1];			data[3] -= r.data[3];

	return *this;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::operator=(const Matrix2 &r)
{
	data[0] = r.data[0];			data[2] = r.data[2];
	data[1] = r.data[1];			data[3] = r.data[3];

	return *this;
}


bool mgraphics::datatypes::Matrix2::operator==(const Matrix2 &r) const
{
	bool equal = true;

	for(int i = 0; i < 4; i++)
		equal = equal && (fabs(data[i] - r.data[i]) <= float_epsilon);

	return equal;
}


bool mgraphics::datatypes::Matrix2::operator!=(const Matrix2 &r) const
{
	return !(*this == r);
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::Transpose(const Matrix2 &in)
{
	Matrix2 mat;

	mat.data[0] = in.data[0];			mat.data[2] = in.data[1];
	mat.data[1] = in.data[2];			mat.data[3] = in.data[3];

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::Invert(const Matrix2 &in)
{
	GLfloat det = Matrix2::Determinant(in);

	if(det == 0)
		throw NotInvertibleException();

	Matrix2 adj = Matrix2::Adjugate(in);

	return (1.0f / det) * adj;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::Rotate(const GLfloat angle)
{
	Matrix2 mat;

	mat.data[0] = cos(angle);		mat.data[2] = -sin(angle);
	mat.data[1] = sin(angle);		mat.data[3] = cos(angle);

	return mat;
}


GLfloat mgraphics::datatypes::Matrix2::Determinant(const Matrix2 &in)
{
	return in.data[0] * in.data[3] - in.data[1] * in.data[2];
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::Adjugate(const Matrix2 &in)
{
	Matrix2 mat;

	mat.data[0] = in.data[3];		mat.data[2] = -in.data[2];
	mat.data[1] = -in.data[1];		mat.data[3] = in.data[0];

	return mat;
}


mgraphics::datatypes::Matrix2 mgraphics::datatypes::Matrix2::Identity()
{
	Matrix2 mat;
	return mat;
}
//	===========================================================================================================================================================================================		#####



// Matrix3 Implementierung																																						  Matrix3 Implementierung
//	===========================================================================================================================================================================================		#####
mgraphics::datatypes::Matrix3::Matrix3()
{
	data[0] = 1.0f;		data[3] = 0.0f;		data[6] = 0.0f;
	data[1] = 0.0f;		data[4] = 1.0f;		data[7] = 0.0f;
	data[2] = 0.0f;		data[5] = 0.0f;		data[8] = 1.0f;
}


mgraphics::datatypes::Matrix3::Matrix3(const Matrix2 &m)
{
	data[0] = m.A11();		data[3] = m.A12();		data[6] = 0.0f;
	data[1] = m.A21();		data[4] = m.A22();		data[7] = 0.0f;
	data[2] = 0.0f;			data[5] = 0.0f;			data[8] = 1.0f;
}


mgraphics::datatypes::Matrix3::Matrix3(const Matrix4 &m)
{
	data[0] = m.A11();		data[3] = m.A12();		data[6] = m.A13();
	data[1] = m.A21();		data[4] = m.A22();		data[7] = m.A23();
	data[2] = m.A31();		data[5] = m.A32();		data[8] = m.A33();
}


mgraphics::datatypes::Matrix3::Matrix3(const Vector3 &column1, const Vector3 &column2, const Vector3 &column3)
{
	data[0] = column1.X();		data[3] = column2.X();		data[6] = column3.X();
	data[1] = column1.Y();		data[4] = column2.Y();		data[7] = column3.Y();
	data[2] = column1.Z();		data[5] = column2.Z();		data[8] = column3.Z();
}


mgraphics::datatypes::Matrix3::Matrix3(const GLfloat a11, const GLfloat a12, const GLfloat a13, const GLfloat a21, const GLfloat a22, const GLfloat a23, const GLfloat a31, const GLfloat a32, const GLfloat a33)
{
	data[0] = a11;		data[3] = a12;		data[6] = a13;
	data[1] = a21;		data[4] = a22;		data[7] = a23;
	data[2] = a31;		data[5] = a32;		data[8] = a33;
}


mgraphics::datatypes::Matrix3::~Matrix3()
{
	for(int i = 0; i < 9; i++)
		data[i] = 0.0f;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator*(const Matrix3 &r) const
{
	//	0	3	6			0	3	6			0*0+3*1+6*2		0*3+3*4+6*5		0*6+3*7+6*8
	//	1	4	7			1	4	7			1*0+4*1+7*2		1*3+4*4+7*5		1*6+4*7+7*8
	//	2	5	8			2	5	8			2*0+5*1+8*2		2*3+5*4+8*5		2*6+5*7+8*8
	Matrix3 m;

	m.data[0] = data[0] * r.data[0] + data[3] * r.data[1] + data[6] * r.data[2];	m.data[3] = data[0] * r.data[3] + data[3] * r.data[4] + data[6] * r.data[5];		m.data[6] = data[0] * r.data[6] + data[3] * r.data[7] + data[6] * r.data[8];
	m.data[1] = data[1] * r.data[0] + data[4] * r.data[1] + data[7] * r.data[2];	m.data[4] = data[1] * r.data[3] + data[4] * r.data[4] + data[7] * r.data[5];		m.data[7] = data[1] * r.data[6] + data[4] * r.data[7] + data[7] * r.data[8];
	m.data[2] = data[2] * r.data[0] + data[5] * r.data[1] + data[8] * r.data[2];	m.data[5] = data[2] * r.data[3] + data[5] * r.data[4] + data[8] * r.data[5];		m.data[8] = data[2] * r.data[6] + data[5] * r.data[7] + data[8] * r.data[8];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator*(const GLfloat r) const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = data[i] * r;

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::operator*(const GLfloat l, const Matrix3 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Matrix3::operator*(const Vector3 &r) const
{
	//	0	3	6			x			0*x+3*y+6*z
	//	1	4	7			y			1*x+4*y+7*z
	//	2	5	8			z			2*x+5*y+8*z
	Vector3 v;

	v.X(data[0] * r.X() + data[3] * r.Y() + data[6] * r.Z());
	v.Y(data[1] * r.X() + data[4] * r.Y() + data[7] * r.Z());
	v.Z(data[2] * r.X() + data[5] * r.Y() + data[8] * r.Z());

	return v;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator/(const GLfloat r) const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = data[i] / r;

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::operator/(const GLfloat l, const Matrix3 &r)
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = l / r.data[i];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator+(const Matrix3 &r) const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = data[i] + r.data[i];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator+() const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = +data[i];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator-(const Matrix3 &r) const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = data[i] - r.data[i];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator-() const
{
	Matrix3 m;

	for(int i = 0; i < 9; i++)
		m.data[i] = -data[i];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator*=(const GLfloat r)
{
	for(int i = 0; i < 9; i++)
		data[i] *= r;

	return *this;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator*=(const Matrix3 &m)
{
	Matrix3 v = *this * m;

	*this = v;

	return *this;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator/=(const GLfloat r)
{
	for(int i = 0; i < 9; i++)
		data[i] /= r;

	return *this;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator+=(const Matrix3 &r)
{
	for(int i = 0; i < 9; i++)
		data[i] += r.data[i];

	return *this;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator-=(const Matrix3 &r)
{
	for(int i = 0; i < 9; i++)
		data[i] -= r.data[i];

	return *this;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::operator=(const Matrix3 &r)
{
	for(int i = 0; i < 9; i++)
		data[i] = r.data[i];

	return *this;
}


bool mgraphics::datatypes::Matrix3::operator==(const Matrix3 &r) const
{
	bool eq = true;

	for(int i = 0; i < 9; i++)
		eq = eq && (fabs(data[i] - r.data[i]) <= float_epsilon);

	return eq;
}


bool mgraphics::datatypes::Matrix3::operator!=(const Matrix3 &r) const
{
	return !(*this == r);
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::Transpose(const Matrix3 &in)
{
	Matrix3 m;

	m.data[0] = in.data[0];		m.data[3] = in.data[1];		m.data[6] = in.data[2];
	m.data[1] = in.data[3];		m.data[4] = in.data[4];		m.data[7] = in.data[5];
	m.data[2] = in.data[6];		m.data[5] = in.data[7];		m.data[8] = in.data[8];

	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::Invert(const Matrix3 &in)
{
	float det = Matrix3::Determinant(in);

	if(det == 0)
		throw NotInvertibleException();

	Matrix3 adj = Matrix3::Adjugate(in);

	return (1.0f / det) * adj;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::Rotate(const GLfloat angle, const Vector3 &axis)
{
	Vector3 n_axis = axis;
	n_axis.normalize();

	float c = cos(angle);
	float s = sin(angle);

	float one_c = 1.0f - c;

	float x = n_axis.X(), y = n_axis.Y(), z = n_axis.Z();

	float yx = y * x;
	float zx = z * x;
	float zy = z * y;

	float a11 = x * x * (one_c) + c;			float a12 = yx * (one_c) - z * s;			float a13 = zx * (one_c) + y * s;
	float a21 = yx * (one_c) + z * s;			float a22 = y * y * (one_c) + c;			float a23 = zy * (one_c) - x * s;
	float a31 = zx * (one_c) - y * s;			float a32 = zy * (one_c) + x * s;			float a33 = z * z * (one_c) + c;
	
	Matrix3 m(a11, a12, a13, a21, a22, a23, a31, a32, a33);
	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::RotateFromQuaternion(const Quaternion &rotation)
{

	Quaternion nrotation = rotation;
	nrotation.normalize();

	float qx = nrotation.X();
	float qy = nrotation.Y();
	float qz = nrotation.Z();
	float qw = nrotation.W();

	float a11 = 1.0f - 2.0f * (qy * qy + qz * qz);		float a12 = 2.0f * (qx * qy - qw * qz);				float a13 = 2.0f * (qx * qz + qw * qy);
	float a21 = 2.0f * (qx * qy + qw * qz);				float a22 = 1.0f - 2.0f * (qx * qx + qz * qz);		float a23 = 2.0f * (qy * qz - qw * qx);
	float a31 = 2.0f * (qx * qz - qw * qy);				float a32 = 2.0f * (qy * qz + qw * qx);				float a33 = 1.0f - 2.0f * (qx * qx + qy * qy);

	Matrix3 m(a11, a12, a13, a21, a22, a23, a31, a32, a33);
	return m;
}


GLfloat mgraphics::datatypes::Matrix3::Determinant(const Matrix3 &in)
{
	GLfloat m = (in.data[0] * in.data[4] * in.data[8]) + (in.data[3] * in.data[7] * in.data[2]) + (in.data[6] * in.data[1] * in.data[5]) - (in.data[6] * in.data[4] * in.data[2]) - (in.data[3] * in.data[1] * in.data[8]) - (in.data[0] * in.data[7] * in.data[5]);
	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::Adjugate(const Matrix3 &in)
{
	// Adjunkte
	// Ausgang
	// a   b   c		0   3   6
	// d   e   f		1   4   7
	// g   h   i		2   5   8
	//
	// ei-fh	ch-bi	bf-ce
	// fg-di	ai-cg	cd-af
	// dh-eg	bg-ah	ae-bd

	float a11 = in.data[4] * in.data[8] - in.data[7] * in.data[5];		float a12 = in.data[6] * in.data[5] - in.data[3] * in.data[8];		float a13 = in.data[3] * in.data[7] - in.data[6] * in.data[4];
	float a21 = in.data[7] * in.data[2] - in.data[1] * in.data[8];		float a22 = in.data[0] * in.data[8] - in.data[6] * in.data[2];		float a23 = in.data[6] * in.data[1] - in.data[0] * in.data[7];
	float a31 = in.data[1] * in.data[5] - in.data[4] * in.data[2];		float a32 = in.data[3] * in.data[2] - in.data[0] * in.data[5];		float a33 = in.data[0] * in.data[4] - in.data[3] * in.data[1];

	Matrix3 m(a11, a12, a13, a21, a22, a23, a31, a32, a33);
	return m;
}


mgraphics::datatypes::Matrix3 mgraphics::datatypes::Matrix3::Identity()
{
	Matrix3 m;
	return m;
}
//	===========================================================================================================================================================================================		#####



// Matrix4 Implementierung																																						  Matrix4 Implementierung
//	===========================================================================================================================================================================================		#####
mgraphics::datatypes::Matrix4::Matrix4(void)
{
	data[0] = 1.0f;		data[4] = 0.0f;		data[8] = 0.0f;		data[12] = 0.0f;
	data[1] = 0.0f;		data[5] = 1.0f;		data[9] = 0.0f;		data[13] = 0.0f;
	data[2] = 0.0f;		data[6] = 0.0f;		data[10] = 1.0f;	data[14] = 0.0f;
	data[3] = 0.0f;		data[7] = 0.0f;		data[11] = 0.0f;	data[15] = 1.0f;
}


mgraphics::datatypes::Matrix4::~Matrix4(void)
{
	for(int i = 0; i < 16; i++)
		data[i] = 0.0f;
}


mgraphics::datatypes::Matrix4::Matrix4(const Matrix2 &m)
{
	data[0] = m.A11();		data[4] = m.A12();		data[8] = 0.0f;		data[12] = 0.0f;
	data[1] = m.A21();		data[5] = m.A22();		data[9] = 0.0f;		data[13] = 0.0f;
	data[2] = 0.0f;			data[6] = 0.0f;			data[10] = 1.0f;	data[14] = 0.0f;
	data[3] = 0.0f;			data[7] = 0.0f;			data[11] = 0.0f;	data[15] = 1.0f;
}


mgraphics::datatypes::Matrix4::Matrix4(const Matrix3 &m)
{
	data[0] = m.A11();		data[4] = m.A12();		data[8] = m.A13();		data[12] = 0.0f;
	data[1] = m.A21();		data[5] = m.A22();		data[9] = m.A23();		data[13] = 0.0f;
	data[2] = m.A31();		data[6] = m.A32();		data[10] = m.A33();		data[14] = 0.0f;
	data[3] = 0.0f;			data[7] = 0.0f;			data[11] = 0.0f;		data[15] = 1.0f;
}


mgraphics::datatypes::Matrix4::Matrix4(const Vector4 &column1, const Vector4 &column2, const Vector4 &column3, const Vector4 &column4)
{
	data[0] = column1.X();		data[4] = column2.X();		data[8] = column3.X();		data[12] = column4.X();
	data[1] = column1.Y();		data[5] = column2.Y();		data[9] = column3.Y();		data[13] = column4.Y();
	data[2] = column1.Z();		data[6] = column2.Z();		data[10] = column3.Z();		data[14] = column4.Z();
	data[3] = column1.W();		data[7] = column2.W();		data[11] = column3.W();		data[15] = column4.W();
}


mgraphics::datatypes::Matrix4::Matrix4(const GLfloat a11, const GLfloat a12, const GLfloat a13, const GLfloat a14, const GLfloat a21, const GLfloat a22, const GLfloat a23, const GLfloat a24, const GLfloat a31, const GLfloat a32, const GLfloat a33, const GLfloat a34, const GLfloat a41, const GLfloat a42, const GLfloat a43, const GLfloat a44)
{
	data[0] = a11;		data[4] = a12;		data[8] = a13;		data[12] = a14;
	data[1] = a21;		data[5] = a22;		data[9] = a23;		data[13] = a24;
	data[2] = a31;		data[6] = a32;		data[10] = a33;		data[14] = a34;
	data[3] = a41;		data[7] = a42;		data[11] = a43;		data[15] = a44;
}

mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator*(const Matrix4 &r) const
{
	//	0	4	8	12			0	4	8	12				0*0+4*1+8*2+12*3		0*4+4*5+8*6+12*7		0*8+4*9+8*10+12*11		0*12+4*13+8*14+12*15
	//	1	5	9	13			1	5	9	13				1*0+5*1+9*2+13*3		1*4+5*5+9*6+13*7		1*8+5*9+9*10+13*11		1*12+5*13+9*14+13*15
	//	2	6	10	14			2	6	10	14				2*0+6*1+10*2+14*3		2*4+6*5+10*6+14*7		2*8+6*9+10*10+14*11		2*12+6*13+10*14+14*15
	//	3	7	11	15			3	7	11	15				3*0+7*1+11*2+15*3		3*4+7*5+11*6+15*7		3*8+7*9+11*10+15*11		3*12+7*13+11*14+15*15

	float a11 = data[0] * r.data[0] + data[4] * r.data[1] + data[8] * r.data[2] + data[12] * r.data[3],			a12 = data[0] * r.data[4] + data[4] * r.data[5] + data[8] * r.data[6] + data[12] * r.data[7],		a13 = data[0] * r.data[8] + data[4] * r.data[9] + data[8] * r.data[10] + data[12] * r.data[11],			a14 = data[0] * r.data[12] + data[4] * r.data[13] + data[8] * r.data[14] + data[12] * r.data[15];
	float a21 = data[1] * r.data[0] + data[5] * r.data[1] + data[9] * r.data[2] + data[13] * r.data[3],			a22 = data[1] * r.data[4] + data[5] * r.data[5] + data[9] * r.data[6] + data[13] * r.data[7],		a23 = data[1] * r.data[8] + data[5] * r.data[9] + data[9] * r.data[10] + data[13] * r.data[11],			a24 = data[1] * r.data[12] + data[5] * r.data[13] + data[9] * r.data[14] + data[13] * r.data[15];
	float a31 = data[2] * r.data[0] + data[6] * r.data[1] + data[10] * r.data[2] + data[14] * r.data[3],		a32 = data[2] * r.data[4] + data[6] * r.data[5] + data[10] * r.data[6] + data[14] * r.data[7],		a33 = data[2] * r.data[8] + data[6] * r.data[9] + data[10] * r.data[10] + data[14] * r.data[11],		a34 = data[2] * r.data[12] + data[6] * r.data[13] + data[10] * r.data[14] + data[14] * r.data[15];
	float a41 = data[3] * r.data[0] + data[7] * r.data[1] + data[11] * r.data[2] + data[15] * r.data[3],		a42 = data[3] * r.data[4] + data[7] * r.data[5] + data[11] * r.data[6] + data[15] * r.data[7],		a43 = data[3] * r.data[8] + data[7] * r.data[9] + data[11] * r.data[10] + data[15] * r.data[11],		a44 = data[3] * r.data[12] + data[7] * r.data[13] + data[11] * r.data[14] + data[15] * r.data[15];

	Matrix4 m(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44);
	return m;
}

mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator*(const GLfloat r) const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = data[i] * r;

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::operator*(const GLfloat l, const Matrix4 &r)
{
	return r * l;
}


mgraphics::datatypes::Vector4 mgraphics::datatypes::Matrix4::operator*(const Vector4 &r) const
{
	float v0 = data[0] * r.X() + data[4] * r.Y() + data[8] * r.Z() + data[12] * r.W();
	float v1 = data[1] * r.X() + data[5] * r.Y() + data[9] * r.Z() + data[13] * r.W();
	float v2 = data[2] * r.X() + data[6] * r.Y() + data[10] * r.Z() + data[14] * r.W();
	float v3 = data[3] * r.X() + data[7] * r.Y() + data[11] * r.Z() + data[15] * r.W();

	Vector4 m(v0, v1, v2, v3);
	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator/(const GLfloat r) const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = data[i] / r;

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::operator/(const GLfloat l, const Matrix4 &r)
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = l / r.data[i];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator+(const Matrix4 &r) const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = data[i] + r.data[i];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator+() const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = +data[i];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator-(const Matrix4 &r) const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = data[i] - r.data[i];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator-() const
{
	Matrix4 m;

	for(int i = 0; i < 16; i++)
		m.data[i] = -data[i];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator*=(const GLfloat r)
{
	for(int i = 0; i < 16; i++)
		data[i] *= r;

	return *this;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator*=(const Matrix4 &m)
{
	Matrix4 fl = *this * m;
	*this = fl;

	return *this;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator/=(const GLfloat r)
{
	for(int i = 0; i < 16; i++)
		data[i] /= r;

	return *this;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator+=(const Matrix4 &r)
{
	Matrix4 fl = *this + r;
	*this = fl;

	return *this;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator-=(const Matrix4 &r)
{
	Matrix4 fl = *this - r;
	*this = fl;

	return *this;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::operator=(const Matrix4 &r)
{
	for(int i = 0; i < 16; i++)
		data[i] = r.data[i];

	return *this;
}


bool mgraphics::datatypes::Matrix4::operator==(const Matrix4 &r) const
{
	bool ok = true;

	for(int i = 0; i < 16; i++)
		ok = ok && (fabs(data[i] - r.data[i]) <= float_epsilon);

	return ok;
}


bool mgraphics::datatypes::Matrix4::operator!=(const Matrix4 &r) const
{
	return !(*this == r);
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Transpose(const Matrix4 &in)
{
	Matrix4 m;

	m.data[0] = in.data[0];		m.data[4] = in.data[1];		m.data[8] = in.data[2];		m.data[12] = in.data[3];
	m.data[1] = in.data[4];		m.data[5] = in.data[5];		m.data[9] = in.data[6];		m.data[13] = in.data[7];
	m.data[2] = in.data[8];		m.data[6] = in.data[9];		m.data[10] = in.data[10];	m.data[14] = in.data[11];
	m.data[3] = in.data[12];	m.data[7] = in.data[13];	m.data[11] = in.data[14];	m.data[15] = in.data[15];

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Invert(const Matrix4 &in)
{
	float det = Matrix4::Determinant(in);

	if(det == 0)
		throw NotInvertibleException();

	Matrix4 adj = Matrix4::Adjugate(in);

	return (1.0f / det) * adj;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Rotate(const GLfloat angle, const Vector3 &axis)
{
	Matrix3 r = Matrix3::Rotate(angle, axis);

	Matrix4 m(r);
	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::RotateFromQuaternion(const Quaternion &rotation)
{
	Matrix3 r = Matrix3::RotateFromQuaternion(rotation);

	Matrix4 m(r);
	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Translate(const GLfloat x, const GLfloat y, const GLfloat z)
{
	Matrix4 m;

	m.data[12] = x;
	m.data[13] = y;
	m.data[14] = z;

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Translate(const Vector3 &translation)
{
	Matrix4 m;

	m.data[12] = translation.X();
	m.data[13] = translation.Y();
	m.data[14] = translation.Z();
	
	return m;
}


GLfloat mgraphics::datatypes::Matrix4::Determinant(const Matrix4 &in)
{
	float fA = in.A11(), fB = -in.A12(), fC = in.A13(), fD = -in.A14();

	std::unique_ptr<Matrix3> mA, mB, mC, mD;

	mA = std::make_unique<Matrix3>(in.A22(), in.A23(), in.A24(), in.A32(), in.A33(), in.A34(), in.A42(), in.A43(), in.A44());
	mB = std::make_unique<Matrix3>(in.A21(), in.A23(), in.A24(), in.A31(), in.A33(), in.A34(), in.A41(), in.A43(), in.A44());
	mC = std::make_unique<Matrix3>(in.A21(), in.A22(), in.A24(), in.A31(), in.A32(), in.A34(), in.A41(), in.A42(), in.A44());
	mD = std::make_unique<Matrix3>(in.A21(), in.A22(), in.A23(), in.A31(), in.A32(), in.A33(), in.A41(), in.A42(), in.A43());

	GLfloat d = fA * Matrix3::Determinant(*mA) + fB * Matrix3::Determinant(*mB) + fC * Matrix3::Determinant(*mC) + fD * Matrix3::Determinant(*mD);

	return d;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Adjugate(const Matrix4 &in)
{
	float s11 = 1.0f;		float s12 = -1.0f;		float s13 = 1.0f;		float s14 = -1.0f;
	float s21 = -1.0f;		float s22 = 1.0f;		float s23 = -1.0f;		float s24 = 1.0f;
	float s31 = 1.0f;		float s32 = -1.0f;		float s33 = 1.0f;		float s34 = -1.0f;
	float s41 = -1.0f;		float s42 = 1.0f;		float s43 = -1.0f;		float s44 = 1.0f;

	Matrix3 m11(in.A22(), in.A23(), in.A24(), in.A32(), in.A33(), in.A34(), in.A42(), in.A43(), in.A44());		Matrix3 m12(in.A21(), in.A23(), in.A24(), in.A31(), in.A33(), in.A34(), in.A41(), in.A43(), in.A44());		Matrix3 m13(in.A21(), in.A22(), in.A24(), in.A31(), in.A32(), in.A34(), in.A41(), in.A42(), in.A44());		Matrix3 m14(in.A21(), in.A22(), in.A23(), in.A31(), in.A32(), in.A33(), in.A41(), in.A42(), in.A43());
	Matrix3 m21(in.A12(), in.A13(), in.A14(), in.A32(), in.A33(), in.A34(), in.A42(), in.A43(), in.A44());		Matrix3 m22(in.A11(), in.A13(), in.A14(), in.A31(), in.A33(), in.A34(), in.A41(), in.A43(), in.A44());		Matrix3 m23(in.A11(), in.A12(), in.A14(), in.A31(), in.A32(), in.A34(), in.A41(), in.A42(), in.A44());		Matrix3 m24(in.A11(), in.A12(), in.A13(), in.A31(), in.A32(), in.A33(), in.A41(), in.A42(), in.A43());
	Matrix3 m31(in.A12(), in.A13(), in.A14(), in.A22(), in.A23(), in.A24(), in.A42(), in.A43(), in.A44());		Matrix3 m32(in.A11(), in.A13(), in.A14(), in.A21(), in.A23(), in.A24(), in.A41(), in.A43(), in.A44());		Matrix3 m33(in.A11(), in.A12(), in.A14(), in.A21(), in.A22(), in.A24(), in.A41(), in.A42(), in.A44());		Matrix3 m34(in.A11(), in.A12(), in.A13(), in.A21(), in.A22(), in.A23(), in.A41(), in.A42(), in.A43());
	Matrix3 m41(in.A12(), in.A13(), in.A14(), in.A22(), in.A23(), in.A24(), in.A32(), in.A33(), in.A34());		Matrix3 m42(in.A11(), in.A13(), in.A14(), in.A21(), in.A23(), in.A24(), in.A31(), in.A33(), in.A34());		Matrix3 m43(in.A11(), in.A12(), in.A14(), in.A21(), in.A22(), in.A24(), in.A31(), in.A32(), in.A34());		Matrix3 m44(in.A11(), in.A12(), in.A13(), in.A21(), in.A22(), in.A23(), in.A31(), in.A32(), in.A33());

	float d11 = s11 * Matrix3::Determinant(m11);		float d12 = s12 * Matrix3::Determinant(m12);		float d13 = s13 * Matrix3::Determinant(m13);		float d14 = s14 * Matrix3::Determinant(m14);
	float d21 = s21 * Matrix3::Determinant(m21);		float d22 = s22 * Matrix3::Determinant(m22);		float d23 = s23 * Matrix3::Determinant(m23);		float d24 = s24 * Matrix3::Determinant(m24);
	float d31 = s31 * Matrix3::Determinant(m31);		float d32 = s32 * Matrix3::Determinant(m32);		float d33 = s33 * Matrix3::Determinant(m33);		float d34 = s34 * Matrix3::Determinant(m34);
	float d41 = s41 * Matrix3::Determinant(m41);		float d42 = s42 * Matrix3::Determinant(m42);		float d43 = s43 * Matrix3::Determinant(m43);		float d44 = s44 * Matrix3::Determinant(m44);

	Matrix4 cm(d11, d12, d13, d14, d21, d22, d23, d24, d31, d32, d33, d34, d41, d42, d43, d44);

	return Matrix4::Transpose(cm);
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Identity()
{
	Matrix4 m;
	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Orthographic(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearZ, const GLfloat farZ)
{
	float a11 = 2.0f / (right - left);		float a12 = 0.0f;						float a13 = 0.0f;						float a14 = -((right + left) / (right - left));
	float a21 = 0.0f;						float a22 = 2.0f / (top - bottom);		float a23 = 0.0f;						float a24 = -((top + bottom) / (top - bottom));
	float a31 = 0.0f;						float a32 = 0.0f;						float a33 = -2.0f / (farZ - nearZ);		float a34 = -((farZ + nearZ) / (farZ - nearZ));
	float a41 = 0.0f;						float a42 = 0.0f;						float a43 = 0.0f;						float a44 = 1.0f;

	Matrix4 m(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44);
	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Orthographic(const GLfloat width, const GLfloat height, const GLfloat nearZ, const GLfloat farZ)
{
	float right = width * .5f;
	float left = -right;
	float top = height * .5f;
	float bottom = -top;

	return Matrix4::Orthographic(left, right, bottom, top, nearZ, farZ);
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Perspective(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearZ, const GLfloat farZ)
{
	float a11 = (2.0f * nearZ) / (right - left);		float a12 = 0.0f;									float a13 = (right + left) / (right - left);	float a14 = 0.0f;
	float a21 = 0.0f;									float a22 = (2.0f * nearZ) / (top - bottom);		float a23 = (top + bottom) / (top - bottom);	float a24 = 0.0f;
	float a31 = 0.0f;									float a32 = 0.0f;									float a33 = (-farZ - nearZ) / (farZ - nearZ);	float a34 = -((farZ + nearZ) / (farZ - nearZ));
	float a41 = 0.0f;									float a42 = 0.0f;									float a43 = -1.0f;								float a44 = 0.0f;

	Matrix4 m(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44);

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Perspective(const GLfloat fovy, const GLfloat aspect, const GLfloat nearZ, const GLfloat farZ)
{
	float y = 1.0f / tanf(fovy * 0.5f);
	float x = y / aspect;

	float a11 = x;			float a12 = 0.0f;		float a13 = 0.0f;						float a14 = 0.0f;
	float a21 = 0.0f;		float a22 = y;			float a23 = 0.0f;						float a24 = 0.0f;
	float a31 = 0.0f;		float a32 = 0.0f;		float a33 = farZ / (nearZ - farZ);		float a34 = (nearZ * farZ) / (nearZ - farZ);
	float a41 = 0.0f;		float a42 = 0.0f;		float a43 = -1.0f;						float a44 = 0.0f;

	Matrix4 m(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44);

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Scale(const GLfloat x, const GLfloat y, const GLfloat z)
{
	Matrix4 m;

	m.data[0] = x;
	m.data[5] = y;
	m.data[10] = z;

	return m;
}


mgraphics::datatypes::Matrix4 mgraphics::datatypes::Matrix4::Scale(const Vector3 &scale)
{
	Matrix4 m;

	m.data[0] = scale.X();
	m.data[5] = scale.Y();
	m.data[10] = scale.Z();

	return m;
}
//	===========================================================================================================================================================================================		#####



// Quaternion Implementierung																																				   Quaternion Implementierung
//	===========================================================================================================================================================================================		#####
mgraphics::datatypes::Quaternion::Quaternion()
{
	_w = 1.0f;		_x = 0.0f;		_y = 0.0f;		_z = 0.0f;
}


mgraphics::datatypes::Quaternion::Quaternion(const GLfloat w, const GLfloat x, const GLfloat y, const GLfloat z)
{
	_w = w;		_x = x;		_y = y;		_z = z;
}


mgraphics::datatypes::Quaternion::Quaternion(const Vector2 &wx, const GLfloat y, const GLfloat z)
{
	_w = wx.X();	_x = wx.Y();	_y = y;		_z = z;
}


mgraphics::datatypes::Quaternion::Quaternion(const GLfloat w, const Vector2 &xy, const GLfloat z)
{
	_w = w;		_x = xy.X();		_y = xy.Y();		_z = z;
}


mgraphics::datatypes::Quaternion::Quaternion(const GLfloat w, const GLfloat x, const Vector2 &yz)
{
	_w = w;		_x = x;		_y = yz.X();		_z = yz.Y();
}


mgraphics::datatypes::Quaternion::Quaternion(const Vector2 &wx, const Vector2 &yz)
{
	_w = wx.X();		_x = wx.Y();		_y = yz.X();		_z = yz.Y();
}


mgraphics::datatypes::Quaternion::Quaternion(const Vector3 &wxy, const GLfloat z)
{
	_w = wxy.X();		_x = wxy.Y();		_y = wxy.Z();		_z = z;
}


mgraphics::datatypes::Quaternion::Quaternion(const GLfloat w, const Vector3 &xyz)
{
	_w = w;		_x = xyz.X();		_y = xyz.Y();		_z = xyz.Z();
}


mgraphics::datatypes::Quaternion::Quaternion(const Vector4 &wxyz)
{
	_w = wxyz.X();		_x = wxyz.Y();		_y = wxyz.Z();		_z = wxyz.W();
}


mgraphics::datatypes::Quaternion::~Quaternion()
{
	_w = 1.0f;		_x = 0.0f;		_y = 0.0f;		_z = 0.0f;
}


void mgraphics::datatypes::Quaternion::normalize()
{
	float l = sqrt(_w * _w + _x * _x + _y * _y + _z * _z);

	_w /= l;	_x /= l;	_y /= l;	_z /= l;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::conjugate() const
{
	Quaternion q(_w, -_x, -_y, -_z);
	return q;
}


void mgraphics::datatypes::Quaternion::ConvertToAxisAngle(Vector3 &axis, float &angle)
{
	float vLen = sqrt(_x * _x + _y * _y + _z * _z);
	
	axis.X(_x / vLen);
	axis.Y(_y / vLen);
	axis.Z(_z / vLen);

	angle = acos(_w) * 2.0f;
}


mgraphics::datatypes::Vector3 mgraphics::datatypes::Quaternion::operator*(const Vector3 &v) const
{
	Vector3 nvec = v;
	nvec.normalize();

	Quaternion target;
	Quaternion qvec(0.0f, nvec);
	Quaternion qcon = this->conjugate();

	target = qvec * qcon;

	target = *this * target;

	Vector3 m(target._x, target._y, target._z);
	return m;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::operator*(const Quaternion &r) const
{
	float w = _w * r._w - _x * r._x - _y * r._y - _z * r._z;
	float x = _w * r._x + _x * r._w + _y * r._z - _z * r._y;
	float y = _w * r._y - _x * r._z + _y * r._w + _z * r._x;
	float z = _w * r._z + _x * r._y - _y * r._x + _z * r._w;

	Quaternion q(w, x, y, z);
	return q;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::operator+(const Quaternion &r) const
{
	Quaternion q(_w + r._w, _x + r._x, _y + r._y, _z + r._z);
	return q;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::operator-(const Quaternion &r) const
{
	Quaternion q(_w - r._w, _x - r._x, _y - r._y, _z - r._z);
	return q;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::operator=(const Quaternion &r)
{
	_w = r._w;
	_x = r._x;
	_y = r._y;
	_z = r._z;

	return *this;
}


bool mgraphics::datatypes::Quaternion::operator==(const Quaternion &r) const
{
	return (fabs(_w - r._w) <= float_epsilon) && (fabs(_x - r._x) <= float_epsilon) && (fabs(_y - r._y) <= float_epsilon) && (fabs(_z - r._z) <= float_epsilon);
}


bool mgraphics::datatypes::Quaternion::operator!=(const Quaternion &r) const
{
	return !(*this == r);
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::CreateFromAxisAngle(const Vector3 &axis, const float angle)
{
	float s = sin(angle / 2.0f);

	Vector3 nAxis = axis;
	nAxis.normalize();

	Quaternion q(cos(angle / 2.0f), nAxis.X() * s, nAxis.Y() * s, nAxis.Z() * s);
	return q;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::CreateFromEulerAngles(const float pitch, const float yaw, const float roll)
{
	float w, x, y, z;

	// pitch - beta; yaw - alpha; roll - gamma

	float sinYaw = sin(yaw / 2);
	float cosYaw = cos(yaw / 2);
	float sinPitch = sin(pitch / 2);
	float cosPitch = cos(pitch / 2);
	float sinRoll = sin(roll / 2);
	float cosRoll = cos(roll / 2);

	w = cosYaw * cosPitch * cosRoll + sinYaw * sinPitch * sinRoll;
	y = sinYaw * cosPitch * cosRoll - cosYaw * sinPitch * sinRoll;
	x = cosYaw * sinPitch * cosRoll + sinYaw * cosPitch * sinRoll;
	z = cosYaw * cosPitch * sinRoll - sinYaw * sinPitch * cosRoll;

	Quaternion q(w, x, y, z);
	return q;
}


mgraphics::datatypes::Quaternion mgraphics::datatypes::Quaternion::Identity()
{
	Quaternion a;
	return a;
}
//	===========================================================================================================================================================================================		#####
