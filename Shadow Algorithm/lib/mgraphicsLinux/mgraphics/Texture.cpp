// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <Texture.h>
#include <mgraphics.h>
//	===========================================================================================================================================================================================		#####


namespace mgraphics
{
	namespace textures
	{
		Texture2D::Texture2D(void)
		{
			gl::GenTextures(1, &textureID);
		}


		Texture2D::~Texture2D(void)
		{
			gl::DeleteTextures(1, &textureID);
		}

		void Texture2D::Generate(std::string filepath, bool autoMipmap)
		{
			int components;
			GLenum format;

			data = LoadImageFile(filepath.c_str(), width, height, components, format);

			gl::BindTexture(gl::TEXTURE_2D, textureID);
			gl::TexImage2D(gl::TEXTURE_2D, 0, components, width, height, 0, format, gl::UNSIGNED_BYTE, data);

			if(autoMipmap)
				gl::GenerateMipmap(gl::TEXTURE_2D);

			gl::BindTexture(gl::TEXTURE_2D, 0);
		}

		void Texture2D::Generate(unsigned char* imgData, int imgWidth, int imgHeight, int imgComponents, GLenum imgFormat, bool autoMipmap)
		{
			data = imgData;
			width = imgWidth;
			height = imgHeight;
			
			gl::BindTexture(gl::TEXTURE_2D, textureID);
			gl::TexImage2D(gl::TEXTURE_2D, 0, imgComponents, width, height, 0, imgFormat, gl::UNSIGNED_BYTE, data);
			
			if(autoMipmap)
				gl::GenerateMipmap(gl::TEXTURE_2D);

			gl::BindTexture(gl::TEXTURE_2D, 0);
		}




		void Texture2DStorage::Store(std::string path, char *data, int width, int height, int imgDepth)
		{

		}
	}
}