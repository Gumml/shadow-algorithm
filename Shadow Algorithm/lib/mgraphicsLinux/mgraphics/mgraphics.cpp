// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include "mgraphics.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>

#include <FreeImage.h>
//	===========================================================================================================================================================================================		#####



namespace mgraphics
{
	// Shader																																													   Shader
	//	=======================================================================================================================================================================================		#####
	namespace shader
	{
		// Shader Methoden
		void readFile(const std::string& srcFile, std::vector<char>& target)
		{
			//std::ifstream input(srcFile, std::ifstream::in);

			//if(!input)
			//	std::cout << "Fehler beim Lesen der Datei: " << srcFile << std::endl;

			//target.assign(std::istreambuf_iterator<char>(input), std::istreambuf_iterator<char>());

			//input.close();

			std::ifstream input(srcFile, std::ifstream::in);

			std::stringstream stream;
			std::string s;

			while(std::getline(input, s))
				stream << s << std::endl;

			stream << "// make peace with the compiler";

			std::string code = stream.str();

			std::copy(std::begin(code), std::end(code), std::back_inserter(target));
		}



		unsigned int getFileSize(const std::string& file)
		{
			std::ifstream input(file);
			input.seekg(0, std::ios::end);

			unsigned int filesize = static_cast<unsigned int>(input.tellg());

			input.close();

			return filesize;
		}



		GLuint buildShader(const std::vector<char>& sourceCode, const GLenum type)
		{
			GLuint handle = gl::CreateShader(type);

			const char *src = sourceCode.data();
			gl::ShaderSource(handle, 1, &src, nullptr);

			gl::CompileShader(handle);

			GLint err;
			gl::GetShaderiv(handle, gl::COMPILE_STATUS, &err);

			if(err == gl::FALSE_)
			{
				int infolog_len;
				gl::GetShaderiv(handle, gl::INFO_LOG_LENGTH, &infolog_len);

				std::vector<char> infoLog;
				infoLog.reserve(infolog_len);
				gl::GetShaderInfoLog(handle, infolog_len, nullptr, infoLog.data());

				std::cout << "Fehler beim Kompilieren des Shaders: " << handle << std::endl << "Folgender Fehler trat auf: " << std::endl << infoLog.data() << std::endl;

				gl::DeleteShader(handle);
			}

			return handle;
		}



		void buildProgram(GLuint &programHandle, GLuint &vertexShaderHandle, GLuint &fragmentShaderHandle)
		{
			programHandle = gl::CreateProgram();

			gl::AttachShader(programHandle, vertexShaderHandle);
			gl::AttachShader(programHandle, fragmentShaderHandle);

			gl::LinkProgram(programHandle);

			GLint err;
			gl::GetProgramiv(programHandle, gl::LINK_STATUS, &err);

			if(err == gl::FALSE_)
			{
				int infolog_len;
				gl::GetProgramiv(programHandle, gl::INFO_LOG_LENGTH, &infolog_len);

				std::vector<char> infoLog;
				infoLog.reserve(infolog_len);
				gl::GetProgramInfoLog(programHandle, infolog_len, nullptr, infoLog.data());

				std::cout << "Fehler beim Linken des Programms: " << programHandle << std::endl << "Folgender Fehler trat auf:" << std::endl << infoLog.data() << std::endl;

				gl::DeleteProgram(programHandle);
			}
		}
	}
	//	=======================================================================================================================================================================================		#####





	// Texturen																																													 Texturen
	//	=======================================================================================================================================================================================		#####
	namespace textures
	{
		// Textur Methoden
		const GLubyte* LoadImageFile(const std::string& sourceFile, int &width, int &height, int &components, GLenum &format)
		{
			std::ifstream file(sourceFile, std::fstream::in);

			if(!file.is_open())
			{
				std::cout << "Datei " << sourceFile << " existiert nicht!" << std::endl;
				return nullptr;
			}

			file.close();

			GLubyte *image_bits = nullptr;

			FREE_IMAGE_FORMAT image_format = FIF_UNKNOWN;

			image_format = FreeImage_GetFileType(sourceFile.c_str(), 0);

			if(image_format == FIF_UNKNOWN)
				image_format = FreeImage_GetFIFFromFilename(sourceFile.c_str());

			if(image_format == FIF_UNKNOWN)
			{
				std::cout << "Fehler beim Lesen der Datei: " << sourceFile << std::endl << "Datei-Format unbekannt" << std::endl;
				return nullptr;
			}

			if(FreeImage_FIFSupportsReading(image_format))
			{
				FIBITMAP *bitmap = FreeImage_Load(image_format, sourceFile.c_str(), 0);

				int bpp = FreeImage_GetBPP(bitmap);
				int depth = bpp / 8;

				width = FreeImage_GetWidth(bitmap);

				height = FreeImage_GetHeight(bitmap);

				image_bits = FreeImage_GetBits(bitmap);

				switch(depth)
				{
				case 4:
					format = gl::BGRA;
					components = gl::RGBA;
					break;

				case 3:
					format = gl::BGR;
					components = gl::RGB;
					break;

					//case 1:
					//	format = GL_LUMINANCE;
					//	components = GL_LUMINANCE;
					//	break;
				default:
					format = gl::BGR;
					components = gl::RGB;
					break;
				}
			}

			return image_bits;
		}



		void WriteImageFile(const std::string& targetFile, unsigned int width, unsigned int height, unsigned int depth, GLubyte *imageData)
		{
			FREE_IMAGE_FORMAT image_format = FreeImage_GetFIFFromFilename(targetFile.c_str());

			if(FreeImage_FIFSupportsWriting(image_format))
			{
				FIBITMAP *bitmap = FreeImage_Allocate(width, height, depth);

				for(unsigned int y = 0, i = 0; y < height; ++y)
				{
					for(unsigned int x = 0; x < width; ++x, i += (depth >> 3))
					{
						RGBQUAD color;

						color.rgbBlue = imageData[i];
						color.rgbGreen = imageData[i + 1];
						color.rgbRed = imageData[i + 2];
						//color.rgbReserved = 1; // imageData[i + 3];

						FreeImage_SetPixelColor(bitmap, x, y, &color);
					}
				}

				FreeImage_Save(image_format, bitmap, targetFile.c_str());
			}
		}
	}
	//	=======================================================================================================================================================================================		#####





	// Mathematik																																											   Mathematik
	//	=======================================================================================================================================================================================		#####
	namespace math
	{
		float round(const float in)
		{
			return (in > 0.0f) ? std::floor(in + 0.5f) : std::ceil(in - 0.5f);
		}



		double round(const double in)
		{
			return (in > 0.0) ? std::floor(in + 0.5) : std::ceil(in - 0.5);
		}




		float Radians(const float in)
		{
			return in * (float) M_PI / 180.f;
		}



		double Radians(const double in)
		{
			return in * M_PI / 180.0;
		}



		float Degree(const float in)
		{
			return in * 180.f / (float) M_PI;
		}



		double Degree(const double in)
		{
			return in * 180.0 / M_PI;
		}
	}
	//	=======================================================================================================================================================================================		#####
}