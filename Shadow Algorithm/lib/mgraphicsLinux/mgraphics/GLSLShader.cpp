// Includes																																														 Includes
//	===========================================================================================================================================================================================		#####
#include <GLSLShader.h>

#include <iostream>

#include <mgraphics.h>
//	===========================================================================================================================================================================================		#####




// Usings																																														   Usings
//	===========================================================================================================================================================================================		#####
using mgraphics::shader::getFileSize;
//	===========================================================================================================================================================================================		#####




// GLSLShader																																												   GLSLShader
//	===========================================================================================================================================================================================		#####
mgraphics::shader::GLSLShader::GLSLShader(const std::string& sourcePath, GLenum type)
{
	std::vector<char> source;

	unsigned int filesize = getFileSize(sourcePath);

	source.reserve(filesize + 1);

	readFile(sourcePath, source);

	buildShader(source, sourcePath, type);
}

void mgraphics::shader::GLSLShader::buildShader(const std::vector<char>& sourceCode, const std::string& filepath, const GLenum type)
{
	shader = gl::CreateShader(type);

	const char* src = sourceCode.data();
	gl::ShaderSource(shader, 1, &src, nullptr);

	gl::CompileShader(shader);

	GLint err;
	gl::GetShaderiv(shader, gl::COMPILE_STATUS, &err);

	if(err == gl::FALSE_)
	{
		int infolog_len;
		gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &infolog_len);

		std::unique_ptr<char[]> infoLog = std::make_unique<char[]>(infolog_len);
		gl::GetShaderInfoLog(shader, infolog_len, nullptr, infoLog.get());
		
		std::cout << "Fehler beim Kompilieren des Shaders: " << filepath << std::endl << "Folgender Fehler trat auf: " << std::endl << infoLog.get() << std::endl;

		gl::DeleteShader(shader);
	}
}

mgraphics::shader::GLSLShader::~GLSLShader()
{
	gl::DeleteShader(shader);
}
//	===========================================================================================================================================================================================		#####




// GLSLShaderprogram																																									GLSLShaderprogram
//	===========================================================================================================================================================================================		#####
mgraphics::shader::GLSLShaderprogram::GLSLShaderprogram()
{
	program = gl::CreateProgram();
}


mgraphics::shader::GLSLShaderprogram::GLSLShaderprogram(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	std::unique_ptr<GLSLShader> vertex(new GLSLShader(vertexShaderPath, gl::VERTEX_SHADER));
	std::unique_ptr<GLSLShader> fragment(new GLSLShader(fragmentShaderPath, gl::FRAGMENT_SHADER));

	program = gl::CreateProgram();

	attachShader(vertex);
	attachShader(fragment);

	bind();
}


mgraphics::shader::GLSLShaderprogram::~GLSLShaderprogram(void)
{
	gl::DeleteProgram(program);
}


void mgraphics::shader::GLSLShaderprogram::attachShader(const std::unique_ptr<GLSLShader>& shader)
{
	gl::AttachShader(program, shader->shader);
}


void mgraphics::shader::GLSLShaderprogram::bind()
{
	gl::LinkProgram(program);

	GLint err;
	gl::GetProgramiv(program, gl::LINK_STATUS, &err);

	if(err == gl::FALSE_)
	{
		int infolog_len;
		gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &infolog_len);

		std::unique_ptr<char[]> infoLog = std::make_unique<char[]>(infolog_len);
		gl::GetProgramInfoLog(program, infolog_len, nullptr, infoLog.get());

		std::cout << "Fehler beim Linken des Programms: " << program << std::endl << "Folgender Fehler trat auf:" << std::endl << infoLog.get() << std::endl;

		gl::DeleteProgram(program);
	}
}


void mgraphics::shader::GLSLShaderprogram::use(void)
{
	gl::UseProgram(program);
}


GLuint mgraphics::shader::GLSLShaderprogram::getUniformLocation(const std::string& name)
{
	return gl::GetUniformLocation(program, name.c_str());
}


GLuint mgraphics::shader::GLSLShaderprogram::getAttribLocation(const std::string& name)
{
	return gl::GetAttribLocation(program, name.c_str());
}


GLuint mgraphics::shader::GLSLShaderprogram::getSubroutineIndex(unsigned int shaderType, const std::string& name)
{
	return gl::GetSubroutineIndex(program, shaderType, name.c_str());
}
//	===========================================================================================================================================================================================		#####
