#pragma once



// Usings																																														   Usings
//	===========================================================================================================================================================================================		#####
#include <gl_core_4_4.hpp>
#include <string>
//	===========================================================================================================================================================================================		#####



namespace mgraphics
{
	namespace datatypes
	{
		// Forward Declarations																																						 Forward Declarations
		//	===================================================================================================================================================================================		#####
		class Vector2;
		class Vector3;
		class Vector4;
		class Matrix2;
		class Matrix3;
		class Matrix4;
		class Quaternion;

		// Fight the linker errors with friend operators
		Vector2 operator*(const GLfloat l, const Vector2 &r);
		Vector2 operator/(const GLfloat l, const Vector2 &r);
		Vector3 operator*(const GLfloat l, const Vector3 &r);
		Vector3 operator/(const GLfloat l, const Vector3 &r);
		Vector4 operator*(const GLfloat l, const Vector4 &r);
		Vector4 operator/(const GLfloat l, const Vector4 &r);
		Matrix2 operator*(const GLfloat l, const Matrix2 &r);
		Matrix2 operator/(const GLfloat l, const Matrix2 &r);
		Matrix3 operator*(const GLfloat l, const Matrix3 &r);
		Matrix3 operator/(const GLfloat l, const Matrix3 &r);
		Matrix4 operator*(const GLfloat l, const Matrix4 &r);
		Matrix4 operator/(const GLfloat l, const Matrix4 &r);
		//	===================================================================================================================================================================================		#####
		


		// Vector2																																												  Vector2
		//	===================================================================================================================================================================================		#####
		class Vector2
		{
			GLfloat data[2];

		public:
			Vector2(void);
			Vector2(GLfloat x, GLfloat y);
			Vector2(const Vector3 &xy);
			Vector2(const Vector4 &xy);
			~Vector2(void);

			GLfloat X(void) const
			{
				return data[0];
			}
			void X(GLfloat x)
			{
				data[0] = x;
			}
			GLfloat Y(void) const
			{
				return data[1];
			}
			void Y(GLfloat y)
			{
				data[1] = y;
			}
			GLfloat* Data()
			{
				return data;
			}

			void normalize(void);
			GLfloat length(void) const;
			GLfloat Dot(const Vector2 &r) const;

			Vector2 operator*(const GLfloat r) const;
			friend Vector2 operator*(const GLfloat l, const Vector2 &r);
			Vector2 operator/(const GLfloat r) const;
			friend Vector2 operator/(const GLfloat l, const Vector2 &r);
			Vector2 operator+(const Vector2 &r) const;
			Vector2 operator+() const;
			Vector2 operator-(const Vector2 &r) const;
			Vector2 operator-() const;
			Vector2 operator=(const Vector2 &r);
			Vector2 operator*=(const GLfloat r);
			Vector2 operator/=(const GLfloat r);
			Vector2 operator+=(const Vector2 &r);
			Vector2 operator-=(const Vector2 &r);
			bool operator==(const Vector2 &r) const;
			bool operator!=(const Vector2 &r) const;

			static const Vector2 UnitX;
			static const Vector2 UnitY;
			static const Vector2 Zero;
		};
		//	===================================================================================================================================================================================		#####
		


		// Vector3																																												  Vector3
		//	===================================================================================================================================================================================		#####
		class Vector3
		{
			GLfloat data[3];

		public:
			Vector3(void);
			Vector3(const Vector2 &xy, GLfloat z);
			Vector3(GLfloat x, const Vector2 &yz);
			Vector3(GLfloat x, GLfloat y, GLfloat z);
			Vector3(const Vector4 &xyz);
			~Vector3(void);

			GLfloat X() const
			{
				return data[0];
			}
			void X(GLfloat x)
			{
				data[0] = x;
			}
			GLfloat Y() const
			{
				return data[1];
			}
			void Y(GLfloat y)
			{
				data[1] = y;
			}
			GLfloat Z() const
			{
				return data[2];
			}
			void Z(GLfloat z)
			{
				data[2] = z;
			}
			GLfloat* Data()
			{
				return data;
			}

			void normalize();
			GLfloat length() const;
			GLfloat Dot(const Vector3 &r) const;
			Vector3 Cross(const Vector3 &r) const;

			Vector3 operator*(const GLfloat r) const;
			friend Vector3 operator*(const GLfloat l, const Vector3 &r);
			Vector3 operator/(const GLfloat r) const;
			friend Vector3 operator/(const GLfloat l, const Vector3 &r);
			Vector3 operator+(const Vector3 &r) const;
			Vector3 operator+() const;
			Vector3 operator-(const Vector3 &r) const;
			Vector3 operator-() const;
			Vector3 operator=(const Vector3 &r);
			Vector3 operator*=(const GLfloat r);
			Vector3 operator/=(const GLfloat r);
			Vector3 operator+=(const Vector3 &r);
			Vector3 operator-=(const Vector3 &r);
			bool operator==(const Vector3 &r) const;
			bool operator!=(const Vector3 &r) const;

			static const Vector3 UnitX;
			static const Vector3 UnitY;
			static const Vector3 UnitZ;
			static const Vector3 Zero;
		};
		//	===================================================================================================================================================================================		#####
		


		// Vector4																																												  Vector4
		//	===================================================================================================================================================================================		#####
		class Vector4
		{
			GLfloat data[4];

		public:
			Vector4(void);
			Vector4(GLfloat x, GLfloat y, GLfloat z, GLfloat w);
			Vector4(const Vector2 &xy, GLfloat z, GLfloat w);
			Vector4(GLfloat x, const Vector2 &yz, GLfloat w);
			Vector4(GLfloat x, GLfloat y, const Vector2 &zw);
			Vector4(const Vector2 &xy, const Vector2 &zw);
			Vector4(const Vector3 &xyz, GLfloat w);
			Vector4(GLfloat x, const Vector3 &yzw);
			~Vector4(void);

			GLfloat X() const
			{
				return data[0];
			}
			void X(GLfloat x)
			{
				data[0] = x;
			}
			GLfloat Y() const
			{
				return data[1];
			}
			void Y(GLfloat y)
			{
				data[1] = y;
			}
			GLfloat Z() const
			{
				return data[2];
			}
			void Z(GLfloat z)
			{
				data[2] = z;
			}
			GLfloat W() const
			{
				return data[3];
			}
			void W(GLfloat w)
			{
				data[3] = w;
			}
			GLfloat* Data()
			{
				return data;
			}
			
			void normalize();
			GLfloat length() const;
			GLfloat Dot(const Vector4 &r) const;

			Vector4 operator*(const GLfloat r) const;
			friend Vector4 operator*(const GLfloat l, const Vector4 &r);
			Vector4 operator/(const GLfloat r) const;
			friend Vector4 operator/(const GLfloat l, const Vector4 &r);
			Vector4 operator+(const Vector4 &r) const;
			Vector4 operator+() const;
			Vector4 operator-(const Vector4 &r) const;
			Vector4 operator-() const;
			Vector4 operator=(const Vector4 &r);
			Vector4 operator*=(const GLfloat r);
			Vector4 operator/=(const GLfloat r);
			Vector4 operator+=(const Vector4 &r);
			Vector4 operator-=(const Vector4 &r);
			bool operator==(const Vector4 &r) const;
			bool operator!=(const Vector4 &r) const;

			static const Vector4 UnitX;
			static const Vector4 UnitY;
			static const Vector4 UnitZ;
			static const Vector4 UnitW;
			static const Vector4 Zero;
		};
		//	===================================================================================================================================================================================		#####



		// Matrix2																																												  Matrix2
		//	===================================================================================================================================================================================		#####
		//
		// a11 (data0)		a12 (data2)
		// a21 (data1)		a22 (data3)
		class Matrix2
		{
			GLfloat data[4];

		public:
			Matrix2(void);
			Matrix2(const Matrix3 &m);
			Matrix2(const Matrix4 &m);
			Matrix2(const Vector2 &column1, const Vector2 &column2);
			Matrix2(const GLfloat a11, const GLfloat a12, const GLfloat a21, const GLfloat a22);
			~Matrix2(void);

			Matrix2 operator*(const Matrix2 &r) const;
			Matrix2 operator*(const GLfloat r) const;
			friend Matrix2 operator*(const GLfloat l, const Matrix2 &r);
			Vector2 operator*(Vector2 &r) const;
			Matrix2 operator/(const GLfloat r) const;
			friend Matrix2 operator/(const GLfloat l, const Matrix2 &r);
			Matrix2 operator+(const Matrix2 &r) const;
			Matrix2 operator+() const;
			Matrix2 operator-(const Matrix2 &r) const;
			Matrix2 operator-() const;
			Matrix2 operator*=(const GLfloat r);
			Matrix2 operator*=(const Matrix2 &m);
			Matrix2 operator/=(const GLfloat r);
			Matrix2 operator+=(const Matrix2 &r);
			Matrix2 operator-=(const Matrix2 &r);
			Matrix2 operator=(const Matrix2 &r);
			bool operator==(const Matrix2 &r) const;
			bool operator!=(const Matrix2 &r) const;

			GLfloat A11() const
			{
				return data[0];
			}
			void A11(GLfloat v)
			{
				data[0] = v;
			}
			GLfloat A12() const
			{
				return data[2];
			}
			void A12(GLfloat v)
			{
				data[2] = v;
			}
			GLfloat A21() const
			{
				return data[1];
			}
			void A21(GLfloat v)
			{
				data[1] = v;
			}
			GLfloat A22() const
			{
				return data[3];
			}
			void A22(GLfloat v)
			{
				data[3] = v;
			}
			GLfloat* Data()
			{
				return data;
			}

			static Matrix2 Transpose(const Matrix2 &in);
			static Matrix2 Invert(const Matrix2 &in);
			static Matrix2 Rotate(const GLfloat angle);
			static GLfloat Determinant(const Matrix2 &in);
			static Matrix2 Adjugate(const Matrix2 &in);
			static Matrix2 Identity();
		};
		//	===================================================================================================================================================================================		#####



		// Matrix3																																												  Matrix3
		//	===================================================================================================================================================================================		#####
		//
		// a11 (data0)		a12 (data3)		a13 (data6)
		// a21 (data1)		a22 (data4)		a23 (data7)
		// a31 (data2)		a32 (data5)		a33 (data8)
		class Matrix3
		{
			GLfloat data[9];

		public:
			Matrix3(void);
			Matrix3(const Matrix2 &m);
			Matrix3(const Matrix4 &m);
			Matrix3(const Vector3 &column1, const Vector3 &column2, const Vector3 &column3);
			Matrix3(const GLfloat a11, const GLfloat a12, const GLfloat a13, const GLfloat a21, const GLfloat a22, const GLfloat a23, const GLfloat a31, const GLfloat a32, const GLfloat a33);
			~Matrix3(void);

			Matrix3 operator*(const Matrix3 &r) const;
			Matrix3 operator*(const GLfloat r) const;
			friend Matrix3 operator*(const GLfloat l, const Matrix3 &r);
			Vector3 operator*(const Vector3 &r) const;
			Matrix3 operator/(const GLfloat r) const;
			friend Matrix3 operator/(const GLfloat l, const Matrix3 &r);
			Matrix3 operator+(const Matrix3 &r) const;
			Matrix3 operator+() const;
			Matrix3 operator-(const Matrix3 &r) const;
			Matrix3 operator-() const;
			Matrix3 operator*=(const GLfloat r);
			Matrix3 operator*=(const Matrix3 &m);
			Matrix3 operator/=(const GLfloat r);
			Matrix3 operator+=(const Matrix3 &r);
			Matrix3 operator-=(const Matrix3 &r);
			Matrix3 operator=(const Matrix3 &r);
			bool operator==(const Matrix3 &r) const;
			bool operator!=(const Matrix3 &r) const;

			GLfloat A11() const
			{
				return data[0];
			}
			void A11(GLfloat v)
			{
				data[0] = v;
			}
			GLfloat A12() const
			{
				return data[3];
			}
			void A12(GLfloat v)
			{
				data[3] = v;
			}
			GLfloat A13() const
			{
				return data[6];
			}
			void A13(GLfloat v)
			{
				data[6] = v;
			}
			GLfloat A21() const
			{
				return data[1];
			}
			void A21(GLfloat v)
			{
				data[1] = v;
			}
			GLfloat A22() const
			{
				return data[4];
			}
			void A22(GLfloat v)
			{
				data[4] = v;
			}
			GLfloat A23() const
			{
				return data[7];
			}
			void A23(GLfloat v)
			{
				data[7] = v;
			}
			GLfloat A31() const
			{
				return data[2];
			}
			void A31(GLfloat v)
			{
				data[2] = v;
			}
			GLfloat A32() const
			{
				return data[5];
			}
			void A32(GLfloat v)
			{
				data[5] = v;
			}
			GLfloat A33() const
			{
				return data[8];
			}
			void A33(GLfloat v)
			{
				data[8] = v;
			}
			GLfloat* Data()
			{
				return data;
			}

			static Matrix3 Transpose(const Matrix3 &in);
			static Matrix3 Invert(const Matrix3 &in);
			static Matrix3 Rotate(const GLfloat angle, const Vector3 &axis);
			static Matrix3 RotateFromQuaternion(const Quaternion &rotation);
			static GLfloat Determinant(const Matrix3 &in);
			static Matrix3 Adjugate(const Matrix3 &in);
			static Matrix3 Identity();
		};
		//	===================================================================================================================================================================================		#####



		// Matrix4																																												  Matrix4
		//	===================================================================================================================================================================================		#####
		//
		// a11 (data0)		a12 (data4)		a13 (data8)		a14 (data12)
		// a21 (data1)		a22 (data5)		a23 (data9)		a24 (data13)
		// a31 (data2)		a32 (data6)		a33 (data10)		a34 (data14)
		// a41 (data3)		a42 (data7)		a43 (data11)		a44 (data15)
		class Matrix4
		{
			GLfloat data[16];

		public:
			Matrix4(void);
			~Matrix4(void);
			Matrix4(const Matrix2 &m);
			Matrix4(const Matrix3 &m);
			Matrix4(const Vector4 &column1, const Vector4 &column2, const Vector4 &column3, const Vector4 &column4);
			Matrix4(const GLfloat a11, const GLfloat a12, const GLfloat a13, const GLfloat a14, const GLfloat a21, const GLfloat a22, const GLfloat a23, const GLfloat a24, const GLfloat a31, const GLfloat a32, const GLfloat a33, const GLfloat a34, const GLfloat a41, const GLfloat a42, const GLfloat a43, const GLfloat a44);

			Matrix4 operator*(const Matrix4 &r) const;
			Matrix4 operator*(const GLfloat r) const;
			friend Matrix4 operator*(const GLfloat l, const Matrix4 &r);
			Vector4 operator*(const Vector4 &r) const;
			Matrix4 operator/(const GLfloat r) const;
			friend Matrix4 operator/(const GLfloat l, const Matrix4 &r);
			Matrix4 operator+(const Matrix4 &r) const;
			Matrix4 operator+() const;
			Matrix4 operator-(const Matrix4 &r) const;
			Matrix4 operator-() const;
			Matrix4 operator*=(const GLfloat r);
			Matrix4 operator*=(const Matrix4 &m);
			Matrix4 operator/=(const GLfloat r);
			Matrix4 operator+=(const Matrix4 &r);
			Matrix4 operator-=(const Matrix4 &r);
			Matrix4 operator=(const Matrix4 &r);
			bool operator==(const Matrix4 &r) const;
			bool operator!=(const Matrix4 &r) const;

			GLfloat A11() const
			{
				return data[0];
			}
			void A11(GLfloat v)
			{
				data[0] = v;
			}
			GLfloat A12() const
			{
				return data[4];
			}
			void A12(GLfloat v)
			{
				data[4] = v;
			}
			GLfloat A13() const
			{
				return data[8];
			}
			void A13(GLfloat v)
			{
				data[8] = v;
			}
			GLfloat A14() const
			{
				return data[12];
			}
			void A14(GLfloat v)
			{
				data[12] = v;
			}
			GLfloat A21() const
			{
				return data[1];
			}
			void A21(GLfloat v)
			{
				data[1] = v;
			}
			GLfloat A22() const
			{
				return data[5];
			}
			void A22(GLfloat v)
			{
				data[5] = v;
			}
			GLfloat A23() const
			{
				return data[9];
			}
			void A23(GLfloat v)
			{
				data[9] = v;
			}
			GLfloat A24() const
			{
				return data[13];
			}
			void A24(GLfloat v)
			{
				data[13] = v;
			}
			GLfloat A31() const
			{
				return data[2];
			}
			void A31(GLfloat v)
			{
				data[2] = v;
			}
			GLfloat A32() const
			{
				return data[6];
			}
			void A32(GLfloat v)
			{
				data[6] = v;
			}
			GLfloat A33() const
			{
				return data[10];
			}
			void A33(GLfloat v)
			{
				data[10] = v;
			}
			GLfloat A34() const
			{
				return data[14];
			}
			void A34(GLfloat v)
			{
				data[14] = v;
			}
			GLfloat A41() const
			{
				return data[3];
			}
			void A41(GLfloat v)
			{
				data[3] = v;
			}
			GLfloat A42() const
			{
				return data[7];
			}
			void A42(GLfloat v)
			{
				data[7] = v;
			}
			GLfloat A43() const
			{
				return data[11];
			}
			void A43(GLfloat v)
			{
				data[11] = v;
			}
			GLfloat A44() const
			{
				return data[15];
			}
			void A44(GLfloat v)
			{
				data[15] = v;
			}
			GLfloat* Data()
			{
				return data;
			}

			static Matrix4 Transpose(const Matrix4 &in);
			static Matrix4 Invert(const Matrix4 &in);
			static Matrix4 Rotate(const GLfloat angle, const Vector3 &axis);
			static Matrix4 RotateFromQuaternion(const Quaternion &rotation);
			static Matrix4 Translate(const GLfloat x, const GLfloat y, const GLfloat z);
			static Matrix4 Translate(const Vector3 &translation);
			static GLfloat Determinant(const Matrix4 &in);
			static Matrix4 Adjugate(const Matrix4 &in);
			static Matrix4 Identity();
			static Matrix4 Orthographic(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearZ, const GLfloat farZ);
			static Matrix4 Orthographic(const GLfloat width, const GLfloat height, const GLfloat nearZ, const GLfloat farZ);
			static Matrix4 Perspective(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearZ, const GLfloat farZ);
			static Matrix4 Perspective(const GLfloat fovy, const GLfloat aspect, const GLfloat nearZ, const GLfloat farZ);
			static Matrix4 Scale(const GLfloat x, const GLfloat y, const GLfloat z);
			static Matrix4 Scale(const Vector3 &scale);
		};
		//	===================================================================================================================================================================================		#####



		// Quaternion																																										   Quaternion
		//	===================================================================================================================================================================================		#####
		// w + xi + yj + zk
		class Quaternion
		{
			//Vector4 q;
			float _w, _x, _y, _z;

		public:
			Quaternion();
			Quaternion(const GLfloat w, const GLfloat x, const GLfloat y, const GLfloat z);
			Quaternion(const Vector2 &wx, const GLfloat y, const GLfloat z);
			Quaternion(const GLfloat w, const Vector2 &xy, const GLfloat z);
			Quaternion(const GLfloat w, const GLfloat x, const Vector2 &yz);
			Quaternion(const Vector2 &wx, const Vector2 &yz);
			Quaternion(const Vector3 &wxy, const GLfloat z);
			Quaternion(const GLfloat w, const Vector3 &xyz);
			Quaternion(const Vector4 &wxyz);
			~Quaternion();

			GLfloat W() const
			{
				return _w;
			}
			void W(GLfloat w_value)
			{
				_w = w_value;
			}
			GLfloat X() const
			{
				return _x;
			}
			void X(GLfloat x_value)
			{
				_x = x_value;
			}
			GLfloat Y() const
			{
				return _y;
			}
			void Y(GLfloat y_value)
			{
				_y = y_value;
			}
			GLfloat Z() const
			{
				return _z;
			}
			void Z(GLfloat z_value)
			{
				_z = z_value;
			}

			void normalize();
			Quaternion conjugate() const;
			void ConvertToAxisAngle(Vector3 &axis, float &angle);

			Vector3 operator*(const Vector3 &v) const;
			Quaternion operator*(const Quaternion &r) const;
			Quaternion operator+(const Quaternion &r) const;
			Quaternion operator-(const Quaternion &r) const;
			Quaternion operator=(const Quaternion &r);
			bool operator==(const Quaternion &r) const;
			bool operator!=(const Quaternion &r) const;

			static Quaternion CreateFromAxisAngle(const Vector3 &axis, const float angle);
			static Quaternion CreateFromEulerAngles(const float pitch, const float yaw, const float roll);
			static Quaternion Identity();
		};
		//	===================================================================================================================================================================================		#####



		// NotInvertibleException																																				   NotInvertibleException
		//	===================================================================================================================================================================================		#####
		class NotInvertibleException
		{
			std::string err; 

		public:
			NotInvertibleException()
			{
				err = "Matrix is not invertible";
			}
			~NotInvertibleException(){}

			std::string getMessage()
			{
				return err;
			}
		};
		//	===================================================================================================================================================================================		#####
	}
}
