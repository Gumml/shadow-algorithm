#pragma once

#include <iostream>
#include <cuda_runtime.h>
#include <CL/cl.h>

namespace gpu
{
	namespace error
	{
		namespace cuda
		{
			static void check(cudaError e)
			{
				if(e != cudaSuccess)
				{
					printf("Cuda Error: %d\nLog: %s\n", e, cudaGetErrorString(e));
				}
			}
		}


		namespace opencl
		{
			static void check(cl_int e)
			{
				if(e != CL_SUCCESS)
				{
					std::string err = "";

					switch(e) 
					{
					case CL_DEVICE_NOT_FOUND:
						err.append("Device not found.");
						break;

					case CL_DEVICE_NOT_AVAILABLE:
						err.append("Device not available");
						break;

					case CL_COMPILER_NOT_AVAILABLE:
						err.append("Compiler not available");
						break;

					case CL_MEM_OBJECT_ALLOCATION_FAILURE:
						err.append("Memory object allocation failure");
						break;

					case CL_OUT_OF_RESOURCES:
						err.append("Out of resources");
						break;

					case CL_OUT_OF_HOST_MEMORY:
						err.append("Out of host memory");
						break;

					case CL_PROFILING_INFO_NOT_AVAILABLE:
						err.append("Profiling information not available");
						break;

					case CL_MEM_COPY_OVERLAP:
						err.append("Memory copy overlap");
						break;

					case CL_IMAGE_FORMAT_MISMATCH:
						err.append("Image format mismatch");
						break;

					case CL_IMAGE_FORMAT_NOT_SUPPORTED:
						err.append("Image format not supported");
						break;

					case CL_BUILD_PROGRAM_FAILURE:
						err.append("Program build failure");
						break;

					case CL_MAP_FAILURE:
						err.append("Map failure");
						break;

					case CL_INVALID_VALUE:
						err.append("Invalid value");
						break;

					case CL_INVALID_DEVICE_TYPE:
						err.append("Invalid device type");
						break;

					case CL_INVALID_PLATFORM:
						err.append("Invalid platform");
						break;

					case CL_INVALID_DEVICE:
						err.append("Invalid device");
						break;

					case CL_INVALID_CONTEXT:
						err.append("Invalid context");
						break;

					case CL_INVALID_QUEUE_PROPERTIES:
						err.append("Invalid queue properties");
						break;

					case CL_INVALID_COMMAND_QUEUE:
						err.append("Invalid command queue");
						break;

					case CL_INVALID_HOST_PTR:
						err.append("Invalid host pointer");
						break;

					case CL_INVALID_MEM_OBJECT:
						err.append("Invalid memory object");
						break;

					case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
						err.append("Invalid image format descriptor");
						break;

					case CL_INVALID_IMAGE_SIZE:
						err.append("Invalid image size");
						break;

					case CL_INVALID_SAMPLER:
						err.append("Invalid sampler");
						break;

					case CL_INVALID_BINARY:
						err.append("Invalid binary");
						break;

					case CL_INVALID_BUILD_OPTIONS:
						err.append("Invalid build options");
						break;

					case CL_INVALID_PROGRAM:
						err.append("Invalid program");
						break;

					case CL_INVALID_PROGRAM_EXECUTABLE:
						err.append("Invalid program executable");
						break;

					case CL_INVALID_KERNEL_NAME:
						err.append("Invalid kernel name");
						break;

					case CL_INVALID_KERNEL_DEFINITION:
						err.append("Invalid kernel definition");
						break;

					case CL_INVALID_KERNEL:
						err.append("Invalid kernel");
						break;

					case CL_INVALID_ARG_INDEX:
						err.append("Invalid argument index");
						break;

					case CL_INVALID_ARG_VALUE:
						err.append("Invalid argument value");
						break;

					case CL_INVALID_ARG_SIZE:
						err.append("Invalid argument size");
						break;

					case CL_INVALID_KERNEL_ARGS:
						err.append("Invalid kernel arguments");
						break;

					case CL_INVALID_WORK_DIMENSION:
						err.append("Invalid work dimension");
						break;

					case CL_INVALID_WORK_GROUP_SIZE:
						err.append("Invalid work group size");
						break;

					case CL_INVALID_WORK_ITEM_SIZE:
						err.append("Invalid work item size");
						break;

					case CL_INVALID_GLOBAL_OFFSET:
						err.append("Invalid global offset");
						break;

					case CL_INVALID_EVENT_WAIT_LIST:
						err.append("Invalid event wait list");
						break;

					case CL_INVALID_EVENT:
						err.append("Invalid event");
						break;

					case CL_INVALID_OPERATION:
						err.append("Invalid operation");
						break;

					case CL_INVALID_GL_OBJECT:
						err.append("Invalid OpenGL object");
						break;

					case CL_INVALID_BUFFER_SIZE:
						err.append("Invalid buffer size");
						break;

					case CL_INVALID_MIP_LEVEL:                  
						err.append("Invalid mip-map level");
						break;

					default: 
						err.append("Unknown Error");
						break;
					}

					printf("OpenCL Error %s\n", err.c_str());
				}
			}
		}
	}
}