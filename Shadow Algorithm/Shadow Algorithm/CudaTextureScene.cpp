// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <CudaTextureScene.h>

#include <iostream>

#include <mgraphics.h>
#include <FBXModel.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <CudaTextureScene.cuh>

#include <CudaError.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using std::cout;
using std::endl;

using mgraphics::fbxmodel::FBXModel;
using mgraphics::fbxmodel::Mesh;

using mgraphics::textures::LoadImageFile;
// =================================================================================================================================================================================================	#####




// Methoden																																															 Methoden
// =================================================================================================================================================================================================	#####
CudaTextureScene::CudaTextureScene(void)
{
	cout << "Cuda Texture Scene created" << endl;
}


CudaTextureScene::~CudaTextureScene(void)
{
	cout << "Cuda Texture Scene deleted" << endl;

	gl::DeleteVertexArrays(canvas->meshCount, canvas->meshVAOs.get());
	gl::DeleteBuffers(canvas->meshCount, canvas->meshVBOs.get());

	gl::DeleteTextures(1, &cudatexture);
	gl::DeleteSamplers(1, &texturesampler);

	gpu::error::cuda::check(cudaGraphicsUnregisterResource(resources[0]));
	gpu::error::cuda::check(cudaGraphicsUnregisterResource(resources[1]));

	gpu::error::cuda::check(cudaDeviceReset());
}


void CudaTextureScene::init()
{
	gl::ClearColor(.2353f, .2353f, .2353f, 1.f);
	gl::Enable(gl::DEPTH_TEST);

	initTextures();
	initShader();
	initGeometry();
	initCuda();

	framectr = std::make_unique<statistics::FrameCounter>();
}


void CudaTextureScene::render()
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	cudaPass();

	Matrix4 mvp = projection * model;

	shader->use();

	gl::UniformMatrix4fv(locMatrices_mvp, 1, gl::FALSE_, mvp.Data());

	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &locSubroutine_vertex);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &locSubroutine_fragment);

	gl::ActiveTexture(gl::TEXTURE0);
	//gl::BindTexture(gl::TEXTURE_2D, texture->getTextureID());
	gl::BindTexture(gl::TEXTURE_2D, cudatexture);
	gl::BindSampler(0, texturesampler);
	gl::Uniform1i(locTextures_image, 0);

	for(int i = 0; i < canvas->meshCount; ++i)
	{
		gl::BindVertexArray(canvas->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, canvas->verts[i]);
	}
}


void CudaTextureScene::update(GLFWwindow *window)
{
	framectr->tick(window);
}


void CudaTextureScene::resizeWindow(GLsizei width, GLsizei height)
{
	gl::Viewport(0, 0, width, height);
	float aspect = static_cast<float>(width) / static_cast<float>(height);
	projection = Matrix4::Perspective(60.f * static_cast<float>(M_PI) / 180.f, aspect, 1.f, 20.f);
}


//void CudaTextureScene::dispatchInput(WPARAM key)
//{
//
//}


void CudaTextureScene::dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{

}


void CudaTextureScene::initGeometry()
{
	canvas.reset(new RenderObject);

	std::unique_ptr<FBXModel> canvasmodel(new FBXModel("../models/CudaTextureScene/Canvas.fbx"));

	canvas->meshCount = canvasmodel->MeshCount();

	canvas->meshVAOs.reset(new unsigned int[canvas->meshCount]);
	canvas->meshVBOs.reset(new unsigned int[canvas->meshCount]);
	canvas->verts.reset(new unsigned int[canvas->meshCount]);

	gl::GenVertexArrays(canvas->meshCount, canvas->meshVAOs.get());
	gl::GenBuffers(canvas->meshCount, canvas->meshVBOs.get());

	unsigned int currentMesh = 0;
	std::vector<Mesh> meshes = canvasmodel->Meshes();
	for(Mesh &mesh : meshes)
	{
		canvas->verts[currentMesh] = mesh.VertexCount();

		gl::BindVertexArray(canvas->meshVAOs[currentMesh]);

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindBuffer(gl::ARRAY_BUFFER, canvas->meshVBOs[currentMesh]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, 0, nullptr);

		gl::EnableVertexAttribArray(2);
		gl::VertexAttribPointer(2, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		currentMesh++;
	}

	gl::BindVertexArray(0);
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);

	model = Matrix4::Translate(.0f, .0f, -2.f);
}


void CudaTextureScene::cudaPass()
{
	gpu::error::cuda::check(cudaGraphicsMapResources(2, resources, 0));

	cudaArray_t writeArray;
	gpu::error::cuda::check(cudaGraphicsSubResourceGetMappedArray(&writeArray, resources[0], 0, 0));

	cudaArray_t readArray;
	gpu::error::cuda::check(cudaGraphicsSubResourceGetMappedArray(&readArray, resources[1], 0, 0));

	struct cudaResourceDesc writedescription;
	memset(&writedescription, 0, sizeof(writedescription));
	writedescription.resType = cudaResourceTypeArray;
	writedescription.res.array.array = writeArray;

	cudaSurfaceObject_t write;
	gpu::error::cuda::check(cudaCreateSurfaceObject(&write, &writedescription));


	struct cudaResourceDesc readdescription;
	memset(&readdescription, 0, sizeof(readdescription));
	readdescription.resType = cudaResourceTypeArray;
	readdescription.res.array.array = readArray;

	cudaSurfaceObject_t read;
	gpu::error::cuda::check(cudaCreateSurfaceObject(&read, &readdescription));


	dim3 texDim(texture->getWidth(), texture->getHeight());
	gpu::functions::fill(write, read, texDim);


	gpu::error::cuda::check(cudaDestroySurfaceObject(read));

	gpu::error::cuda::check(cudaGraphicsUnmapResources(2, resources, 0));
}


void CudaTextureScene::initShader()
{
	shader.reset(new GLSLShaderprogram("../shader/CudaTextureScene/VertexShader.glsl", "../shader/CudaTextureScene/FragmentShader.glsl"));

	locMatrices_mvp = shader->getUniformLocation("matrices.mvp");

	locTextures_image = shader->getUniformLocation("textures.image");

	locSubroutine_vertex = shader->getSubroutineIndex(gl::VERTEX_SHADER, "cudatexture");
	locSubroutine_fragment = shader->getSubroutineIndex(gl::FRAGMENT_SHADER, "cudatexture");
}


void CudaTextureScene::initTextures()
{
	texture.reset(new Texture2D());

	int width, height, internalformat;
	GLenum format;
	GLubyte* image = const_cast<GLubyte*>(LoadImageFile("../textures/CudaTextureScene/texture.png", width, height, internalformat, format));

	texture->Generate(image, width, height, gl::RGBA, format, false);


	//texture = std::make_unique<Texture2D>();
	//texture->Generate("../textures/CudaTextureScene/texture.png");



	gl::GenSamplers(1, &texturesampler);
	gl::SamplerParameteri(texturesampler, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::SamplerParameteri(texturesampler, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
	//gl::SamplerParameteri(texturesampler, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR);
	gl::SamplerParameteri(texturesampler, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::SamplerParameteri(texturesampler, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);
	gl::SamplerParameterf(texturesampler, gl::TEXTURE_MAX_ANISOTROPY_EXT, 8.f);



	gl::GenTextures(1, &cudatexture);
	gl::BindTexture(gl::TEXTURE_2D, cudatexture);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA, texture->getWidth(), texture->getHeight(), 0, gl::RGBA, gl::FLOAT, nullptr);
	gl::BindTexture(gl::TEXTURE_2D, 0);
}


void CudaTextureScene::initCuda()
{
	gpu::error::cuda::check(cudaSetDevice(0));
	gpu::error::cuda::check(cudaGLSetGLDevice(0));

	cudaDeviceProp props;
	gpu::error::cuda::check(cudaGetDeviceProperties(&props, 0));

	gpu::error::cuda::check(cudaGraphicsGLRegisterImage(&resources[0], cudatexture, gl::TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore));
	gpu::error::cuda::check(cudaGraphicsGLRegisterImage(&resources[1], texture->getTextureID(), gl::TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore));
}
// =================================================================================================================================================================================================	#####
