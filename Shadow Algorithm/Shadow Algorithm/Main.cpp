//// Includes																																															 Includes
//// =================================================================================================================================================================================================	#####
////#define _CRTDBG_MAP_ALLOC
////#include <stdlib.h>
////#include <crtdbg.h>
////#include <vld.h>
//
//#include <Windows.h>
//#include <iostream>
//#include <string>
//#include <memory>
//
//#include <mgraphics.h>
//
//#include <Scene.h>
//#include <TestScene.h>
//#include <SMScene.h>
//#include <CudaTextureScene.h>
//#include <HeightShadowScene.h>
//#include <VolumeTracingScene.h>
//
//// Resolve conflict with cuda_gl_interop.h  --  line 448 :: DECLARE_HANDLE(HGPUNV);  commented out
//#include <wglext.h>
//// =================================================================================================================================================================================================	#####
//
//
//
//
//
//#pragma comment(lib, "opengl32.lib")
//
//
//
//
//
//// Usings																																															   Usings
//// =================================================================================================================================================================================================	#####
//using std::cout;
//using std::endl;
//using std::cin;
//// =================================================================================================================================================================================================	#####
//
//
//
//
//// Methodendeklarationen																																								Methodendeklarationen
//// =================================================================================================================================================================================================	#####
//// Windowing
//LRESULT CALLBACK msgHandle(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
//bool FindBestDisplayMode(DEVMODE &best, DWORD width, DWORD height, DWORD bpp);
//bool closeWindow();
//bool openWindow(LPCWSTR title, int x, int y, int width, int height, int bitsPerPix, bool fullscreenflag, int multisamples, int majorVersion, int minorVersion, int profile);
//bool displayWindow(bool maximized = false);
//void changeWindowTitle(LPCWSTR title);
//void calcFrames(double deltaT, double refreshInterval);
//void console();
//
//typedef BOOL(APIENTRY *PFNWGLSWAPINTERVALFARPROC)(int);
//PFNWGLSWAPINTERVALFARPROC wglSwapIntervalEXT;
//
//// Program Functions
//void init();
//void kill();
//void render();
//void update();
//void resizeWindow(GLsizei width, GLsizei height);
//
//
//
//void selectScene();
//// =================================================================================================================================================================================================	#####
//
//
//
//
//// Defines																																															  Defines
//// =================================================================================================================================================================================================	#####
//// Debug
////#define CONSOLE_SUPPORT
//
//// Device
//#define CORE_PROFILE							WGL_CONTEXT_CORE_PROFILE_BIT_ARB
//#define COMPATIBILITY_PROFILE					WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
//#define FORWARD_COMPATIBILITY_PROFILE			WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
//// =================================================================================================================================================================================================	#####
//
//
//
//
//// Datenelemente																																												Datenelemente
//// =================================================================================================================================================================================================	#####
//// Windowing
//HGLRC window_render_content;
//HDC window_device_context;
//HWND window_handle;
//HINSTANCE hinstance;
//
//bool window_active = true;
//bool window_fullscreen = false;
//
//int window_width, window_height;
//
//LPCWSTR windowTitle = L"Schattentest Application";
//
//
//// Scenes
//std::unique_ptr<Scene> scene;
//
//
//int selection = 0;
//
//int lowerSelectionBorder = 0;
//int upperSelectionBorder = 4;
//// =================================================================================================================================================================================================	#####
//
//
//
//// Methoden																																															 Methoden
//// =================================================================================================================================================================================================	#####
//void console()
//{
//	// Konsole zum Debuggen der Shader
//#ifdef CONSOLE_SUPPORT
//	//AllocConsole();
//	//AttachConsole(GetCurrentProcessId());
//	//freopen("CON", "w", stdout);
//	//freopen("CON", "r", stdin);
//
//	AllocConsole();
//	AttachConsole(GetCurrentProcessId());
//	FILE *stream;
//	freopen_s(&stream, "CONOUT$", "w", stdout);
//	freopen_s(&stream, "CONIN$", "r", stdin);
//	freopen_s(&stream, "CONERR$", "r", stderr);
//
//	SetConsoleTitle(windowTitle);
//
//	std::cout << "Konsole bereit" << std::endl;
//#endif
//}
//
//
//
//
//
//LRESULT CALLBACK msgHandle(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
//{
//	switch(msg)
//	{
//	case WM_ACTIVATE:
//	{
//		if(!HIWORD(wParam))
//			window_active = true;
//
//		else
//			window_active = false;
//
//		return 0;
//	}
//
//	case WM_CLOSE:
//	{
//		kill();
//		PostQuitMessage(0);
//		return 0;
//	}
//
//	case WM_SIZE:
//	{
//		window_width = LOWORD(lParam);
//		window_height = HIWORD(lParam);
//		resizeWindow(LOWORD(lParam), HIWORD(lParam));
//		return 0;
//	}
//	case WM_KEYDOWN:
//	{
//		if(wParam == VK_ESCAPE)
//			PostQuitMessage(0);
//
//		if(wParam == VK_RIGHT)
//		{
//			selection++;
//			mgraphics::logic::clamp(selection, lowerSelectionBorder, upperSelectionBorder);
//
//			selectScene();
//		}
//
//		if(wParam == VK_LEFT)
//		{
//			selection--;
//			mgraphics::logic::clamp(selection, lowerSelectionBorder, upperSelectionBorder);
//
//			selectScene();
//		}
//
//		scene->dispatchInput(wParam);
//	}
//	}
//
//	return DefWindowProc(hwnd, msg, wParam, lParam);
//}
//
//
//
//
//
//bool closeWindow()
//{
//	if(window_fullscreen)
//	{
//		ChangeDisplaySettings(NULL, 0);
//		ShowCursor(true);
//	}
//
//	if(window_render_content)
//	{
//		wglMakeCurrent(NULL, NULL);
//		wglDeleteContext(window_render_content);
//
//		window_render_content = NULL;
//	}
//
//	if(window_device_context)
//	{
//		ReleaseDC(window_handle, window_device_context);
//
//		window_device_context = NULL;
//	}
//
//	if(window_handle)
//	{
//		DestroyWindow(window_handle);
//
//		window_handle = NULL;
//	}
//
//	UnregisterClass(L"classOpenGL", hinstance);
//
//	hinstance = NULL;
//
//	return true;
//}
//
//
//
//
//
//bool FindBestDisplayMode(DEVMODE &best, DWORD width, DWORD height, DWORD bpp)
//{
//	DEVMODE dm = { 0 };
//	dm.dmSize = sizeof(DEVMODE);
//
//	DWORD n, maxFreq = 0;
//	bool found = false;
//
//	for(n = 0; EnumDisplaySettings(NULL, n, &dm); n++)
//	{
//		if((dm.dmPelsWidth == width) &&
//			(dm.dmPelsHeight == height) &&
//			(dm.dmBitsPerPel == bpp))
//		{
//			found = true;
//
//			if(dm.dmDisplayFrequency > maxFreq)
//			{
//				maxFreq = dm.dmDisplayFrequency;
//				best = dm;
//			}
//		}
//	}
//
//	return found;
//}
//
//
//
//
//
//bool openWindow(LPCWSTR title, int x, int y, int width, int height, int bitsPerPix, bool fullscreenflag, int multisamples, int majorVersion, int minorVersion, int profile)
//{
//	UINT ERR_MSG_MSK = MB_OK | MB_ICONERROR;
//	UINT INF_MSG_MSK = MB_OK | MB_ICONINFORMATION;
//
//	GLuint pixelformat;
//	DWORD dwStyle, dwExStyle;
//
//	hinstance = GetModuleHandle(NULL);
//
//	// Window-Breite f�r Shadow Map
//	window_width = width;
//	window_height = height;
//
//	RECT winrect;
//
//
//	WNDCLASSEX winclass;
//
//	// Gr��e der Struktur
//	winclass.cbSize = sizeof(WNDCLASSEX);
//	// neu zeichnen bei jeder Bewegung + eigener Device Context
//	winclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
//	// Message Handler setzen
//	winclass.lpfnWndProc = (WNDPROC) msgHandle;
//	// Kein Speicher f�r extra Fensterdaten
//	winclass.cbClsExtra = 0;
//	winclass.cbWndExtra = 0;
//	// Instanz setzen
//	winclass.hInstance = hinstance;
//	// Icon f�r das Fenster
//	winclass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
//	// Cursor Set
//	winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
//	// F�r OpenGL kein Background
//	winclass.hbrBackground = NULL;
//	// Kein Men�
//	winclass.lpszMenuName = NULL;
//	// Fensterklassenname
//	winclass.lpszClassName = L"classOpenGL";
//	winclass.hIconSm = NULL;
//
//	// Klasse registrieren
//	if(!RegisterClassEx(&winclass))
//	{
//		MessageBox(NULL, L"Fehler beim Registrieren der Fensterklasse", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	window_fullscreen = fullscreenflag;
//
//	if(window_fullscreen)
//	{
//		// Device Mode
//		DEVMODE devmode;
//
//		// Struktur l�schen
//		memset(&devmode, 0, sizeof(devmode));
//
//		// Gr��e der Struktur
//		//devmode.dmSize = sizeof(devmode);
//		//// Width / Height
//		//devmode.dmPelsWidth = width;
//		//devmode.dmPelsHeight = height;
//		//// Bits pro Pixel
//		//devmode.dmBitsPerPel = bitsPerPix;
//		//devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
//
//		if(!FindBestDisplayMode(devmode, width, height, bitsPerPix))
//		{
//			closeWindow();
//
//			MessageBox(NULL, L"Devmode nicht gefunden", L"DEVMODE Error", ERR_MSG_MSK);
//
//			return false;
//		}
//
//
//		if(ChangeDisplaySettings(&devmode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
//		{
//			MessageBox(NULL, L"Angeforderter Fullscreen Modus nicht unterst�tzt", L"Fehler", INF_MSG_MSK);
//
//			window_fullscreen = false;
//		}
//	}
//
//
//	if(window_fullscreen)
//	{
//		dwExStyle = WS_EX_APPWINDOW;
//		dwStyle = WS_POPUP;
//	}
//	else
//	{
//		dwExStyle = WS_EX_APPWINDOW;
//		dwStyle = WS_OVERLAPPEDWINDOW;
//	}
//
//
//	winrect.left = x;
//	winrect.top = y;
//	winrect.right = width;
//	winrect.bottom = height;
//
//	AdjustWindowRectEx(&winrect, dwStyle, false, dwExStyle);
//
//	if(!(window_handle = CreateWindowEx(dwExStyle, L"classOpenGL", title, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle, winrect.left, winrect.top, winrect.right, winrect.bottom, NULL, NULL, hinstance, NULL)))
//	{
//		closeWindow();
//		MessageBox(NULL, L"Fehler beim Erzeugen des Fensters!", L"Fehler", ERR_MSG_MSK);
//		return false;
//	}
//
//
//	static PIXELFORMATDESCRIPTOR pfd =
//	{
//		// Gr��e der Struktur
//		sizeof(PIXELFORMATDESCRIPTOR),
//		// Version
//		1,
//		// Flags
//		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
//		// RGBA-Format
//		PFD_TYPE_RGBA,
//		// Bits pro Pixel
//		bitsPerPix,
//		// Farb-Bits/Shifts
//		0, 0, 0, 0, 0, 0,
//		// Alpha-Buffer
//		0,
//		// shift-bit
//		0,
//		// accumulation buffer
//		0,
//		// ignoriere accumulatoin bits
//		0, 0, 0, 0,
//		// Z-Buffer Tiefe
//		24,
//		// Stencil Buffer Tiefe
//		8,
//		// kein Auxiliary Buffer
//		0,
//		// Hauptzeichenlayer
//		PFD_MAIN_PLANE,
//		// reserviert
//		0,
//		// Layer Maske
//		0, 0, 0
//	};
//
//
//	if(!(window_device_context = GetDC(window_handle)))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Device Context konnte nicht erstellt werden!", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!(pixelformat = ChoosePixelFormat(window_device_context, &pfd)))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Kein Pixelformat gefunden!", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!SetPixelFormat(window_device_context, pixelformat, &pfd))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Setzen des Pixelformats fehlgeschlagen", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!(window_render_content = wglCreateContext(window_device_context)))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Ein Rendering Content konnte nicht erzeugt werden", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!wglMakeCurrent(window_device_context, window_render_content))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"OpenGL Rendering Content konnte nicht aktiviert werden", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//
//
//	// Multisample RC
//	//if(multisamples > 0)
//	//{
//	PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC) wglGetProcAddress("wglChoosePixelFormatARB");
//
//	//HGLRC wglCreateContextAttribsARB(HDC hDC, HGLRC hshareContext, const int *attribList);
//	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC) wglGetProcAddress("wglCreateContextAttribsARB");
//
//
//	if(!wglChoosePixelFormatARB)
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"wglChoosePixelFormatARB(...) nicht vorhanden", L"Extension Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	//int attributes[] = 
//	//{
//	//	WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
//	//	WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
//	//	WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
//	//	WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
//	//	WGL_COLOR_BITS_ARB, bitsPerPix,
//	//	WGL_DEPTH_BITS_ARB, 24,
//	//	WGL_STENCIL_BITS_ARB, 8,
//	//	WGL_SAMPLE_BUFFERS_ARB, 1,
//	//	WGL_SAMPLES_ARB, multisamples,
//	//	0
//	//};
//
//	int attributes[] =
//	{
//		WGL_DRAW_TO_WINDOW_ARB, gl::TRUE_,
//		WGL_SUPPORT_OPENGL_ARB, gl::TRUE_,
//		WGL_DOUBLE_BUFFER_ARB, gl::TRUE_,
//		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
//		WGL_COLOR_BITS_ARB, bitsPerPix,
//		WGL_DEPTH_BITS_ARB, 24,
//		WGL_STENCIL_BITS_ARB, 8,
//		WGL_SAMPLE_BUFFERS_ARB, 1,
//		WGL_SAMPLES_ARB, multisamples,
//		0
//	};
//
//	int proper_pixelformat;
//	UINT numFormats;
//
//	// Fenster kaputt machen und neu bauen
//	bool fs_old = window_fullscreen;
//	window_fullscreen = false;
//	closeWindow();
//	window_fullscreen = fs_old;
//
//
//	// Klasse registrieren
//	if(!RegisterClassEx(&winclass))
//	{
//		MessageBox(NULL, L"Fehler beim Registrieren der Fensterklasse", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!(window_handle = CreateWindowEx(dwExStyle, L"classOpenGL", title, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle, winrect.left, winrect.top, winrect.right, winrect.bottom, NULL, NULL, hinstance, NULL)))
//	{
//		DWORD err = GetLastError();
//
//		closeWindow();
//		MessageBox(NULL, L"Fehler beim Erzeugen des Fensters!", L"Fehler", ERR_MSG_MSK);
//		return false;
//	}
//
//
//	if(!(window_device_context = GetDC(window_handle)))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Fehler beim erzeugen des neuen Device Context", L"Device Context Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!wglChoosePixelFormatARB(window_device_context, attributes, NULL, 1, &proper_pixelformat, &numFormats))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"wglChoosePixelFormatARB(...) konnte kein passendes Pixelformat finden", L"Pixelformat Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//	if(!SetPixelFormat(window_device_context, proper_pixelformat, &pfd))
//	{
//		DWORD err = GetLastError();
//
//		closeWindow();
//
//		MessageBox(NULL, L"Setzen des eigentlichen Pixelformats nicht m�glich", L"Pixelformat Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//
//
//	// Context Creation
//
//	if(!wglCreateContextAttribsARB)
//	{
//		printf("Versuche Standard Context Creation\n");
//
//		if(!(window_render_content = wglCreateContext(window_device_context)))
//		{
//			closeWindow();
//
//			MessageBox(NULL, L"Der neue Rendering Content konnte nicht erzeugt werden", L"Fehler", ERR_MSG_MSK);
//
//			return false;
//		}
//	}
//
//	int contextmask, context;
//
//	switch(profile)
//	{
//	case WGL_CONTEXT_CORE_PROFILE_BIT_ARB:
//		contextmask = WGL_CONTEXT_PROFILE_MASK_ARB;
//		context = profile;
//		break;
//	case WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB:
//		contextmask = WGL_CONTEXT_PROFILE_MASK_ARB;
//		context = profile;
//		break;
//		// WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB fehlt weil identisch mit WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
//
//	default:
//		contextmask = WGL_CONTEXT_PROFILE_MASK_ARB;
//		context = WGL_CONTEXT_CORE_PROFILE_BIT_ARB;
//		break;
//	}
//
//	GLint contextAttribs[] =
//	{
//		WGL_CONTEXT_MAJOR_VERSION_ARB, majorVersion,
//		WGL_CONTEXT_MINOR_VERSION_ARB, minorVersion,
//		contextmask, context,
//		0
//	};
//
//	if(!(window_render_content = wglCreateContextAttribsARB(window_device_context, NULL, contextAttribs)))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Der neue Rendering Content konnte nicht erzeugt werden", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//
//	printf("OpenGL Context erzeugt!\n");
//
//
//	if(!wglMakeCurrent(window_device_context, window_render_content))
//	{
//		closeWindow();
//
//		MessageBox(NULL, L"Neuer Context und Content konnten nicht gebunden werden", L"Fehler", ERR_MSG_MSK);
//
//		return false;
//	}
//	//}
//
//
//	if(window_fullscreen)
//		resizeWindow(width, height);
//
//	return true;
//}
//
//
//
//
//
//bool displayWindow(bool maximized)
//{
//	if(!window_fullscreen && maximized)
//		ShowWindow(window_handle, SW_MAXIMIZE);
//	else
//		ShowWindow(window_handle, SW_SHOW);
//
//	SetForegroundWindow(window_handle);
//	SetFocus(window_handle);
//
//	return true;
//}
//
//
//
//
//
//void changeWindowTitle(LPCWSTR title)
//{
//	SetWindowText(window_handle, title);
//}
//
//
//
//
//
//void init()
//{
//	if(!gl::sys::LoadFunctions())
//	{
//		MessageBox(window_handle, L"Fehler beim Initialisieren der OpenGL-Funktionen", L"GL LOAD ERROR", MB_OK | MB_ICONERROR);
//		PostQuitMessage(-1);
//	}
//
//	selectScene();
//}
//
//
//
//
//
//void kill()
//{
//	// Fix invalid cuda context exception
//	scene.reset(nullptr);
//}
//
//
//
//
//
//void render()
//{
//	scene->render();
//	SwapBuffers(window_device_context);
//}
//
//
//
//
//
//void update()
//{
//	scene->update();
//}
//
//
//
//
//
//void resizeWindow(GLsizei width, GLsizei height)
//{
//	scene->resizeWindow(width, height);
//}
//
//
//
//
//
//void selectScene()
//{
//	switch(selection)
//	{
//	case 0:
//		scene.reset(new TestScene());
//		break;
//	case 1:
//		scene.reset(new SMScene());
//		break;
//	case 2:
//		scene.reset(new HeightShadowScene());
//		break;
//	case 3:
//		scene.reset(new CudaTextureScene());
//		break;
//	case 4:
//		scene.reset(new VolumeTracingScene());
//		break;
//	}
//
//	scene->init();
//
//	scene->resizeWindow(window_width, window_height);
//}
//
//
//
//
//
//int WINAPI  WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
//{
//	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//	//_crtBreakAlloc = 499;
//
//	console();
//
//	openWindow(windowTitle, 250, 150, 1280, 720, 32, false, 4, 4, 3, CORE_PROFILE);
//	//openWindow(windowTitle, 250, 150, 1280, 720, 32, false, 4, 4, 3, COMPATIBILITY_PROFILE);
//
//	init();
//
//	displayWindow(false);
//
//
//	MSG msg;
//
//	bool done = false;
//
//	while(!done)
//	{
//		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
//		{
//			if(msg.message == WM_QUIT)
//				done = true;
//
//			else
//			{
//				TranslateMessage(&msg);
//				DispatchMessage(&msg);
//			}
//		}
//		else
//		{
//			update();
//			render();
//		}
//	}
//
//
//	kill();
//
//	closeWindow();
//
//	//MessageBox(NULL, L"Hello Universe!", L"HELLO UNIVERSE!", MB_OK | MB_ICONHAND);
//	return 0;
//}
//// =================================================================================================================================================================================================	#####


#include <iostream>
#include <string>
#include <memory>

#include <iostream>
#include <string>
#include <memory>

#include <Renderer.h>

//#ifdef WIN32
//// Resolve conflict with cuda_gl_interop.h  --  line 448 :: DECLARE_HANDLE(HGPUNV);  commented out
//#include <wglext.h>
//#elif 
//#include <glext.h>
//#include <glxext.h>
//#endif


#include <GLFW/glfw3.h>



void resize(GLFWwindow *window, int width, int height);
void destroy(GLFWwindow *window);
void input(GLFWwindow *window, int key, int scancode, int action, int modifiers);
void errorHandler(int error, const char* description);

void update();
void render();



std::unique_ptr<computergraphics::Renderer> renderer;


int main()
{
	if(!glfwInit())
	{
		std::cout << "GLFW failed" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow *window = glfwCreateWindow(1280, 720, "GLFW Shadow Algorithm", nullptr, nullptr);
	//GLFWwindow *window = glfwCreateWindow(1024, 1024, "GLFW Shadow Algorithm", nullptr, nullptr);

	if(!window)
	{
		std::cout << "Failed to open window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	
	renderer = std::make_unique<computergraphics::Renderer>(window);
	if(!renderer->setup())
		return -1;


	glfwSetWindowCloseCallback(window, destroy);
	glfwSetErrorCallback(errorHandler);
	glfwSetKeyCallback(window, input);
	glfwSetFramebufferSizeCallback(window, resize);

	glfwSetInputMode(window, GLFW_STICKY_KEYS, gl::TRUE_);

	glfwSwapInterval(0);

	
	do
	{
		render();
		update();

		glfwSwapBuffers(window);
		glfwPollEvents();

	} while(!glfwWindowShouldClose(window));

	renderer.release();

	glfwDestroyWindow(window);
	window = nullptr;

	glfwTerminate();
}


void resize(GLFWwindow *window, int width, int height)
{
	renderer->resize(width, height);
}


void destroy(GLFWwindow *window)
{
	renderer->destroy();
}


void input(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{
	renderer->input(key, scancode, action, modifiers);
}


void errorHandler(int error, const char* description)
{
	std::string msg(description);
	std::cout << msg << std::endl;
}


void render()
{
	renderer->render();
}


void update()
{
	renderer->update();
}


//bool init()
//{
//	if(!gl::sys::LoadFunctions())
//		return false;
//
//	selectScene();
//}


//void selectScene()
//{
//	switch(selection)
//	{
//	case 0:
//		scene.reset(new TestScene());
//		break;
//	case 1:
//		scene.reset(new SMScene());
//		break;
//	case 2:
//		scene.reset(new HeightShadowScene());
//		break;
//	case 3:
//		scene.reset(new CudaTextureScene());
//		break;
//	case 4:
//		scene.reset(new VolumeTracingScene());
//		break;
//	}
//
//	scene->init();
//
//	scene->resizeWindow(window_width, window_height);
//}