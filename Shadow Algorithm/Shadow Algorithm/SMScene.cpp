// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <SMScene.h>

#include <vector>
#include <iostream>

#include <FBXModel.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using std::cout;
using std::endl;
using std::vector;

using mgraphics::fbxmodel::FBXModel;
using mgraphics::fbxmodel::Mesh;

using mgraphics::textures::WriteImageFile;
// =================================================================================================================================================================================================	#####




// Methoden																																															 Methoden
// =================================================================================================================================================================================================	#####
SMScene::SMScene(void)
{
	ground = std::make_unique<RenderObject>();
	cube = std::make_unique<RenderObject>();
	last = std::chrono::high_resolution_clock::now();
	cout << "Shadow Mapping Scene created" << endl;
}




SMScene::~SMScene(void)
{
	gl::DeleteVertexArrays(ground->meshCount, ground->meshVAOs.get());
	gl::DeleteBuffers(ground->meshCount, ground->meshVBOs.get());


	gl::DeleteVertexArrays(cube->meshCount, cube->meshVAOs.get());
	gl::DeleteBuffers(cube->meshCount, cube->meshVBOs.get());


	gl::DeleteFramebuffers(1, &framebuffer);
	gl::DeleteTextures(1, &fbotexture);
	gl::DeleteSamplers(1, &fbosampler);


	cout << "Shadow Mapping Scene deleted" << endl;
}




void SMScene::init()
{
	gl::ClearColor(.2353f, .2353f, .2353f, 1.f);
	gl::Enable(gl::DEPTH_TEST);
	gl::PolygonOffset(1.f, 4096.f);

	initShader();
	initGeometry();

	cubeY = 0.f;


	// Light
	lightpos.X(5.f); lightpos.Y(7.5f); lightpos.Z(0.f);
	ambientcolor.X(.3f); ambientcolor.Y(.3f); ambientcolor.Z(.3f); ambientcolor.W(1.f);
	diffusecolor.X(1.f); diffusecolor.Y(1.f); diffusecolor.Z(1.f); diffusecolor.W(1.f);


	// Geometrie Farben
	groundcolor.X(.522f); groundcolor.Y(.596f); groundcolor.Z(.663f); groundcolor.W(1.f);
	cubecolor.X(.0f); cubecolor.Y(.6f); cubecolor.Z(.8f); cubecolor.W(1.f);

	sceneRotation = 0.f;


	// Shadow Framebuffer
	shadowmapWidth = 2048;
	shadowmapHeight = 2048;
	imagedata.reset(new unsigned char[shadowmapWidth * shadowmapHeight * 4]);

	gl::GenFramebuffers(1, &framebuffer);
	gl::BindFramebuffer(gl::FRAMEBUFFER, framebuffer);

	gl::GenTextures(1, &fbotexture);
	gl::BindTexture(gl::TEXTURE_2D, fbotexture);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::DEPTH_COMPONENT, shadowmapWidth, shadowmapHeight, 0, gl::DEPTH_COMPONENT, gl::FLOAT, nullptr);

	gl::GenSamplers(1, &fbosampler);
	//gl::SamplerParameteri(fbosampler, gl::TEXTURE_MIN_FILTER, gl::NEAREST);
	//gl::SamplerParameteri(fbosampler, gl::TEXTURE_MAG_FILTER, gl::NEAREST);

	gl::SamplerParameteri(fbosampler, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::SamplerParameteri(fbosampler, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::SamplerParameteri(fbosampler, gl::TEXTURE_COMPARE_MODE, gl::COMPARE_REF_TO_TEXTURE);

	gl::SamplerParameteri(fbosampler, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::SamplerParameteri(fbosampler, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	gl::FramebufferTexture(gl::FRAMEBUFFER, gl::DEPTH_ATTACHMENT, fbotexture, 0);
	gl::DrawBuffer(gl::NONE);

	if(gl::CheckFramebufferStatus(gl::FRAMEBUFFER) != gl::FRAMEBUFFER_COMPLETE)
		MessageBox(nullptr, L"Fehler beim Erzeugen des FBO", L"FBO ERROR", MB_OK);

	gl::BindFramebuffer(gl::FRAMEBUFFER, 0);


	// Shadow Matrizen
	shadowprojection = Matrix4::Orthographic(-10.f, 10.f, -10.f, 10.f, -10.f, 30.f);
	shadowview = Matrix4::Rotate(45.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(-40.f * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(-lightpos.X(), -lightpos.Y(), lightpos.Z() - 5.f);
	shadowprojectionview = shadowprojection * shadowview;
	texturebias = Matrix4(.5f, 0.f, 0.f, .5f, 0.f, .5f, 0.f, .5f, 0.f, 0.f, .5f, .5f, 0.f, 0.f, 0.f, 1.f);

	shadow = false;

	framectr = std::make_unique<statistics::FrameCounter>();
}




void SMScene::render()
{	
	// Shadow Map
	gl::Enable(gl::POLYGON_OFFSET_FILL);

	gl::BindFramebuffer(gl::FRAMEBUFFER, framebuffer);

	gl::Viewport(0, 0, shadowmapWidth, shadowmapHeight);

	gl::Clear(gl::DEPTH_BUFFER_BIT);


	program->use();

	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &loc_shadowvertex);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &loc_shadowfragment);

	model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(sceneRotation * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, -1.f, 0.f);
	shadowprojectionviewmodel = shadowprojectionview * model;

	gl::UniformMatrix4fv(loc_mat_mvp, 1, gl::FALSE_, shadowprojectionviewmodel.Data());

	for(int i = 0; i < ground->meshCount; ++i)
	{
		gl::BindVertexArray(ground->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, ground->verts[i]);
	}

	model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(sceneRotation * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, cubeY, 0.f);
	shadowprojectionviewmodel = shadowprojectionview * model;

	gl::UniformMatrix4fv(loc_mat_mvp, 1, gl::FALSE_, shadowprojectionviewmodel.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}

	// Uncomment for ugly screenshot of shadow map
	//gl::ReadPixels(0, 0, shadowmapWidth, shadowmapHeight, gl::DEPTH_COMPONENT, gl::UNSIGNED_BYTE, imagedata.get());
	//WriteImageFile("../textures/SMScene/shadowmap.png", shadowmapWidth, shadowmapHeight, 32, imagedata.get());





	// Scene
	gl::Disable(gl::POLYGON_OFFSET_FILL);

	gl::BindFramebuffer(gl::FRAMEBUFFER, 0);

	gl::Viewport(0, 0, windowWidth, windowHeight);

	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);


	//view = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(sceneRotation * (float) M_PI / 180.f, Vector3::UnitY);
	//view = Matrix4::Rotate(45.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(-40.f * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(-lightpos.X(), -lightpos.Y(), lightpos.Z() - 5.f);
	//projection = shadowprojection;


	model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(sceneRotation * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, -1.f, 0.f);

	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));
	shadowprojectionviewmodel = texturebias * shadowprojectionview * model;

	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &loc_vertexprogram);

	if(shadow)
		gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &loc_fragmentprogram);

	else
		gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &loc_fragmentprogramNoShadow);

	gl::UniformMatrix4fv(loc_mat_mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(loc_mat_mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix4fv(loc_mat_shadow, 1, gl::FALSE_, shadowprojectionviewmodel.Data());
	gl::UniformMatrix3fv(loc_mat_normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform3fv(loc_light_position, 1, lightpos.Data());
	gl::Uniform4fv(loc_light_ambient, 1, ambientcolor.Data());
	gl::Uniform4fv(loc_light_diffuse, 1, diffusecolor.Data());

	gl::Uniform4fv(loc_surfacecolor, 1, groundcolor.Data());

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindSampler(0, fbosampler);
	gl::BindTexture(gl::TEXTURE_2D, fbotexture);
	gl::Uniform1i(loc_textures_shadowmap, 0);


	for(int i = 0; i < ground->meshCount; ++i)
	{
		gl::BindVertexArray(ground->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, ground->verts[i]);
	}


	model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(sceneRotation * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, cubeY, 0.f);
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));
	shadowprojectionviewmodel = texturebias * shadowprojectionview * model;

	gl::UniformMatrix4fv(loc_mat_mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(loc_mat_mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix4fv(loc_mat_shadow, 1, gl::FALSE_, shadowprojectionviewmodel.Data());
	gl::UniformMatrix3fv(loc_mat_normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(loc_surfacecolor, 1, cubecolor.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}

	//gl::ReadPixels(0, 0, windowWidth, windowHeight, gl::BGRA, gl::UNSIGNED_BYTE, imagedata.get());
	//WriteImageFile("../textures/SMScene/screen.png", windowWidth, windowHeight, 32, imagedata.get());
}




void SMScene::update(GLFWwindow *window)
{
	auto now = std::chrono::high_resolution_clock::now();
	long long delta = std::chrono::duration_cast<std::chrono::milliseconds>(now - last).count();

	sceneRotation += static_cast<float>(delta) * .05f;

	last = now;

	framectr->tick(window);
}




void SMScene::resizeWindow(GLsizei width, GLsizei height)
{
	windowWidth = width;
	windowHeight = height;

	gl::Viewport(0, 0, width, height);

	float aspect = (float) width / (float) height;

	projection = Matrix4::Perspective(60.f * (float) M_PI / 180.f, aspect, .5f, 50.f);
}




//void SMScene::dispatchInput(WPARAM key)
//{
//	if(key == VK_UP)
//		cubeY += .5f;
//
//	if(key == VK_DOWN)
//		cubeY -= .5f;
//
//	if(key == VK_PRIOR)
//		shadow = true;
//
//	if(key == VK_NEXT)
//		shadow = false;
//}




void SMScene::dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{
	if(key == GLFW_KEY_UP && action == GLFW_RELEASE)
		cubeY += .5f;

	if(key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
		cubeY -= .5f;

	if(key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE)
		shadow = true;

	if(key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE)
		shadow = false;
}




void SMScene::initShader()
{
	program.reset(new GLSLShaderprogram("../shader/SMScene/VertexShader.glsl", "../shader/SMScene/FragmentShader.glsl"));

	loc_mat_mvp = program->getUniformLocation("matrices.mvp");
	loc_mat_mv = program->getUniformLocation("matrices.mv");
	loc_mat_normal = program->getUniformLocation("matrices.normal");

	loc_light_position = program->getUniformLocation("light.position");
	loc_light_ambient = program->getUniformLocation("light.ambient");
	loc_light_diffuse = program->getUniformLocation("light.diffuse");

	loc_surfacecolor = program->getUniformLocation("surfacecolor");

	loc_textures_shadowmap = program->getUniformLocation("textures.shadowmap");
	loc_mat_shadow = program->getUniformLocation("matrices.shadow");

	loc_vertexprogram = program->getSubroutineIndex(gl::VERTEX_SHADER, "colored");
	loc_fragmentprogram = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "colored");
	loc_fragmentprogramNoShadow = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "noShadow");
	loc_shadowvertex = program->getSubroutineIndex(gl::VERTEX_SHADER, "shadowmap");
	loc_shadowfragment = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "shadowmap");
}




void SMScene::initGeometry()
{
	FBXModel *groundmodel = new FBXModel("../models/SMScene/Ground.fbx");

	ground->meshCount = groundmodel->MeshCount();

	ground->meshVAOs.reset(new unsigned int[ground->meshCount]);
	gl::GenVertexArrays(ground->meshCount, ground->meshVAOs.get());

	ground->meshVBOs.reset(new unsigned int[ground->meshCount]);
	gl::GenBuffers(ground->meshCount, ground->meshVBOs.get());

	ground->verts.reset(new unsigned int[ground->meshCount]);

	unsigned int current = 0;
	vector<Mesh> meshes = groundmodel->Meshes();
	for(Mesh &mesh : meshes)
	{
		ground->verts[current] = mesh.VertexCount();

		gl::BindVertexArray(ground->meshVAOs[current]);

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindBuffer(gl::ARRAY_BUFFER, ground->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(1);
		gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		gl::EnableVertexAttribArray(2);
		gl::VertexAttribPointer(2, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbuffersize + normalbuffersize));
	}

	delete groundmodel;



	FBXModel *cubemodel = new FBXModel("../models/SMScene/Cube.fbx");

	cube->meshCount = cubemodel->MeshCount();

	cube->meshVAOs.reset(new unsigned int[cube->meshCount]);
	gl::GenVertexArrays(cube->meshCount, cube->meshVAOs.get());

	cube->meshVBOs.reset(new unsigned int[cube->meshCount]);
	gl::GenBuffers(cube->meshCount, cube->meshVBOs.get());

	cube->verts.reset(new unsigned int[cube->meshCount]);

	current = 0;
	meshes = cubemodel->Meshes();
	for(Mesh &mesh : meshes)
	{
		cube->verts[current] = mesh.VertexCount();

		gl::BindVertexArray(cube->meshVAOs[current]);

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);

		gl::BindBuffer(gl::ARRAY_BUFFER, cube->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(1);
		gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);
	}



	gl::BindVertexArray(0);
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);

	delete cubemodel;
}
// =================================================================================================================================================================================================	#####
