#include <VolumeTracingScene.cuh>
#include <device_launch_parameters.h>

#include <CudaError.h>

#include <vector>


namespace gpu
{
	namespace kernels
	{
		__global__ void clear(cudaSurfaceObject_t volumeSurface, dim3 textureDimension)
		{
			unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
			unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
			unsigned int z = blockIdx.z * blockDim.z + threadIdx.z;

			uchar4 data = make_uchar4(0x00, 0x00, 0x00, 0x00);
			surf3Dwrite(data, volumeSurface, x * sizeof(uchar4), y, z);
		}


		__global__ void fillVolumeWithColor(cudaSurfaceObject_t volumeSurface, dim3 textureDimension)
		{
			unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
			unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
			unsigned int z = blockIdx.z * blockDim.z + threadIdx.z;

			if(x < textureDimension.x && y < textureDimension.y && z < textureDimension.z)
			{
				uchar4 data;

				switch(z)
				{
				case 0:
					data = make_uchar4(0xFF, 0x00, 0x00, 0xff);
					break;
				case 1:
					data = make_uchar4(0x00, 0xFF, 0x00, 0xff);
					break;
				case 2:
					data = make_uchar4(0x00, 0x00, 0xFF, 0xff);
					break;
				case 3:
					data = make_uchar4(0xFF, 0xFF, 0x00, 0xff);
					break;
				case 4:
					data = make_uchar4(0x00, 0xFF, 0xFF, 0xff);
					break;
				case 5:
					data = make_uchar4(0xFF, 0x00, 0xFF, 0xff);
					break;
				case 6:
					data = make_uchar4(0x00, 0x99, 0xCC, 0xff);
					break;
				case 7:
					data = make_uchar4(0xC0, 0xC0, 0xC0, 0xff);
					break;
				case 8:
					data = make_uchar4(0x3C, 0x3C, 0x3C, 0xff);
					break;
				case 9:
					data = make_uchar4(0x40, 0x80, 0x70, 0xff);
					break;
				default:
					data = make_uchar4(0xff, 0xff, 0xff, 0xff);
					break;
				}

				surf3Dwrite(data, volumeSurface, x * sizeof(uchar4), y, z);
			}
		}


		__global__ void fillVolumeWithVector(cudaSurfaceObject_t volumeSurface, dim3 textureDimension, unsigned char *v)
		{
			unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
			unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
			unsigned int z = blockIdx.z * blockDim.z + threadIdx.z;

			if(x < textureDimension.x && y < textureDimension.y && z < textureDimension.z)
			{
				uchar4 data = make_uchar4(v[0], v[1], v[2], v[3]);
				surf3Dwrite(data, volumeSurface, x * sizeof(uchar4), y, z);
			}
		}




		struct Point
		{
			float x;
			float y;
			float z;
		};


		__device__ bool contains(Point *point, float *bounding)
		{
			Point lower;
			lower.x = bounding[0];
			lower.y = bounding[1];
			lower.z = bounding[2];

			Point upper;
			upper.x = bounding[3];
			upper.y = bounding[4];
			upper.z = bounding[5];

			if(point->x < lower.x || point->x >= upper.x)
				return false;

			if(point->y < lower.y || point->y >= upper.y)
				return false;

			if(point->z < lower.z || point->z >= upper.z)
				return false;

			return true;
		}


		__global__ void fillVolumeWithBoundingVolume(cudaSurfaceObject_t volumeSurface, dim3 textureDimension, float *bounding)
		{
			unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
			unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
			unsigned int z = blockIdx.z * blockDim.z + threadIdx.z;

			if(x < textureDimension.x && y < textureDimension.y && z < textureDimension.z)
			{
				uchar4 data;
				
				Point p;
				p.x = static_cast<float>(x);
				p.y = static_cast<float>(y);
				p.z = static_cast<float>(z);

				if(contains(&p, bounding))
				{
					data = make_uchar4(0xff, 0xff, 0xff, 0xff);
				}
				else
				{
					data = make_uchar4(0x00, 0x00, 0x00, 0xff);
				}

				surf3Dwrite(data, volumeSurface, x * sizeof(uchar4), y, z);
			}
		}
	}


	namespace functions
	{
		void fillVolume(cudaSurfaceObject_t volumeSurface, dim3 textureDimension)
		{
			dim3 threads(10, 10, 10);
			dim3 blocks(static_cast<unsigned int>(std::ceil(static_cast<double>(textureDimension.x) / static_cast<double>(threads.x))), static_cast<unsigned int>(std::ceil(static_cast<double>(textureDimension.y) / static_cast<double>(threads.y))), static_cast<unsigned int>(std::ceil(static_cast<double>(textureDimension.z) / static_cast<double>(threads.z))));

			//kernels::fillVolumeWithColor<<< blocks, threads >>>(volumeSurface, textureDimension);


			//std::vector<unsigned char> vec;
			//vec.push_back(164); vec.push_back(199); vec.push_back(57); vec.push_back(0xff);

			//unsigned char *cudaVec = nullptr;
			//error::cuda::check(cudaMalloc(&cudaVec, vec.size() * sizeof(unsigned char)));
			//error::cuda::check(cudaMemcpy(cudaVec, vec.data(), vec.size() * sizeof(unsigned char), cudaMemcpyHostToDevice));

			//kernels::fillVolumeWithVector<<< blocks, threads >>>(volumeSurface, textureDimension, cudaVec);

			//error::cuda::check(cudaFree(cudaVec));



			std::vector<float> cube;

			// Volumen mit Offsetanpassung
			cube.push_back(4.5f); cube.push_back(0.f); cube.push_back(4.5f);
			cube.push_back(5.5f); cube.push_back(1.f); cube.push_back(5.5f);

			float *cudaCube;
			error::cuda::check(cudaMalloc(&cudaCube, cube.size() * sizeof(float)));
			error::cuda::check(cudaMemcpy(cudaCube, cube.data(), cube.size() * sizeof(float), cudaMemcpyHostToDevice));

			kernels::fillVolumeWithBoundingVolume<<< blocks, threads >>>(volumeSurface, textureDimension, cudaCube);

			error::cuda::check(cudaFree(cudaCube));



			//kernels::clear<<< blocks, threads >>>(volumeSurface, textureDimension);
		}
	}
}
