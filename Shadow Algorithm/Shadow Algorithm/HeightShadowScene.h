#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <Scene.h>

#include <GLSLShader.h>
#include <DataTypes.h>
#include <Camera.h>

#include <FrameCounter.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using mgraphics::shader::GLSLShaderprogram;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Matrix4;
// =================================================================================================================================================================================================	#####




// Klassen																																															  Klassen
// =================================================================================================================================================================================================	#####
class HeightShadowScene :
	public Scene
{
	struct AttributeLocations
	{
		unsigned int vertex;
		unsigned int normal;
		unsigned int uv;
	};

	struct UniformLocations
	{
		unsigned int mvp;
		unsigned int mv;
		unsigned int m;
		unsigned int normal;

		unsigned int surfacecolor;

		unsigned int lightpos;
		unsigned int lightambient;
		unsigned int lightdiffuse;
		unsigned int lightspecular;
		unsigned int lightshininess;

		unsigned int heightmap;

		unsigned int renderFragment;
		unsigned int renderVertex;
		
		unsigned int heightmapFragment;
		unsigned int heightmapVertex;
	};

	struct Lighting
	{
		Vector3 position;
		Vector4 ambient, diffuse, specular;
		float shininess;
	};

	struct ImageDimension
	{
		unsigned int width;
		unsigned int height;
	};

	std::unique_ptr<RenderObject> ground;

	std::unique_ptr<RenderObject> cube;

	float cubeY;

	Vector4 groundcolor, cubecolor;

	std::unique_ptr<GLSLShaderprogram> program;

	AttributeLocations attribs;
	UniformLocations uniforms;

	Lighting light;

	Matrix4 projection, view, model;
	Matrix4 projectionviewmodel, viewmodel;
	Matrix3 normal;

	Matrix4 heightprojection;

	std::unique_ptr<mgraphics::Camera> observerCamera;
	std::unique_ptr<mgraphics::Camera> heightmapCamera;

	ImageDimension windowSize;

	unsigned int framebuffer;
	unsigned int heightmap;
	unsigned int fbodepthtexture;
	unsigned int sampler;
	ImageDimension heightmapSize;

	std::unique_ptr<statistics::FrameCounter> framectr;



	void initGeometry();
	void initShader();
	void initFramebuffer();

	void renderHeightmap();
	void renderScene();

public:
	HeightShadowScene(void);
	virtual ~HeightShadowScene(void);

	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};
// =================================================================================================================================================================================================	#####
