#include <POMShadowScene.h>

#include <iostream>
#include <FBXModel.h>




using mgraphics::fbxmodel::FBXModel;
using mgraphics::fbxmodel::Mesh;
using mgraphics::shader::GLSLShaderprogram;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Matrix4;
using mgraphics::Camera;



POMShadowScene::POMShadowScene()
{
	ground = std::make_unique<RenderObject>();
	cube = std::make_unique<RenderObject>();

	std::cout << "POM Shadow Scene created" << std::endl;
}


POMShadowScene::~POMShadowScene()
{
	gl::DeleteVertexArrays(ground->meshCount, ground->meshVAOs.get());
	gl::DeleteVertexArrays(cube->meshCount, cube->meshVAOs.get());

	gl::DeleteBuffers(ground->meshCount, ground->meshVBOs.get());
	gl::DeleteBuffers(cube->meshCount, cube->meshVBOs.get());

	gl::DeleteFramebuffers(1, &framebuffer);
	gl::DeleteTextures(1, &heightmap);
	gl::DeleteTextures(1, &fbodepthtexture);
	gl::DeleteSamplers(1, &sampler);

	gl::Disable(gl::DEPTH_TEST);
	gl::Disable(gl::CULL_FACE);

	std::cout << "POM Shadow Scene deleted" << std::endl;
}


void POMShadowScene::init()
{
	gl::ClearColor(.2353f, .2353f, .2353f, 1.f);
	gl::Enable(gl::DEPTH_TEST);
	gl::Enable(gl::CULL_FACE);

	initShader();
	initGeometry();
	initFramebuffer();


	//heightprojection = Matrix4::Orthographic(10.0f, 10.0f, 0.f, 10.1f);
	heightprojection = Matrix4::Orthographic(10.0f, 10.0f, 0.f, 10.0f);


	cubecolor = Vector4(.75f, .69f, .69f, 1.f);
	groundcolor = Vector4(.3f, .5f, 1.f, 1.f);


	cubeY = .5f;


	light.position = Vector3(std::cos(lightradius) * 20.0f, 20.0f, std::sin(lightradius) * 20.0f);
	light.ambient = Vector4(.3f, .3f, .3f, 1.f);
	light.diffuse = Vector4(1.f, 1.f, 1.f, 1.f);
	light.specular = Vector4(1.f, 1.f, 1.f, 1.f);
	light.shininess = 12.f;


	observerCamera = std::make_unique<Camera>(Vector3(0.f, 6.f, 10.f), Vector3(0.f, 0.f, 0.f));
	heightmapCamera = std::make_unique<Camera>(Vector3(0.f, 9.9999f, 0.f), Vector3(0.f, 0.f, 0.f));
	//heightmapCamera->setUpOrientation(static_cast<float>(M_PI)); // An Geometrie-Orientierung anpassen.

	framectr = std::make_unique<statistics::FrameCounter>();
}


void POMShadowScene::render()
{
	renderHeightmap();

	renderScene();
}


void POMShadowScene::update(GLFWwindow *window)
{
	framectr->tick(window);
}


void POMShadowScene::resizeWindow(GLsizei width, GLsizei height)
{
	windowSize.width = width;
	windowSize.height = height;

	gl::Viewport(0, 0, width, height);

	float aspect = static_cast<float>(width) / static_cast<float>(height);
	projection = Matrix4::Perspective(60.f * static_cast<float>(M_PI) / 180.f, aspect, .5f, 50.f);
}


//void POMShadowScene::dispatchInput(WPARAM key)
//{
//	if(key == VK_UP)
//		cubeY += .5f;
//
//	if(key == VK_DOWN)
//		cubeY -= .5f;
//
//	mgraphics::logic::clamp<float>(cubeY, -.5f, 3.f);
//}


void POMShadowScene::dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{
	if(key == GLFW_KEY_UP && action == GLFW_RELEASE)
		cubeY += .5f;

	if(key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
		cubeY -= .5f;

	if(key == GLFW_KEY_A && (action == GLFW_REPEAT || action == GLFW_PRESS))
	{
		lightradius -= static_cast<float>(M_PI) / 20.f;
		light.position.X(std::cos(lightradius) * 20.f);
		light.position.Z(std::sin(lightradius) * 20.f);

		std::cout << "Light Pos: " << light.position.X() << " / " << light.position.Y() << " / " << light.position.Z() << std::endl;
	}
	if(key == GLFW_KEY_D && (action == GLFW_REPEAT || action == GLFW_PRESS))
	{
		lightradius += static_cast<float>(M_PI) / 20.f;
		light.position.X(std::cos(lightradius) * 20.f);
		light.position.Z(std::sin(lightradius) * 20.f);

		std::cout << "Light Pos: " << light.position.X() << " / " << light.position.Y() << " / " << light.position.Z() << std::endl;
	}

	mgraphics::logic::clamp<float>(cubeY, -.5f, 8.f);
}


void POMShadowScene::initGeometry()
{
	std::unique_ptr<FBXModel> model = std::make_unique<FBXModel>("../models/SMScene/Ground.fbx");

	ground->meshCount = model->MeshCount();

	ground->meshVAOs.reset(new unsigned int[ground->meshCount]);
	ground->meshVBOs.reset(new unsigned int[ground->meshCount]);
	ground->verts.reset(new unsigned int[ground->meshCount]);


	gl::GenVertexArrays(ground->meshCount, ground->meshVAOs.get());

	gl::GenBuffers(ground->meshCount, ground->meshVBOs.get());

	unsigned int current = 0;
	std::vector<Mesh> groundmeshes = model->Meshes();
	for(Mesh &mesh : groundmeshes)
	{
		ground->verts[current] = mesh.VertexCount();

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindVertexArray(ground->meshVAOs[current]);

		gl::BindBuffer(gl::ARRAY_BUFFER, ground->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(attribs.vertex);
		gl::VertexAttribPointer(attribs.vertex, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(attribs.normal);
		gl::VertexAttribPointer(attribs.normal, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		gl::EnableVertexAttribArray(attribs.uv);
		gl::VertexAttribPointer(attribs.uv, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbuffersize + normalbuffersize));
	}


	model.reset();
	model = std::make_unique<FBXModel>("../models/SMScene/Cube.fbx");

	cube->meshCount = model->MeshCount();

	cube->meshVAOs.reset(new unsigned int[cube->meshCount]);
	cube->meshVBOs.reset(new unsigned int[cube->meshCount]);
	cube->verts.reset(new unsigned int[cube->meshCount]);


	gl::GenVertexArrays(cube->meshCount, cube->meshVAOs.get());
	gl::GenBuffers(cube->meshCount, cube->meshVBOs.get());


	current = 0;
	std::vector<Mesh> cubemeshes = model->Meshes();
	for(Mesh &mesh : cubemeshes)
	{
		cube->verts[current] = mesh.VertexCount();

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindVertexArray(cube->meshVAOs[current]);

		gl::BindBuffer(gl::ARRAY_BUFFER, cube->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(attribs.vertex);
		gl::VertexAttribPointer(attribs.vertex, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(attribs.normal);
		gl::VertexAttribPointer(attribs.normal, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		gl::EnableVertexAttribArray(attribs.uv);
		gl::VertexAttribPointer(attribs.uv, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbuffersize + normalbuffersize));
	}

	model.reset();
	gl::BindVertexArray(0);
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);
}


void POMShadowScene::initShader()
{
	program = std::make_unique<GLSLShaderprogram>("../shader/POMShadowScene/VertexShader.glsl", "../shader/POMShadowScene/FragmentShader.glsl");

	uniforms.mvp = program->getUniformLocation("matrices.mvp");
	uniforms.mv = program->getUniformLocation("matrices.mv");
	uniforms.m = program->getUniformLocation("matrices.m");
	uniforms.normal = program->getUniformLocation("matrices.normal");

	uniforms.surfacecolor = program->getUniformLocation("surfacecolor");

	uniforms.lightpos = program->getUniformLocation("light.position");
	uniforms.lightambient = program->getUniformLocation("light.ambient");
	uniforms.lightdiffuse = program->getUniformLocation("light.diffuse");
	uniforms.lightspecular = program->getUniformLocation("light.specular");
	uniforms.lightshininess = program->getUniformLocation("light.shininess");

	uniforms.heightmap = program->getUniformLocation("textures.heightmap");

	uniforms.renderVertex = program->getSubroutineIndex(gl::VERTEX_SHADER, "renderpass");
	uniforms.renderFragment = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "renderpass");
	uniforms.heightmapVertex = program->getSubroutineIndex(gl::VERTEX_SHADER, "heightmap");
	uniforms.heightmapFragment = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "heightmap");
	uniforms.accesspassVertex = program->getSubroutineIndex(gl::VERTEX_SHADER, "accesspass");
	uniforms.accesspassFragment = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "accesspass");

	attribs.vertex = program->getAttribLocation("vertex");
	attribs.normal = program->getAttribLocation("normal");
	attribs.uv = program->getAttribLocation("uv");


	texHightmap = std::make_unique<mgraphics::textures::Texture2D>();
	//texHightmap->Generate("../textures/POMShadowScene/heightmap.png");
	//texHightmap->Generate("../textures/POMShadowScene/cobblestonesDepth.png");
	//texHightmap->Generate("../textures/heightmapHighContrast.png");
}


void POMShadowScene::renderHeightmap()
{
	gl::BindFramebuffer(gl::FRAMEBUFFER, framebuffer);

	gl::Viewport(0, 0, heightmapSize.width, heightmapSize.height);

	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);


	program->use();

	view = heightmapCamera->getViewMatrix();

	model = Matrix4::Translate(2.f, cubeY, -2.f);
	viewmodel = view * model;
	projectionviewmodel = heightprojection * viewmodel;


	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.heightmapVertex);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.heightmapFragment);


	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}


	model = Matrix4::Translate(-2.f, cubeY, 3.f);
	viewmodel = view * model;
	projectionviewmodel = heightprojection * viewmodel;

	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}



	model = Matrix4::Identity();
	viewmodel = view * model;
	projectionviewmodel = heightprojection * viewmodel;


	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());

	for(int i = 0; i < ground->meshCount; ++i)
	{
		gl::BindVertexArray(ground->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, ground->verts[i]);
	}
}


void POMShadowScene::renderScene()
{
	gl::BindFramebuffer(gl::FRAMEBUFFER, 0);

	gl::Viewport(0, 0, windowSize.width, windowSize.height);

	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);


	view = observerCamera->getViewMatrix();
	//view = heightmapCamera->getViewMatrix();


	program->use();

	// 1st Cube
	//model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(0.f * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, cubeY, 0.f);;
	model = Matrix4::Translate(2.f, cubeY, -2.f);
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	//projectionviewmodel = heightprojection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));


	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.renderVertex);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.renderFragment);
	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.heightmapVertex);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.heightmapFragment);
	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.accesspassVertex);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.accesspassFragment);

	gl::Uniform3fv(uniforms.lightpos, 1, light.position.Data());
	gl::Uniform4fv(uniforms.lightambient, 1, light.ambient.Data());
	gl::Uniform4fv(uniforms.lightdiffuse, 1, light.diffuse.Data());
	gl::Uniform4fv(uniforms.lightspecular, 1, light.specular.Data());
	gl::Uniform1f(uniforms.lightshininess, light.shininess);

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindSampler(0, sampler);
	gl::BindTexture(gl::TEXTURE_2D, heightmap);
	//gl::BindTexture(gl::TEXTURE_2D, texHightmap->getTextureID());
	gl::Uniform1i(uniforms.heightmap, 0);



	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(uniforms.mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix4fv(uniforms.m, 1, gl::FALSE_, model.Data());
	gl::UniformMatrix3fv(uniforms.normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(uniforms.surfacecolor, 1, cubecolor.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}



	// 2nd Cube
	model = Matrix4::Translate(-2.f, cubeY, 3.f);
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	//projectionviewmodel = heightprojection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));


	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.renderVertex);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.renderFragment);
	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.heightmapVertex);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.heightmapFragment);
	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.accesspassVertex);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.accesspassFragment);

	gl::Uniform3fv(uniforms.lightpos, 1, light.position.Data());
	gl::Uniform4fv(uniforms.lightambient, 1, light.ambient.Data());
	gl::Uniform4fv(uniforms.lightdiffuse, 1, light.diffuse.Data());
	gl::Uniform4fv(uniforms.lightspecular, 1, light.specular.Data());
	gl::Uniform1f(uniforms.lightshininess, light.shininess);

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindSampler(0, sampler);
	gl::BindTexture(gl::TEXTURE_2D, heightmap);
	//gl::BindTexture(gl::TEXTURE_2D, texHightmap->getTextureID());
	gl::Uniform1i(uniforms.heightmap, 0);



	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(uniforms.mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix4fv(uniforms.m, 1, gl::FALSE_, model.Data());
	gl::UniformMatrix3fv(uniforms.normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(uniforms.surfacecolor, 1, cubecolor.Data());

	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}





	//model = Matrix4::Translate(0.f, 0.f, -5.f) * Matrix4::Rotate(20.f * (float) M_PI / 180.f, Vector3::UnitX) * Matrix4::Rotate(0.f * (float) M_PI / 180.f, Vector3::UnitY) * Matrix4::Translate(0.f, -1.f, 0.f);
	model = Matrix4::Identity();
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	//projectionviewmodel = heightprojection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));


	gl::UniformMatrix4fv(uniforms.mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(uniforms.mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix4fv(uniforms.m, 1, gl::FALSE_, model.Data());
	gl::UniformMatrix3fv(uniforms.normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(uniforms.surfacecolor, 1, groundcolor.Data());

	for(int i = 0; i < ground->meshCount; ++i)
	{
		gl::BindVertexArray(ground->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, ground->verts[i]);
	}
}


void POMShadowScene::initFramebuffer()
{
	gl::GenFramebuffers(1, &framebuffer);
	gl::BindFramebuffer(gl::FRAMEBUFFER, framebuffer);

	heightmapSize.width = 1024;
	heightmapSize.height = 1024;
	gl::GenTextures(1, &heightmap);
	gl::BindTexture(gl::TEXTURE_2D, heightmap);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA, heightmapSize.width, heightmapSize.height, 0, gl::RGBA, gl::UNSIGNED_BYTE, nullptr);

	gl::GenTextures(1, &fbodepthtexture);
	gl::BindTexture(gl::TEXTURE_2D, fbodepthtexture);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::DEPTH_COMPONENT, heightmapSize.width, heightmapSize.height, 0, gl::DEPTH_COMPONENT, gl::FLOAT, nullptr);

	gl::GenSamplers(1, &sampler);
	gl::SamplerParameteri(sampler, gl::TEXTURE_MIN_FILTER, gl::NEAREST);
	gl::SamplerParameteri(sampler, gl::TEXTURE_MAG_FILTER, gl::NEAREST);
	gl::SamplerParameteri(sampler, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::SamplerParameteri(sampler, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);

	gl::FramebufferTexture(gl::FRAMEBUFFER, gl::DEPTH_ATTACHMENT, fbodepthtexture, 0);
	gl::FramebufferTexture(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, heightmap, 0);

	if(gl::CheckFramebufferStatus(gl::FRAMEBUFFER) != gl::FRAMEBUFFER_COMPLETE)
		MessageBox(nullptr, L"Fehler beim Erzeugen des FBO", L"FBO ERROR", MB_OK);

	gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
}