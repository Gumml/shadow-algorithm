#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <Scene.h>

#include <chrono>

#include <GLSLShader.h>
#include <DataTypes.h>

#include <FrameCounter.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using mgraphics::shader::GLSLShaderprogram;
using mgraphics::datatypes::Matrix4;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;
using mgraphics::datatypes::Vector2;
// =================================================================================================================================================================================================	#####




// Klassen																																															  Klassen
// =================================================================================================================================================================================================	#####
class SMScene :
	public Scene
{
	std::unique_ptr<RenderObject> ground;

	std::unique_ptr<RenderObject> cube;

	float cubeY;

	Vector4 groundcolor, cubecolor;

	std::unique_ptr<GLSLShaderprogram> program;

	GLuint loc_mat_mvp, loc_mat_mv, loc_mat_normal;
	GLuint loc_fragmentprogram, loc_fragmentprogramNoShadow, loc_vertexprogram, loc_shadowfragment, loc_shadowvertex;
	GLuint loc_light_position, loc_light_ambient, loc_light_diffuse;
	GLuint loc_surfacecolor;
	GLuint loc_textures_shadowmap;
	GLuint loc_mat_shadow;

	bool shadow;

	Matrix4 model, view, projection, projectionviewmodel, viewmodel;
	Matrix3 normal;

	Matrix4 shadowview, shadowprojection, shadowprojectionviewmodel, shadowprojectionview, texturebias;

	Vector3 lightpos;
	Vector4 ambientcolor, diffusecolor, specularcolor;
	float shininess;

	float sceneRotation;

	GLuint framebuffer;
	GLuint fbotexture, fbosampler;

	int shadowmapWidth, shadowmapHeight;
	int windowWidth, windowHeight;

	void initGeometry();
	void initShader();

	std::unique_ptr<unsigned char[]> imagedata;

	std::chrono::high_resolution_clock::time_point last;

	std::unique_ptr<statistics::FrameCounter> framectr;

public:
	SMScene(void);
	virtual ~SMScene(void);
	
	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};
// =================================================================================================================================================================================================	#####
