#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <Scene.h>

#include <GLSLShader.h>
#include <DataTypes.h>
#include <Texture.h>
#include <memory>
#include <vector>
#include <chrono>
#include <bitset>
#include <FrameCounter.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using mgraphics::shader::GLSLShaderprogram;
using mgraphics::datatypes::Matrix4;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;
using mgraphics::datatypes::Vector2;
using mgraphics::textures::Texture2D;
// =================================================================================================================================================================================================	#####




// Klassen																																															  Klassen
// =================================================================================================================================================================================================	#####
class TestScene :
	public Scene
{
	int meshCount;
	std::vector<unsigned int> verts;
	std::vector<unsigned int> meshVAOs;
	std::vector<unsigned int> meshVBOs;

	std::unique_ptr<GLSLShaderprogram> shader;

	std::unique_ptr<Texture2D> diffuseTexture, normalTexture, heightTexture;
	unsigned int textureSampler;

	unsigned int loc_matrices_mvp, loc_matrices_mv, loc_matrices_normal;
	unsigned int loc_light_position, loc_light_ambient, loc_light_diffuse, loc_light_specular, loc_light_shininess;
	unsigned int loc_textures_diffuse, loc_textures_normal, loc_textures_height;
	unsigned int loc_poms_textureScale, loc_poms_heightScale, loc_poms_heightBias, loc_poms_minSamples, loc_poms_maxSamples, loc_poms_textureDimension, loc_poms_shadowLength;

	unsigned int loc_vertex_subroutine, loc_fragment_subroutine;

	std::unique_ptr<Vector3> lightPosition;
	std::unique_ptr<Vector4> ambientLight, diffuseLight, specularLight;
	float shininess;

	Matrix4 projection, model, mvp;
	Matrix3 normal;

	std::unique_ptr<Vector2> textureDimension;

	std::chrono::high_resolution_clock::time_point last;

	std::bitset<2> keys;

	std::unique_ptr<statistics::FrameCounter> framectr;

public:
	TestScene(void);
	~TestScene(void);

	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};
// =================================================================================================================================================================================================	#####
