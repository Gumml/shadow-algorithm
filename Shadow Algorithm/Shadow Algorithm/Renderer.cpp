#include <Renderer.h>

#include <Scene.h>
#include <TestScene.h>
#include <SMScene.h>
#include <CudaTextureScene.h>
#include <HeightShadowScene.h>
#include <VolumeTracingScene.h>
#include <POMShadowScene.h>
#include <CurvySurfaceScene.h>
#include <VolumeTracingShadowScene.h>


namespace computergraphics
{
	Renderer::Renderer(GLFWwindow *window)
	{
		this->window = window;
	}


	Renderer::~Renderer()
	{
		scene.release();
	}


	void Renderer::destroy()
	{
		scene.release();
	}


	void Renderer::resize(int width, int height)
	{
		scene->resizeWindow(width, height);
	}


	void Renderer::input(int key, int scancode, int action, int modifiers)
	{
		if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
			glfwSetWindowShouldClose(window, gl::TRUE_);

		if(key == GLFW_KEY_LEFT && action == GLFW_RELEASE)
		{
			selection--;
			mgraphics::logic::clamp(selection, lowerSelectionBorder, upperSelectionBorder);
			selectScene();
		}

		if(key == GLFW_KEY_RIGHT && action == GLFW_RELEASE)
		{
			selection++;
			mgraphics::logic::clamp(selection, lowerSelectionBorder, upperSelectionBorder);
			selectScene();
		}

		scene->dispatchInput(window, key, scancode, action, modifiers);
	}


	bool Renderer::setup()
	{
		glfwGetFramebufferSize(window, &window_width, &window_height);

		if(!gl::sys::LoadFunctions())
			return false;

		selectScene();

		return true;
	}


	void Renderer::render()
	{
		scene->render();
	}


	void Renderer::update()
	{
		scene->update(window);
	}


	void Renderer::selectScene()
	{
		switch(selection)
		{
		case 0:
			scene.reset();
			scene = std::make_unique<TestScene>();
			break;
		case 1:
			scene.reset();
			scene = std::make_unique<SMScene>();
			break;
		case 2:
			scene.reset();
			scene = std::make_unique<HeightShadowScene>();
			break;
		case 3:
			scene.reset();
			scene = std::make_unique<CudaTextureScene>();
			break;
		case 4:
			scene.reset();
			scene = std::make_unique<VolumeTracingScene>();
			break;
		case 5:
			scene.reset();
			scene = std::make_unique<POMShadowScene>();
			break;
		case 6:
			scene.reset();
			scene = std::make_unique<CurvySurfaceScene>();
			break;
		case 7:
			scene.reset();
			scene = std::make_unique<VolumeTracingShadowScene>();
			break;

		default:
			scene.reset(new TestScene());
			break;
		}

		scene->init();

		scene->resizeWindow(window_width, window_height);
	}
}