#pragma once

#include <cuda.h>
#include <cuda_runtime.h>

namespace gpu
{
	namespace functions
	{
		void fill(cudaSurfaceObject_t target, cudaSurfaceObject_t source, dim3 textureDim);
	}
}