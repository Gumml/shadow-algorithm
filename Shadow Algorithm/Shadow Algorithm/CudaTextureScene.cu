#include <iostream>
#include <CudaTextureScene.cuh>
#include <device_launch_parameters.h>

#include <CudaError.h>

namespace gpu
{
	namespace functions
	{
		__global__ void negative(cudaSurfaceObject_t target, cudaSurfaceObject_t source, dim3 dimension)
		{
			unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
			unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

			if(x < dimension.x && y < dimension.y)
			{
				uchar4 readdata;
				surf2Dread(&readdata, source, x * sizeof(uchar4), y);

				uchar4 writedata = make_uchar4(0xff - readdata.x, 0xff - readdata.y, 0xff - readdata.z, 0xff);
				surf2Dwrite(writedata, target, x * sizeof(uchar4), y);
			}
		}




		void fill(cudaSurfaceObject_t target, cudaSurfaceObject_t source, dim3 textureDim)
		{
			dim3 threads(32, 32);
			dim3 blocks(static_cast<unsigned int>(std::ceil(static_cast<double>(textureDim.x) / static_cast<double>(threads.x))), static_cast<unsigned int>(std::ceil(static_cast<double>(textureDim.y) / static_cast<double>(threads.y))));

			//cudaEvent_t start, stop;
			//float elapsed;

			//gpu::error::cuda::check(cudaEventCreate(&start));
			//gpu::error::cuda::check(cudaEventCreate(&stop));

			//gpu::error::cuda::check(cudaEventRecord(start));

			negative<<< blocks, threads >>>(target, source, textureDim);

			//gpu::error::cuda::check(cudaEventRecord(stop));
			//gpu::error::cuda::check(cudaEventSynchronize(stop));
			//gpu::error::cuda::check(cudaEventElapsedTime(&elapsed, start, stop));
		}
	}
}