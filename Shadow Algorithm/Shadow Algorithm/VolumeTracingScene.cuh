#pragma once

#include <cuda.h>
#include <cuda_runtime.h>

namespace gpu
{
	namespace functions
	{
		void fillVolume(cudaSurfaceObject_t volumeSurface, dim3 textureDimension);
	}
}