#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <Scene.h>

#include <DataTypes.h>

#include <GLSLShader.h>

#include <Camera.h>

#include <cuda_gl_interop.h>

#include <FrameCounter.h>
// =================================================================================================================================================================================================	#####




// Klassen																																															  Klassen
// =================================================================================================================================================================================================	#####
class VolumeTracingScene :
	public Scene
{
	struct AttributeLocations
	{
		unsigned int vertex;
		unsigned int normal;
		unsigned int uv;
	};

	struct UniformLocations
	{
		unsigned int matrices_mvp;
		unsigned int matrices_mv;
		unsigned int matrices_normal;

		unsigned int surfacecolor;

		unsigned int light_position;
		unsigned int light_ambient;
		unsigned int light_diffuse;
		unsigned int light_specular;
		unsigned int light_shininess;

		unsigned int textures_volume;
		unsigned int layer;

		unsigned int vertexRenderpass;
		unsigned int fragmentRenderpass;
		unsigned int vertexRenderpassWithTexture;
		unsigned int fragmentRenderpassWithTexture;
	};

	struct Lighting
	{
		mgraphics::datatypes::Vector3 position;
		mgraphics::datatypes::Vector4 ambient;
		mgraphics::datatypes::Vector4 diffuse;
		mgraphics::datatypes::Vector4 specular;
		float shininess;
	};

	std::unique_ptr<RenderObject> cube;
	float cubeY;
	mgraphics::datatypes::Vector4 cubecolor;

	std::unique_ptr<RenderObject> ground;
	mgraphics::datatypes::Vector4 groundcolor;

	std::unique_ptr<mgraphics::shader::GLSLShaderprogram> program;

	AttributeLocations attribs;
	UniformLocations uniforms;
	Lighting light;

	mgraphics::datatypes::Matrix4 projection, view, model;
	mgraphics::datatypes::Matrix4 projectionviewmodel, viewmodel;
	mgraphics::datatypes::Matrix3 normal;

	std::unique_ptr<mgraphics::Camera> observer;

	dim3 volumeDimension;
	cudaGraphicsResource *volumeResource;
	unsigned int volumeTexture;
	unsigned int volumeSampler;
	float layer;

	std::unique_ptr<statistics::FrameCounter> framectr;


	void initGeometry();
	void initShader();
	void initCuda();
	void initTexture();

	void cudaPass();
	void renderScene();


public:
	VolumeTracingScene(void);
	virtual ~VolumeTracingScene(void);

	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};
// =================================================================================================================================================================================================	#####
