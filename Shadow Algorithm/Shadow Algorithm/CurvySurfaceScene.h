#pragma once
#include <Scene.h>

#include <GLSLShader.h>
#include <DataTypes.h>
#include <Camera.h>
#include <Texture.h>
#include <bitset>
#include <chrono>
#include <FrameCounter.h>



class CurvySurfaceScene :
	public Scene
{
	struct AttributeLocations
	{
		unsigned int vertex;
		unsigned int normal;
		unsigned int uv;
	};

	struct UniformLocations
	{
		unsigned int mvp;
		unsigned int mv;
		unsigned int m;
		unsigned int normal;

		unsigned int surfacecolor;

		unsigned int lightpos;
		unsigned int lightambient;
		unsigned int lightdiffuse;
		unsigned int lightspecular;
		unsigned int lightshininess;

		unsigned int heightmap;

		unsigned int renderFragment;
		unsigned int renderVertex;

		unsigned int heightmapFragment;
		unsigned int heightmapVertex;

		unsigned int accesspassFragment;
		unsigned int accesspassVertex;
	};

	struct Lighting
	{
		mgraphics::datatypes::Vector3 position;
		mgraphics::datatypes::Vector4 ambient, diffuse, specular;
		float shininess;
	};

	struct ImageDimension
	{
		unsigned int width;
		unsigned int height;
	};


	std::bitset<2> keys;

	std::unique_ptr<RenderObject> ground;

	mgraphics::datatypes::Vector4 groundcolor;

	std::unique_ptr<mgraphics::shader::GLSLShaderprogram> program;

	AttributeLocations attribs;
	UniformLocations uniforms;

	float lightradius = 0.f;
	Lighting light;

	mgraphics::datatypes::Matrix4 projection, view, model;
	mgraphics::datatypes::Matrix4 projectionviewmodel, viewmodel;
	mgraphics::datatypes::Matrix3 normal;

	mgraphics::datatypes::Matrix4 heightprojection;

	std::unique_ptr<mgraphics::Camera> observerCamera;
	std::unique_ptr<mgraphics::Camera> heightmapCamera;

	ImageDimension windowSize;

	unsigned int framebuffer;
	unsigned int heightmap;
	unsigned int fbodepthtexture;
	unsigned int sampler;
	ImageDimension heightmapSize;


	std::chrono::high_resolution_clock::time_point last;
	std::unique_ptr<mgraphics::textures::Texture2D> texHightmap;

	std::unique_ptr<statistics::FrameCounter> framectr;



	void initGeometry();
	void initShader();
	void initFramebuffer();

	void renderHeightmap();
	void renderScene();

public:
	CurvySurfaceScene();
	virtual ~CurvySurfaceScene();

	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};

