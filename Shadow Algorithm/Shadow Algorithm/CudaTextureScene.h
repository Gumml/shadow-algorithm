#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <Scene.h>

#include <GLSLShader.h>
#include <DataTypes.h>
#include <Texture.h>

#include <cuda_gl_interop.h>

#include <FrameCounter.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using mgraphics::shader::GLSLShaderprogram;
using mgraphics::datatypes::Matrix4;
using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;
using mgraphics::textures::Texture2D;
// =================================================================================================================================================================================================	#####




// Klassen																																															  Klassen
// =================================================================================================================================================================================================	#####
class CudaTextureScene :
	public Scene
{
	std::unique_ptr<RenderObject> canvas;

	std::unique_ptr<GLSLShaderprogram> shader;

	GLuint locMatrices_mvp;
	GLuint locTextures_image;
	GLuint locSubroutine_vertex, locSubroutine_fragment;

	std::unique_ptr<Texture2D> texture;
	GLuint cudatexture;

	GLuint texturesampler;

	Matrix4 model, view, projection;

	cudaGraphicsResource *resources[2];

	std::unique_ptr<statistics::FrameCounter> framectr;

	void initGeometry();
	void cudaPass();
	void initShader();
	void initTextures();
	void initCuda();

public:
	CudaTextureScene(void);
	virtual ~CudaTextureScene(void);

	virtual void init();
	virtual void render();
	virtual void update(GLFWwindow *window);
	virtual void resizeWindow(GLsizei width, GLsizei height);
	//virtual void dispatchInput(WPARAM key);
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers);
};
// =================================================================================================================================================================================================	#####
