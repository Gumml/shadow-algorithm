#pragma once

#include <mgraphics.h>

#include <GLFW/glfw3.h>

#include <Scene.h>


namespace computergraphics
{
	class Renderer
	{
	protected:
		GLFWwindow *window;

		int window_width, window_height;

		// Scenes
		std::unique_ptr<Scene> scene;
		
		
		int selection = 7;
		
		int lowerSelectionBorder = 0;
		int upperSelectionBorder = 7;



		void selectScene();

	public:
		Renderer(GLFWwindow *window);
		virtual ~Renderer(void);

		void destroy();
		void resize(int width, int height);
		void input(int key, int scancode, int action, int modifiers);
		bool setup();
		void render();
		void update();
	};
}
