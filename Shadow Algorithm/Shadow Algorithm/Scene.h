#pragma once

// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <mgraphics.h>
#include <memory>

#include <GLFW/glfw3.h>
// =================================================================================================================================================================================================	#####





// Structs																																															  Structs
// =================================================================================================================================================================================================	#####
struct RenderObject
{
	int meshCount;
	std::unique_ptr<unsigned int[]> verts;
	std::unique_ptr<unsigned int[]> meshVAOs;
	std::unique_ptr<unsigned int[]> meshVBOs;
};
// =================================================================================================================================================================================================	#####





class Scene
{
public:
	Scene(void);
	virtual ~Scene(void);

	virtual void init() = 0;
	virtual void render() = 0;
	virtual void update(GLFWwindow *window) = 0;
	virtual void resizeWindow(GLsizei width, GLsizei height) = 0;
	//virtual void dispatchInput(WPARAM key) = 0;
	virtual void dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers) = 0;
};

