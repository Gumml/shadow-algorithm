// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <TestScene.h>

#include <string>
#include <vector>
#include <iostream>

#include <FBXModel.h>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using mgraphics::fbxmodel::FBXModel;
using mgraphics::fbxmodel::Mesh;
// =================================================================================================================================================================================================	#####




// Methoden																																															 Methoden
// =================================================================================================================================================================================================	#####
TestScene::TestScene(void)
{
	std::cout << "TestScene created" << std::endl;
	last = std::chrono::high_resolution_clock::now();
}


TestScene::~TestScene(void)
{
	std::cout << "TestScene deleted" << std::endl;

	gl::DeleteVertexArrays(meshCount, meshVAOs.data());
	gl::DeleteBuffers(meshCount, meshVBOs.data());
	gl::DeleteSamplers(1, &textureSampler);
}



void TestScene::init()
{
	gl::ClearColor(.2353f, .2353f, .2353f, 1.f);
	gl::Enable(gl::DEPTH_TEST);


	// Geometry Data
	FBXModel *fbxmodel = new FBXModel("../models/TestScene/ParallaxCube.fbx");

	meshCount = fbxmodel->MeshCount();

	meshVAOs.resize(meshCount);
	gl::GenVertexArrays(meshCount, meshVAOs.data());

	meshVBOs.resize(meshCount);
	gl::GenBuffers(meshCount, meshVBOs.data());

	verts.resize(meshCount);

	unsigned int currentMesh = 0;
	std::vector<Mesh> meshes = fbxmodel->Meshes();
	for(Mesh &mesh : meshes)
	{
		verts[currentMesh] = mesh.VertexCount();

		gl::BindVertexArray(meshVAOs[currentMesh]);

		int vertexbufferSize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbufferSize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbufferSize = 2 * mesh.UVDataCount() * sizeof(float);
		int tangentbufferSize = 3 * mesh.TangentCount() * sizeof(float);
		int bitangentbufferSize = 3 * mesh.BitangentCount() * sizeof(float);

		gl::BindBuffer(gl::ARRAY_BUFFER, meshVBOs[currentMesh]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbufferSize + normalbufferSize + uvbufferSize + tangentbufferSize + bitangentbufferSize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbufferSize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbufferSize, normalbufferSize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbufferSize + normalbufferSize, uvbufferSize, mesh.UVData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbufferSize + normalbufferSize + uvbufferSize, tangentbufferSize, mesh.TangentData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbufferSize + normalbufferSize + uvbufferSize + tangentbufferSize, bitangentbufferSize, mesh.BitangentData());

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(1);
		gl::VertexAttribPointer(1, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbufferSize);

		gl::EnableVertexAttribArray(2);
		gl::VertexAttribPointer(2, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbufferSize + normalbufferSize));

		gl::EnableVertexAttribArray(3);
		gl::VertexAttribPointer(3, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbufferSize + normalbufferSize + uvbufferSize));

		gl::EnableVertexAttribArray(4);
		gl::VertexAttribPointer(4, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbufferSize + normalbufferSize + uvbufferSize + tangentbufferSize));

		currentMesh++;
	}

	gl::BindVertexArray(0);
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);

	delete fbxmodel;

	model = Matrix4::Translate(0.f, 0.f, -3.f) * Matrix4::Rotate(30.f * (float) M_PI / 180.f, Vector3::UnitY);




	// Shader Data
	shader = std::make_unique<GLSLShaderprogram>("../shader/TestScene/VertexShader.glsl", "../shader/TestScene/FragmentShader.glsl");

	loc_matrices_mvp = shader->getUniformLocation("matrices.mvp");
	loc_matrices_mv = shader->getUniformLocation("matrices.mv");
	loc_matrices_normal = shader->getUniformLocation("matrices.normal");

	loc_light_position = shader->getUniformLocation("light.position");
	loc_light_ambient = shader->getUniformLocation("light.ambient");
	loc_light_diffuse = shader->getUniformLocation("light.diffuse");
	loc_light_specular = shader->getUniformLocation("light.specular");
	loc_light_shininess = shader->getUniformLocation("light.shininess");

	loc_textures_diffuse = shader->getUniformLocation("textures.diffuse");
	loc_textures_normal = shader->getUniformLocation("textures.normal");
	loc_textures_height = shader->getUniformLocation("textures.height");

	loc_poms_textureScale = shader->getUniformLocation("poms.textureScale");
	loc_poms_heightScale = shader->getUniformLocation("poms.heightScale");
	loc_poms_heightBias = shader->getUniformLocation("poms.heightBias");
	loc_poms_minSamples = shader->getUniformLocation("poms.minSamples");
	loc_poms_maxSamples = shader->getUniformLocation("poms.maxSamples");
	loc_poms_maxSamples = shader->getUniformLocation("poms.textureDimension");
	loc_poms_shadowLength = shader->getUniformLocation("poms.shadowLength");

	loc_vertex_subroutine = shader->getSubroutineIndex(gl::VERTEX_SHADER, "parallaxocclusion");
	loc_fragment_subroutine = shader->getSubroutineIndex(gl::FRAGMENT_SHADER, "parallaxocclusion");


	// Textures
	diffuseTexture = std::make_unique<Texture2D>();
	diffuseTexture->Generate("../textures/TestScene/cobblestonesDiffuse.png", true);

	normalTexture = std::make_unique<Texture2D>();
	normalTexture->Generate("../textures/TestScene/cobblestonesNormal.png", true);

	heightTexture = std::make_unique<Texture2D>();
	heightTexture->Generate("../textures/TestScene/cobblestonesDepth.png", true);

	textureDimension = std::make_unique<Vector2>((float) diffuseTexture->getWidth(), (float) diffuseTexture->getHeight());

	gl::GenSamplers(1, &textureSampler);
	gl::SamplerParameteri(textureSampler, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR);
	gl::SamplerParameteri(textureSampler, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::SamplerParameteri(textureSampler, gl::TEXTURE_WRAP_S, gl::REPEAT);
	gl::SamplerParameteri(textureSampler, gl::TEXTURE_WRAP_T, gl::REPEAT);
	float maxAnisotropy;
	gl::GetFloatv(gl::MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
	gl::SamplerParameterf(textureSampler, gl::TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);


	// Light
	lightPosition = std::make_unique<Vector3>(0.f, 0.f, -1.f);
	ambientLight = std::make_unique<Vector4>(.3f, .3f, .3f, 1.f);
	diffuseLight = std::make_unique<Vector4>(1.f, 1.f, 1.f, 1.f);
	specularLight = std::make_unique<Vector4>(1.f, 1.f, 1.f, 1.f);
	shininess = 10.f;

	framectr = std::make_unique<statistics::FrameCounter>();
}



void TestScene::render()
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	//model *= Matrix3::Rotate(0.5f * (float) M_PI / 180.f, Vector3::UnitY);

	mvp = projection * model;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(model)));

	shader->use();

	gl::UniformMatrix4fv(loc_matrices_mvp, 1, gl::FALSE_, mvp.Data());
	gl::UniformMatrix4fv(loc_matrices_mv, 1, gl::FALSE_, model.Data());
	gl::UniformMatrix3fv(loc_matrices_normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform3fv(loc_light_position, 1, lightPosition->Data());
	gl::Uniform4fv(loc_light_ambient, 1, ambientLight->Data());
	gl::Uniform4fv(loc_light_diffuse, 1, diffuseLight->Data());
	gl::Uniform4fv(loc_light_specular, 1, specularLight->Data());
	gl::Uniform1f(loc_light_shininess, shininess);

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindSampler(0, textureSampler);
	gl::BindTexture(gl::TEXTURE_2D, diffuseTexture->getTextureID());
	gl::Uniform1i(loc_textures_diffuse, 0);

	gl::ActiveTexture(gl::TEXTURE1);
	gl::BindSampler(1, textureSampler);
	gl::BindTexture(gl::TEXTURE_2D, normalTexture->getTextureID());
	gl::Uniform1i(loc_textures_normal, 1);

	gl::ActiveTexture(gl::TEXTURE2);
	gl::BindSampler(2, textureSampler);
	gl::BindTexture(gl::TEXTURE_2D, heightTexture->getTextureID());
	gl::Uniform1i(loc_textures_height, 2);

	gl::Uniform1f(loc_poms_textureScale, 8.f);
	gl::Uniform1f(loc_poms_heightScale, .02f);
	gl::Uniform1f(loc_poms_heightBias, .01f);
	gl::Uniform1i(loc_poms_minSamples, 200);
	gl::Uniform1i(loc_poms_maxSamples, 600);
	gl::Uniform2fv(loc_poms_textureDimension, 1, textureDimension->Data());
	gl::Uniform1f(loc_poms_shadowLength, 25.f);

	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &loc_vertex_subroutine);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &loc_fragment_subroutine);

	for(int i = 0; i < meshCount; ++i)
	{
		gl::BindVertexArray(meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, verts[i]);
	}
}




void TestScene::update(GLFWwindow *window)
{
	auto now = std::chrono::high_resolution_clock::now();
	long long delta = std::chrono::duration_cast<std::chrono::milliseconds>(now - last).count();


	if(keys.test(0))
		model *= Matrix3::Rotate(-0.5f * (float) M_PI / 180.f, Vector3::UnitY);

	if(keys.test(1))
		model *= Matrix3::Rotate(0.5f * (float) M_PI / 180.f, Vector3::UnitY);


	last = now;
	framectr->tick(window);
}




void TestScene::resizeWindow(GLsizei width, GLsizei height)
{
	gl::Viewport(0, 0, width, height);

	float aspect = (float) width / (float) height;

	projection = Matrix4::Perspective(60.0f * (float) M_PI / 180.f, aspect, .5f, 30.f);
}




//void TestScene::dispatchInput(WPARAM key)
//{
//
//}



void TestScene::dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{
	if(key == GLFW_KEY_A && action == GLFW_PRESS)
		keys.set(0, true);

	if(key == GLFW_KEY_A && action == GLFW_RELEASE)
		keys.set(0, false);

	if(key == GLFW_KEY_D && action == GLFW_PRESS)
		keys.set(1, true);

	if(key == GLFW_KEY_D && action == GLFW_RELEASE)
		keys.set(1, false);
}
// =================================================================================================================================================================================================	#####
