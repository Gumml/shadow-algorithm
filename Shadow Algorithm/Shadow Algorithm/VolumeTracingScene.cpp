// Includes																																															 Includes
// =================================================================================================================================================================================================	#####
#include <VolumeTracingScene.h>

#include <vector>

#include <FBXModel.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <CudaError.h>

#include <VolumeTracingScene.cuh>
// =================================================================================================================================================================================================	#####




// Usings																																															   Usings
// =================================================================================================================================================================================================	#####
using std::cout;
using std::endl;

using mgraphics::shader::GLSLShaderprogram;

using mgraphics::Camera;

using mgraphics::datatypes::Matrix3;
using mgraphics::datatypes::Matrix4;
using mgraphics::datatypes::Vector3;
using mgraphics::datatypes::Vector4;

using mgraphics::fbxmodel::FBXModel;
using mgraphics::fbxmodel::Mesh;
// =================================================================================================================================================================================================	#####




// Methoden																																															 Methoden
// =================================================================================================================================================================================================	#####
VolumeTracingScene::VolumeTracingScene(void)
{
	cube = std::make_unique<RenderObject>();
	ground = std::make_unique<RenderObject>();

	cout << "Volume Tracing Scene created" << endl;
}


VolumeTracingScene::~VolumeTracingScene(void)
{
	gl::Disable(gl::DEPTH_TEST);
	gl::Disable(gl::CULL_FACE);

	gl::DeleteVertexArrays(cube->meshCount, cube->meshVAOs.get());
	gl::DeleteBuffers(cube->meshCount, cube->meshVBOs.get());

	gl::DeleteVertexArrays(ground->meshCount, ground->meshVAOs.get());
	gl::DeleteBuffers(ground->meshCount, ground->meshVBOs.get());

	gl::DeleteTextures(1, &volumeTexture);
	gl::DeleteSamplers(1, &volumeSampler);

	gpu::error::cuda::check(cudaGraphicsUnregisterResource(volumeResource));

	gpu::error::cuda::check(cudaDeviceReset());

	cout << "Volume Tracing Scene deleted" << endl;
}


void VolumeTracingScene::init()
{
	gl::ClearColor(.2353f, .2353f, .2353f, 1.f);
	gl::Enable(gl::DEPTH_TEST);
	gl::Enable(gl::CULL_FACE);

	initShader();
	initGeometry();
	initTexture();
	initCuda();


	//groundcolor = Vector4(.6431f, .7804f, .2235f, 1.f);
	groundcolor = Vector4(.3f, .2f, .3f, 1.f);
	cubecolor = Vector4(.0f, .6f, .8f, 1.f);


	observer = std::make_unique<Camera>();
	//observer->moveTo(.0f, 3.f, 5.f);
	observer->moveTo(.0f, 6.f, 10.f);
	view = observer->getViewMatrix();


	light.position = Vector3(4.f, 3.f, -2.5f);
	light.ambient = Vector4(.3f, .3f, .3f, 1.f);
	light.diffuse = Vector4(1.f, 1.f, 1.f, 1.f);
	light.specular = Vector4(1.f, 1.f, 1.f, 1.f);
	light.shininess = 16.f;

	framectr = std::make_unique<statistics::FrameCounter>();
}


void VolumeTracingScene::render()
{
	cudaPass();

	renderScene();
}


void VolumeTracingScene::update(GLFWwindow *window)
{
	framectr->tick(window);
}


void VolumeTracingScene::resizeWindow(GLsizei width, GLsizei height)
{
	gl::Viewport(0, 0, width, height);

	float aspect = static_cast<float>(width) / static_cast<float>(height);
	projection = Matrix4::Perspective(60.f * static_cast<float>(M_PI) / 180.f, aspect, .1f, 100.f);
}


//void VolumeTracingScene::dispatchInput(WPARAM key)
//{
//	if(key == VK_UP)
//		cubeY += .5f;
//
//	if(key == VK_DOWN)
//		cubeY -= .5f;
//
//	if(key == VK_PRIOR)
//		layer += .1001f;
//
//	if(key == VK_NEXT)
//		layer -= .1001f;
//
//	mgraphics::logic::clamp<float>(cubeY, -.5f, 3.f);
//	mgraphics::logic::clamp<float>(layer, .0f, 1.f);
//
//	cout << "Cube Y: " << cubeY << " Layer: " << layer << endl;
//}


void VolumeTracingScene::dispatchInput(GLFWwindow *window, int key, int scancode, int action, int modifiers)
{
	if(key == GLFW_KEY_UP && action == GLFW_RELEASE)
		cubeY += .5f;

	if(key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
		cubeY -= .5f;

	if(key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE)
		layer += .1001f;

	if(key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE)
		layer -= .1001f;

	mgraphics::logic::clamp<float>(cubeY, -.5f, 3.f);
	mgraphics::logic::clamp<float>(layer, .0f, 1.f);

	cout << "Cube Y: " << cubeY << " Layer: " << layer << endl;
}


void VolumeTracingScene::initShader()
{
	program = std::make_unique<GLSLShaderprogram>("../shader/VolumeTracingScene/VertexShader.glsl", "../shader/VolumeTracingScene/FragmentShader.glsl");

	attribs.vertex = program->getAttribLocation("vertex");
	attribs.normal = program->getAttribLocation("normal");
	attribs.uv = program->getAttribLocation("uv");

	uniforms.matrices_mvp = program->getUniformLocation("matrices.mvp");
	uniforms.matrices_mv = program->getUniformLocation("matrices.mv");
	uniforms.matrices_normal = program->getUniformLocation("matrices.normal");

	uniforms.surfacecolor = program->getUniformLocation("surfacecolor");
	uniforms.layer = program->getUniformLocation("layer");

	uniforms.light_position = program->getUniformLocation("light.position");
	uniforms.light_ambient = program->getUniformLocation("light.ambient");
	uniforms.light_diffuse = program->getUniformLocation("light.diffuse");
	uniforms.light_specular = program->getUniformLocation("light.specular");
	uniforms.light_shininess = program->getUniformLocation("light.shininess");

	uniforms.vertexRenderpass = program->getSubroutineIndex(gl::VERTEX_SHADER, "renderpass");
	uniforms.fragmentRenderpass = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "renderpass");
	uniforms.vertexRenderpassWithTexture = program->getSubroutineIndex(gl::VERTEX_SHADER, "renderpassWithTexture");
	uniforms.fragmentRenderpassWithTexture = program->getSubroutineIndex(gl::FRAGMENT_SHADER, "renderpassWithTexture");
}


void VolumeTracingScene::initGeometry()
{
	std::unique_ptr<FBXModel> model = std::make_unique<FBXModel>("../models/SMScene/Ground.fbx");

	ground->meshCount = model->MeshCount();

	ground->meshVAOs.reset(new unsigned int[ground->meshCount]);
	ground->meshVBOs.reset(new unsigned int[ground->meshCount]);
	ground->verts.reset(new unsigned int[ground->meshCount]);


	gl::GenVertexArrays(ground->meshCount, ground->meshVAOs.get());

	gl::GenBuffers(ground->meshCount, ground->meshVBOs.get());

	unsigned int current = 0;
	std::vector<Mesh> groundmeshes = model->Meshes();
	for(Mesh &mesh : groundmeshes)
	{
		ground->verts[current] = mesh.VertexCount();

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindVertexArray(ground->meshVAOs[current]);

		gl::BindBuffer(gl::ARRAY_BUFFER, ground->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(attribs.vertex);
		gl::VertexAttribPointer(attribs.vertex, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(attribs.normal);
		gl::VertexAttribPointer(attribs.normal, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		gl::EnableVertexAttribArray(attribs.uv);
		gl::VertexAttribPointer(attribs.uv, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbuffersize + normalbuffersize));
	}


	model = std::make_unique<FBXModel>("../models/SMScene/Cube.fbx");

	cube->meshCount = model->MeshCount();

	cube->meshVAOs.reset(new unsigned int[cube->meshCount]);
	cube->meshVBOs.reset(new unsigned int[cube->meshCount]);
	cube->verts.reset(new unsigned int[cube->meshCount]);


	gl::GenVertexArrays(cube->meshCount, cube->meshVAOs.get());
	gl::GenBuffers(cube->meshCount, cube->meshVBOs.get());


	current = 0;
	std::vector<Mesh> cubemeshes = model->Meshes();
	for(Mesh &mesh : cubemeshes)
	{
		cube->verts[current] = mesh.VertexCount();

		int vertexbuffersize = 3 * mesh.VertexCount() * sizeof(float);
		int normalbuffersize = 3 * mesh.NormalCount() * sizeof(float);
		int uvbuffersize = 2 * mesh.UVDataCount() * sizeof(float);

		gl::BindVertexArray(cube->meshVAOs[current]);

		gl::BindBuffer(gl::ARRAY_BUFFER, cube->meshVBOs[current]);
		gl::BufferData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize + uvbuffersize, nullptr, gl::STATIC_DRAW);

		gl::BufferSubData(gl::ARRAY_BUFFER, 0, vertexbuffersize, mesh.VertexData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize, normalbuffersize, mesh.NormalData());
		gl::BufferSubData(gl::ARRAY_BUFFER, vertexbuffersize + normalbuffersize, uvbuffersize, mesh.UVData());

		gl::EnableVertexAttribArray(attribs.vertex);
		gl::VertexAttribPointer(attribs.vertex, 3, gl::FLOAT, gl::FALSE_, 0, 0);

		gl::EnableVertexAttribArray(attribs.normal);
		gl::VertexAttribPointer(attribs.normal, 3, gl::FLOAT, gl::FALSE_, 0, (const void *) vertexbuffersize);

		gl::EnableVertexAttribArray(attribs.uv);
		gl::VertexAttribPointer(attribs.uv, 2, gl::FLOAT, gl::FALSE_, 0, (const void *) (vertexbuffersize + normalbuffersize));
	}

	gl::BindVertexArray(0);
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);


	cubeY = .5f;
}


void VolumeTracingScene::initTexture()
{
	volumeDimension = dim3(10, 10, 10);

	gl::GenTextures(1, &volumeTexture);
	gl::BindTexture(gl::TEXTURE_3D, volumeTexture);
	gl::TexImage3D(gl::TEXTURE_3D, 0, gl::RGBA, volumeDimension.x, volumeDimension.y, volumeDimension.z, 0, gl::RGBA, gl::UNSIGNED_BYTE, nullptr);
	gl::BindTexture(gl::TEXTURE_3D, 0);

	gl::GenSamplers(1, &volumeSampler);
	gl::SamplerParameteri(volumeSampler, gl::TEXTURE_MIN_FILTER, gl::NEAREST);
	gl::SamplerParameteri(volumeSampler, gl::TEXTURE_MAG_FILTER, gl::NEAREST);
	gl::SamplerParameteri(volumeSampler, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::SamplerParameteri(volumeSampler, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);
	gl::SamplerParameteri(volumeSampler, gl::TEXTURE_WRAP_R, gl::CLAMP_TO_EDGE);
	
	
	layer = .0f;
}


void VolumeTracingScene::initCuda()
{
	gpu::error::cuda::check(cudaSetDevice(0));
	gpu::error::cuda::check(cudaGLSetGLDevice(0));

	
	gpu::error::cuda::check(cudaGraphicsGLRegisterImage(&volumeResource, volumeTexture, gl::TEXTURE_3D, cudaGraphicsRegisterFlagsSurfaceLoadStore));
}


void VolumeTracingScene::cudaPass()
{
	gpu::error::cuda::check(cudaGraphicsMapResources(1, &volumeResource, 0));

	cudaArray_t volumeArray;
	gpu::error::cuda::check(cudaGraphicsSubResourceGetMappedArray(&volumeArray, volumeResource, 0, 0));

	cudaResourceDesc volumeDescription;
	memset(&volumeDescription, 0, sizeof(volumeDescription));
	volumeDescription.resType = cudaResourceTypeArray;
	volumeDescription.res.array.array = volumeArray;

	cudaSurfaceObject_t volumeSurface;
	gpu::error::cuda::check(cudaCreateSurfaceObject(&volumeSurface, &volumeDescription));

	gpu::functions::fillVolume(volumeSurface, volumeDimension);

	gpu::error::cuda::check(cudaDestroySurfaceObject(volumeSurface));

	gpu::error::cuda::check(cudaGraphicsUnmapResources(1, &volumeResource));
}


void VolumeTracingScene::renderScene()
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	program->use();

	model = Matrix4::Identity();
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));


	gl::Uniform3fv(uniforms.light_position, 1, light.position.Data());
	gl::Uniform4fv(uniforms.light_ambient, 1, light.ambient.Data());
	gl::Uniform4fv(uniforms.light_diffuse, 1, light.diffuse.Data());
	gl::Uniform4fv(uniforms.light_specular, 1, light.specular.Data());
	gl::Uniform1f(uniforms.light_shininess, light.shininess);

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindSampler(0, volumeSampler);
	gl::BindTexture(gl::TEXTURE_3D, volumeTexture);
	gl::Uniform1i(uniforms.textures_volume, 0);

	gl::Uniform1f(uniforms.layer, layer);

	//gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.vertexRenderpass);
	//gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.fragmentRenderpass);
	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.vertexRenderpassWithTexture);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.fragmentRenderpassWithTexture);

	gl::UniformMatrix4fv(uniforms.matrices_mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(uniforms.matrices_mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix3fv(uniforms.matrices_normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(uniforms.surfacecolor, 1, groundcolor.Data());


	for(int i = 0; i < ground->meshCount; ++i)
	{
		gl::BindVertexArray(ground->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, ground->verts[i]);
	}



	model = Matrix4::Translate(0.f, cubeY, 0.f);
	viewmodel = view * model;
	projectionviewmodel = projection * viewmodel;
	normal = Matrix3::Transpose(Matrix3::Invert(Matrix3(viewmodel)));


	gl::UniformSubroutinesuiv(gl::VERTEX_SHADER, 1, &uniforms.vertexRenderpass);
	gl::UniformSubroutinesuiv(gl::FRAGMENT_SHADER, 1, &uniforms.fragmentRenderpass);

	gl::UniformMatrix4fv(uniforms.matrices_mvp, 1, gl::FALSE_, projectionviewmodel.Data());
	gl::UniformMatrix4fv(uniforms.matrices_mv, 1, gl::FALSE_, viewmodel.Data());
	gl::UniformMatrix3fv(uniforms.matrices_normal, 1, gl::FALSE_, normal.Data());

	gl::Uniform4fv(uniforms.surfacecolor, 1, cubecolor.Data());


	for(int i = 0; i < cube->meshCount; ++i)
	{
		gl::BindVertexArray(cube->meshVAOs[i]);
		gl::DrawArrays(gl::TRIANGLES, 0, cube->verts[i]);
	}
}
// =================================================================================================================================================================================================	#####
